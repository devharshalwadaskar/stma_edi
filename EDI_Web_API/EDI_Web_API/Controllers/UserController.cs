﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EDI_Web_API.Entities;
using EDI_Web_API.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace EDI_Web_API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private IConfiguration configuration;
        public UserController(IConfiguration iConfig) 
        {
            configuration = iConfig;
        }

        [HttpGet, Route("GetAll_Roles")]
        public ActionResult<string> GetAll_User_Types()
        {
            User_Service user_Service = new User_Service(configuration);
            return Json(user_Service.GetAll_Roles());
        }

        [HttpGet, Route("Search_User")]
        public ActionResult<string> Search_User(string user)
        {
            User_Service user_Service = new User_Service(configuration);
            return Json(user_Service.Search_User(user));
        }

        [HttpGet, Route("GetAll_Users")]
        public ActionResult<string> GetAll_Users()
        {
            User_Service user_Service = new User_Service(configuration);
            return Json(user_Service.GetAll_Users());
        }

        [HttpGet, Route("Get_User")]
        public ActionResult<string> Get_User(long user_Id)
        {
            User_Service user_Service = new User_Service(configuration);
            return Json(user_Service.Get_User(user_Id));
        }

        [HttpPost, Route("InsertUpdate_User")]
        public JsonResult Mapping_InsertUpdate([FromBody]User_Entity user_Entity)
        {
            User_Service user_Service = new User_Service(configuration);
            return Json(user_Service.InsertUpdate_User(user_Entity));
        }

        [HttpGet, Route("Delete_User")]
        public ActionResult<string> Delete_User(long user_Id)
        {
            User_Service customer_Service = new User_Service(configuration);
            return Json(customer_Service.Delete_User(user_Id));
        }
    }
}