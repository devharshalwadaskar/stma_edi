﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EDI_Web_API.Entities;
using EDI_Web_API.Services;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace EDI_Web_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EDI_DecryptionController : Controller 
    {
        private IConfiguration configuration;
        public EDI_DecryptionController(IConfiguration iConfig) 
        {
            configuration = iConfig;
        }
        public Operation_Message OwnCreated() 
        {
            Operation_Message operation_Message = new Operation_Message(); 

            EDI_Decryption_Service eDI_Decryption_Service = new EDI_Decryption_Service(configuration); 

            string file_Path = @"C:\Users\tejas\OneDrive\Desktop\STMA\EDISamplesFile\EDI3.txt";

            //Array initialization  for edi file 
            String[] edi_File_LineArray;

            //Variable initialization  

            String vendor_Name = "", customer_City = "", customer_State = "", vendor_Zip = "", vendor_Phone_Number = "",
                   vendor_Email_Address1 = "", vendor_Email_Address2 = "", substring_Without_VCD = "", substring_Without_VCD2 = "",
                    substring_Without_INV = "", vendor_Address = "", vendor_Address2 = "", vendor_City_Name = "", vendor_State_Name = "",
                    items_Code, substring_Without_CN = "", customer_Name = "", customer_Id = "", customer_Address = "", sustring_Without_CCD = "", sustring_Without_CCD2 = "",
                    customer_Email_Address1 = "", customer_Email_Address2 = "", customer_Phone_Number = "", invoice_Number = "", invoice_Date = "";


            int index_Of_Vendor_Email1, index_Of_Vendor_Email2, index_Of_Email1, index_Of_Email2;

            edi_File_LineArray = System.IO.File.ReadAllLines(file_Path);

            List<string> item_Code = new List<string>();
            List<string> item_Qunatity = new List<string>();
            List<string> item_Unitcase_Name = new List<string>();
            List<string> item_UnitPrice = new List<string>();
            List<string> item_Discount_Amount = new List<string>();
            List<string> item_Total_Amount = new List<string>();
            List<string> item_Description = new List<string>();
            List<Product_Entity> list_Object = new List<Product_Entity>();


            string customer_Address2 = "", unit_Case_Name = "", customer_Fax = "", TOTdiscount, total_Amount = "", vendor_Tax = "", number_Of_Items;

            for (int p = 0; p < edi_File_LineArray.Length; p++)
            {

                String line_Of_File = edi_File_LineArray[p];
                if (edi_File_LineArray[p].Contains("ITM1*"))
                {

                    int index_Of_ITM1 = edi_File_LineArray[p].IndexOf("ITM1*");

                    items_Code = (edi_File_LineArray[p].Substring(index_Of_ITM1 + 5)).Split('*')[0];

                    // String d = edi_File_LineArray[p].Substring(edi_File_LineArray[p].IndexOf("ITM1*"), edi_File_LineArray[p].Length);

                    item_Code.Add(items_Code);

                    int index_Of_ITM = edi_File_LineArray[p].IndexOf("ITM1*");
                    String substring_Without_ITM = edi_File_LineArray[p].Substring(index_Of_ITM + 5);
                    int index_Of_Quantitystar = substring_Without_ITM.IndexOf('*');
                    String substring_Without_Itemcode = substring_Without_ITM.Substring(index_Of_Quantitystar + 1);
                    int index_Of_Star = substring_Without_Itemcode.IndexOf('*');
                    String Qunatity = substring_Without_Itemcode.Split('*')[0];

                    String substring_Contain_PackagingType = substring_Without_Itemcode.Substring(index_Of_Star + 1);

                    int index_Of_Casestar = substring_Contain_PackagingType.IndexOf('*');
                    unit_Case_Name = substring_Contain_PackagingType.Split('*')[0];


                    String unit_Price = substring_Contain_PackagingType.Substring(index_Of_Casestar + 1);

                    int index_Of_Unit_Pricestar = unit_Price.IndexOf('*');
                    String unitprice = unit_Price.Split('*')[0];

                    String discount_Amount = unit_Price.Substring(index_Of_Unit_Pricestar + 1);

                    int discount = discount_Amount.IndexOf('*');
                    String discountamount = discount_Amount.Split('*')[0];



                    String total_Price = discount_Amount.Substring(discount + 1);

                    int totalprice = total_Price.IndexOf('*');
                    String totalAmountprice = total_Price.Split('~')[0];

                    int items_Qunatity = Int16.Parse(Qunatity);

                    string items_Currency = "";

                    char[] ch = totalAmountprice.ToCharArray();
                    for (int k = 0; k < ch.Length; k++)
                    {
                        if (Char.IsDigit(ch[k]) || ch[k] == '.')
                        {

                        }
                        else
                        {
                            items_Currency = ch[k].ToString();
                        }

                    }


                    int indext = totalAmountprice.IndexOf(items_Currency);
                    String items_Total_Amount1 = totalAmountprice.Substring(indext + 1);

                    int indextt = unitprice.IndexOf(items_Currency);
                    String items_Price1 = unitprice.Substring(indextt + 1);

                    int indexttt = discountamount.IndexOf(items_Currency);
                    String items_Discount1 = discountamount.Substring(indexttt + 1);

                    float items_Total_Amount = float.Parse(items_Total_Amount1);
                    float items_Price = float.Parse(items_Price1);
                    float items_Discount = float.Parse(items_Discount1);

                    list_Object.Add(new Product_Entity()
                    {
                        Unit = unit_Case_Name,
                        ItemCode = items_Code,
                        Quantity = items_Qunatity,
                        Total = items_Total_Amount,
                        Price = items_Price,
                        Discount = items_Discount,
                        Currency = items_Currency
                    });
                }

                //Item Description

                if (edi_File_LineArray[p].Contains("ITM1DESC*"))
                {
                    item_Description.Add((edi_File_LineArray[p].Substring(9)).Split('~')[0]);
                }


                //Vendor Name
                if (edi_File_LineArray[p].Contains("VN*"))
                {

                    vendor_Name = (edi_File_LineArray[p].Substring(3)).Split('~')[0];

                }

                //Vendor Address

                if (edi_File_LineArray[p].Contains("VADD*"))
                {
                    vendor_Address = (edi_File_LineArray[p].Substring(5)).Split('~')[0];

                }

                //Vendor Address
                //Vendor City*Vendor State*Vendor Country


                if (edi_File_LineArray[p].Contains("VADD2*"))
                {
                    vendor_Address2 = (edi_File_LineArray[p].Substring(6)).Split('~')[0];

                    vendor_City_Name = vendor_Address2.Split('*')[0];
                    int index_Of_Star = vendor_Address2.IndexOf('*');
                    if (index_Of_Star >= 0)
                        vendor_State_Name = vendor_Address2.Substring(index_Of_Star + 1);
                }

                //Invoice Details
                if (edi_File_LineArray[p].Contains("INV*"))
                {
                    substring_Without_INV = (edi_File_LineArray[4].Substring(4)).Split('~')[0];
                    invoice_Number = substring_Without_INV.Split('*')[0];
                    int index_Of_Star = substring_Without_INV.IndexOf('*');
                    if (index_Of_Star >= 0)
                        invoice_Date = substring_Without_INV.Substring(index_Of_Star + 1);

                }

                //Vendor Contact Details
                //VCD*Phone Number*Email ID*Alternate Email ID *Vendor Fax No
                if (edi_File_LineArray[p].Contains("VCD*"))
                {
                    substring_Without_VCD = (edi_File_LineArray[p].Substring(4)).Split('~')[0];
                    substring_Without_VCD2 = substring_Without_VCD;
                    index_Of_Vendor_Email1 = substring_Without_VCD.IndexOf("EID1");
                    index_Of_Vendor_Email2 = substring_Without_VCD.IndexOf("*EID2");
                    vendor_Email_Address1 = substring_Without_VCD.Substring(index_Of_Vendor_Email1 + 5, (index_Of_Vendor_Email2 - index_Of_Vendor_Email1) - 4);
                    vendor_Email_Address2 = substring_Without_VCD2.Substring(substring_Without_VCD2.IndexOf("*EID2") + 6);
                    vendor_Phone_Number = (substring_Without_VCD.Substring(3)).Split('*')[0];
                }


                //Vendor Zip 

                if (edi_File_LineArray[p].Contains("VZ*"))
                {
                    vendor_Zip = (edi_File_LineArray[p].Substring(3)).Split('~')[0];
                }



                //Vendor Tax
                if (edi_File_LineArray[p].Contains("VTXN*"))
                {
                    vendor_Tax = (edi_File_LineArray[p].Substring(5)).Split('~')[0];
                }


                //Customer Details
                //CN*Customer Name *Customer ID

                if (edi_File_LineArray[p].Contains("CN*"))
                {
                    substring_Without_CN = (edi_File_LineArray[p].Substring(3)).Split('~')[0];
                    customer_Name = substring_Without_CN.Split('*')[0];
                    int index_Of_Star = substring_Without_CN.IndexOf('*');
                    if (index_Of_Star >= 0)
                        customer_Id = substring_Without_CN.Substring(index_Of_Star + 1);
                }


                //Customer Address 1

                if (edi_File_LineArray[p].Contains("CADD*"))
                {
                    customer_Address = (edi_File_LineArray[p].Substring(5)).Split('~')[0];
                }



                //Customer Address 2
                //Customer City* Customer State* Customer Country

                if (edi_File_LineArray[p].Contains("CADD2*"))
                {
                    customer_Address2 = (edi_File_LineArray[p].Substring(6)).Split('~')[0];
                    customer_City = customer_Address2.Split('*')[0];

                    int index_Of_Star = customer_Address2.IndexOf('*');
                    if (index_Of_Star >= 0)
                        customer_State = customer_Address2.Substring(index_Of_Star + 1);
                }



                //Customer Contact Details
                //CCD*Phone Number*Email ID*Alternate Email ID*Customer Fax Number
                if (edi_File_LineArray[p].Contains("CCD*"))
                {

                    sustring_Without_CCD = (edi_File_LineArray[p].Substring(4)).Split('~')[0];
                    sustring_Without_CCD2 = sustring_Without_CCD;
                    index_Of_Email1 = sustring_Without_CCD.IndexOf("EID1");
                    index_Of_Email2 = sustring_Without_CCD.IndexOf("*EID2");
                    customer_Email_Address1 = sustring_Without_CCD.Substring(index_Of_Email1 + 5, (index_Of_Email2 - index_Of_Email1) - 4);
                    customer_Email_Address2 = (sustring_Without_CCD2.Substring(sustring_Without_CCD2.IndexOf("*EID2") + 6)).Split('*')[0];
                    int index_Of_CF = sustring_Without_CCD.IndexOf("CF*");
                    customer_Fax = sustring_Without_CCD.Substring(index_Of_CF + 3);
                    customer_Phone_Number = (sustring_Without_CCD.Substring(3)).Split('*')[0];
                }


                //Total Amount Details
                //Number of items*Discount Amount * Total Amount
                if (edi_File_LineArray[p].Contains("TOT*"))
                {
                    int a = p;

                    int indexof_TOT = edi_File_LineArray[p].IndexOf("TOT*");
                    String substring_Without_TOT = edi_File_LineArray[p].Substring(indexof_TOT + 4);
                    number_Of_Items = substring_Without_TOT.Split('*')[0];

                    int indexof_Star1 = substring_Without_TOT.IndexOf('*');
                    String substring_Discount_Total = substring_Without_TOT.Substring(indexof_Star1 + 1);
                    TOTdiscount = substring_Discount_Total.Split('*')[0];

                    int indexof_Star2 = substring_Discount_Total.IndexOf('*');
                    String substring_Total = substring_Discount_Total.Substring(indexof_Star2 + 1);

                    total_Amount = substring_Total.Split('~')[0];
                }





            }
            List<string> myarray1 = item_Code;
            List<string> myarray2 = item_Qunatity;
            List<string> myarray3 = item_Unitcase_Name;
            List<string> myarray4 = item_UnitPrice;
            List<string> myarray5 = item_Discount_Amount;
            List<string> myarray6 = item_Total_Amount;
            List<string> myarray7 = item_Description;

            EDI_OwnCreated eDI_OwnCreated = new EDI_OwnCreated();
            eDI_OwnCreated.vendor_Name = vendor_Name;
            eDI_OwnCreated.total_Amount = total_Amount;
            eDI_OwnCreated.invoice_Number = invoice_Number;
            eDI_OwnCreated.invoice_Date = invoice_Date;
            eDI_OwnCreated.customer_Name = customer_Name;
            eDI_OwnCreated.unit_Case_Name = unit_Case_Name;
            eDI_OwnCreated.items_Product = list_Object;
            var response_String = eDI_Decryption_Service.EDI_OwnCreated_Decryption(eDI_OwnCreated);
            return response_String;
        }


        [HttpPost, Route("OwnCreated_New")]
        public List<File_Operation_Message> OwnCreated_New([FromBody]List<FileManager> oFiles)
        {
            
            List<File_Operation_Message> lst_operation_Message = new List<File_Operation_Message>();

            EDI_Decryption_Service eDI_Decryption_Service = new EDI_Decryption_Service(configuration);


            for (int i = 0; i < oFiles.Count(); i++)
            {

                

                string file_Path = oFiles[i].FilePath;
                file_Path = file_Path.Replace("ftp://192.168.43.215/", @"C:\EDI_FTP\").Replace("/", "\\");
                string[] edi_File_LineArray = System.IO.File.ReadAllLines(file_Path);
                string filedata = "";

                

                foreach (var item in edi_File_LineArray)
                {
                    filedata += item + @"~MASYCODA~";
                }

                FileInfo edi_file_to_read = new FileInfo(file_Path);

                File_Operation_Message eDI_OwnCreated = new File_Operation_Message();
                eDI_OwnCreated.VendorName = oFiles[i].VendorName;
                eDI_OwnCreated.FileName = oFiles[i].FileName;
                eDI_OwnCreated.FilePath = oFiles[i].FilePath;

                eDI_OwnCreated.Is_success = true;
                eDI_OwnCreated.Other_Data = filedata;


                var response_String = eDI_Decryption_Service.Save_EDI_FIle(eDI_OwnCreated, 0, (System.IO.File.ReadAllBytes(file_Path)));
                long newinsertedid = response_String.Returned_Id;

                //  edi_File_LineArray = System.IO.File.ReadAllLines(file_Path);




                /*
                 *
                 *
                 
                 *
                 *
                 *
                 **/
                #region FileBreakage

                GetVendorEDIFormat(oFiles[i].VendorName);

                //String vendor_Name = "", customer_City = "", customer_State = "", vendor_Zip = "", vendor_Phone_Number = "",
                //   vendor_Email_Address1 = "", vendor_Email_Address2 = "", substring_Without_VCD = "", substring_Without_VCD2 = "",
                //    substring_Without_INV = "", vendor_Address = "", vendor_Address2 = "", vendor_City_Name = "", vendor_State_Name = "",
                //    items_Code, substring_Without_CN = "", customer_Name = "", customer_Id = "", customer_Address = "", sustring_Without_CCD = "", sustring_Without_CCD2 = "",
                //    customer_Email_Address1 = "", customer_Email_Address2 = "", customer_Phone_Number = "", invoice_Number = "", invoice_Date = "";


                //int index_Of_Vendor_Email1, index_Of_Vendor_Email2, index_Of_Email1, index_Of_Email2;

                edi_File_LineArray = System.IO.File.ReadAllLines(file_Path);

                //List<string> item_Code = new List<string>();
                //List<string> item_Qunatity = new List<string>();
                //List<string> item_Unitcase_Name = new List<string>();
                //List<string> item_UnitPrice = new List<string>();
                //List<string> item_Discount_Amount = new List<string>();
                //List<string> item_Total_Amount = new List<string>();
                //List<string> item_Description = new List<string>();
                //List<Product_Entity> list_Object = new List<Product_Entity>();


                //string customer_Address2 = "", unit_Case_Name = "", customer_Fax = "", TOTdiscount, total_Amount = "", vendor_Tax = "", number_Of_Items;

                string edi_code = GetVendorEDIFormat(oFiles[i].VendorName);
                if (String.IsNullOrEmpty(edi_code))
                {
                    lst_operation_Message.Add(new File_Operation_Message() { FilePath = oFiles[i].FilePath, FileName = oFiles[i].FileName, VendorName = oFiles[i].VendorName, Is_success = false, Error_Message = "Vendor code not found" });
                    continue;
                }
                EDI_OwnCreated EDI_File_Process = null;
                switch (edi_code)
                {
                    case "800":
                        EDI_File_Process = Process800(edi_File_LineArray);
                        break;
                    case "894":
                        EDI_File_Process = Process894(edi_File_LineArray);
                        break;
                }

                //for (int p = 0; p < edi_File_LineArray.Length; p++)
                //{
                    
                //    String line_Of_File = edi_File_LineArray[p];
                //    if (edi_File_LineArray[p].Contains("ITM1*"))
                //    {

                //        int index_Of_ITM1 = edi_File_LineArray[p].IndexOf("ITM1*");

                //        items_Code = (edi_File_LineArray[p].Substring(index_Of_ITM1 + 5)).Split('*')[0];

                //        // String d = edi_File_LineArray[p].Substring(edi_File_LineArray[p].IndexOf("ITM1*"), edi_File_LineArray[p].Length);

                //        item_Code.Add(items_Code);

                //        int index_Of_ITM = edi_File_LineArray[p].IndexOf("ITM1*");
                //        String substring_Without_ITM = edi_File_LineArray[p].Substring(index_Of_ITM + 5);
                //        int index_Of_Quantitystar = substring_Without_ITM.IndexOf('*');
                //        String substring_Without_Itemcode = substring_Without_ITM.Substring(index_Of_Quantitystar + 1);
                //        int index_Of_Star = substring_Without_Itemcode.IndexOf('*');
                //        String Qunatity = substring_Without_Itemcode.Split('*')[0];

                //        String substring_Contain_PackagingType = substring_Without_Itemcode.Substring(index_Of_Star + 1);

                //        int index_Of_Casestar = substring_Contain_PackagingType.IndexOf('*');
                //        unit_Case_Name = substring_Contain_PackagingType.Split('*')[0];


                //        String unit_Price = substring_Contain_PackagingType.Substring(index_Of_Casestar + 1);

                //        int index_Of_Unit_Pricestar = unit_Price.IndexOf('*');
                //        String unitprice = unit_Price.Split('*')[0];

                //        String discount_Amount = unit_Price.Substring(index_Of_Unit_Pricestar + 1);

                //        int discount = discount_Amount.IndexOf('*');
                //        String discountamount = discount_Amount.Split('*')[0];



                //        String total_Price = discount_Amount.Substring(discount + 1);

                //        int totalprice = total_Price.IndexOf('*');
                //        String totalAmountprice = total_Price.Split('~')[0];

                //        int items_Qunatity = Int16.Parse(Qunatity);

                //        string items_Currency = "";

                //        char[] ch = totalAmountprice.ToCharArray();
                //        for (int k = 0; k < ch.Length; k++)
                //        {
                //            if (Char.IsDigit(ch[k]) || ch[k] == '.')
                //            {

                //            }
                //            else
                //            {
                //                items_Currency = ch[k].ToString();
                //            }

                //        }


                //        int indext = totalAmountprice.IndexOf(items_Currency);
                //        String items_Total_Amount1 = totalAmountprice.Substring(indext + 1);

                //        int indextt = unitprice.IndexOf(items_Currency);
                //        String items_Price1 = unitprice.Substring(indextt + 1);

                //        int indexttt = discountamount.IndexOf(items_Currency);
                //        String items_Discount1 = discountamount.Substring(indexttt + 1);

                //        float items_Total_Amount = float.Parse(items_Total_Amount1);
                //        float items_Price = float.Parse(items_Price1);
                //        float items_Discount = float.Parse(items_Discount1);

                //        list_Object.Add(new Product_Entity()
                //        {
                //            Unit = unit_Case_Name,
                //            ItemCode = items_Code,
                //            Quantity = items_Qunatity,
                //            Total = items_Total_Amount,
                //            Price = items_Price,
                //            Discount = items_Discount,
                //            Currency = items_Currency
                //        });
                //    }

                //    //Item Description

                //    if (edi_File_LineArray[p].Contains("ITM1DESC*"))
                //    {
                //        item_Description.Add((edi_File_LineArray[p].Substring(9)).Split('~')[0]);
                //    }


                //    //Vendor Name
                //    if (edi_File_LineArray[p].Contains("VN*"))
                //    {

                //        vendor_Name = (edi_File_LineArray[p].Substring(3)).Split('~')[0];

                //    }

                //    //Vendor Address

                //    if (edi_File_LineArray[p].Contains("VADD*"))
                //    {
                //        vendor_Address = (edi_File_LineArray[p].Substring(5)).Split('~')[0];

                //    }

                //    //Vendor Address
                //    //Vendor City*Vendor State*Vendor Country


                //    if (edi_File_LineArray[p].Contains("VADD2*"))
                //    {
                //        vendor_Address2 = (edi_File_LineArray[p].Substring(6)).Split('~')[0];

                //        vendor_City_Name = vendor_Address2.Split('*')[0];
                //        int index_Of_Star = vendor_Address2.IndexOf('*');
                //        if (index_Of_Star >= 0)
                //            vendor_State_Name = vendor_Address2.Substring(index_Of_Star + 1);
                //    }

                //    //Invoice Details
                //    if (edi_File_LineArray[p].Contains("INV*"))
                //    {
                //        substring_Without_INV = (edi_File_LineArray[4].Substring(4)).Split('~')[0];
                //        invoice_Number = substring_Without_INV.Split('*')[0];
                //        int index_Of_Star = substring_Without_INV.IndexOf('*');
                //        if (index_Of_Star >= 0)
                //            invoice_Date = substring_Without_INV.Substring(index_Of_Star + 1);

                //    }

                //    //Vendor Contact Details
                //    //VCD*Phone Number*Email ID*Alternate Email ID *Vendor Fax No
                //    if (edi_File_LineArray[p].Contains("VCD*"))
                //    {
                //        substring_Without_VCD = (edi_File_LineArray[p].Substring(4)).Split('~')[0];
                //        substring_Without_VCD2 = substring_Without_VCD;
                //        index_Of_Vendor_Email1 = substring_Without_VCD.IndexOf("EID1");
                //        index_Of_Vendor_Email2 = substring_Without_VCD.IndexOf("*EID2");
                //        vendor_Email_Address1 = substring_Without_VCD.Substring(index_Of_Vendor_Email1 + 5, (index_Of_Vendor_Email2 - index_Of_Vendor_Email1) - 4);
                //        vendor_Email_Address2 = substring_Without_VCD2.Substring(substring_Without_VCD2.IndexOf("*EID2") + 6);
                //        vendor_Phone_Number = (substring_Without_VCD.Substring(3)).Split('*')[0];
                //    }


                //    //Vendor Zip 

                //    if (edi_File_LineArray[p].Contains("VZ*"))
                //    {
                //        vendor_Zip = (edi_File_LineArray[p].Substring(3)).Split('~')[0];
                //    }



                //    //Vendor Tax
                //    if (edi_File_LineArray[p].Contains("VTXN*"))
                //    {
                //        vendor_Tax = (edi_File_LineArray[p].Substring(5)).Split('~')[0];
                //    }


                //    //Customer Details
                //    //CN*Customer Name *Customer ID

                //    if (edi_File_LineArray[p].Contains("CN*"))
                //    {
                //        substring_Without_CN = (edi_File_LineArray[p].Substring(3)).Split('~')[0];
                //        customer_Name = substring_Without_CN.Split('*')[0];
                //        int index_Of_Star = substring_Without_CN.IndexOf('*');
                //        if (index_Of_Star >= 0)
                //            customer_Id = substring_Without_CN.Substring(index_Of_Star + 1);
                //    }


                //    //Customer Address 1

                //    if (edi_File_LineArray[p].Contains("CADD*"))
                //    {
                //        customer_Address = (edi_File_LineArray[p].Substring(5)).Split('~')[0];
                //    }



                //    //Customer Address 2
                //    //Customer City* Customer State* Customer Country

                //    if (edi_File_LineArray[p].Contains("CADD2*"))
                //    {
                //        customer_Address2 = (edi_File_LineArray[p].Substring(6)).Split('~')[0];
                //        customer_City = customer_Address2.Split('*')[0];

                //        int index_Of_Star = customer_Address2.IndexOf('*');
                //        if (index_Of_Star >= 0)
                //            customer_State = customer_Address2.Substring(index_Of_Star + 1);
                //    }



                //    //Customer Contact Details
                //    //CCD*Phone Number*Email ID*Alternate Email ID*Customer Fax Number
                //    if (edi_File_LineArray[p].Contains("CCD*"))
                //    {

                //        sustring_Without_CCD = (edi_File_LineArray[p].Substring(4)).Split('~')[0];
                //        sustring_Without_CCD2 = sustring_Without_CCD;
                //        index_Of_Email1 = sustring_Without_CCD.IndexOf("EID1");
                //        index_Of_Email2 = sustring_Without_CCD.IndexOf("*EID2");
                //        customer_Email_Address1 = sustring_Without_CCD.Substring(index_Of_Email1 + 5, (index_Of_Email2 - index_Of_Email1) - 4);
                //        customer_Email_Address2 = (sustring_Without_CCD2.Substring(sustring_Without_CCD2.IndexOf("*EID2") + 6)).Split('*')[0];
                //        int index_Of_CF = sustring_Without_CCD.IndexOf("CF*");
                //        customer_Fax = sustring_Without_CCD.Substring(index_Of_CF + 3);
                //        customer_Phone_Number = (sustring_Without_CCD.Substring(3)).Split('*')[0];
                //    }


                //    //Total Amount Details
                //    //Number of items*Discount Amount * Total Amount
                //    if (edi_File_LineArray[p].Contains("TOT*"))
                //    {
                //        int a = p;

                //        int indexof_TOT = edi_File_LineArray[p].IndexOf("TOT*");
                //        String substring_Without_TOT = edi_File_LineArray[p].Substring(indexof_TOT + 4);
                //        number_Of_Items = substring_Without_TOT.Split('*')[0];

                //        int indexof_Star1 = substring_Without_TOT.IndexOf('*');
                //        String substring_Discount_Total = substring_Without_TOT.Substring(indexof_Star1 + 1);
                //        TOTdiscount = substring_Discount_Total.Split('*')[0];

                //        int indexof_Star2 = substring_Discount_Total.IndexOf('*');
                //        String substring_Total = substring_Discount_Total.Substring(indexof_Star2 + 1);

                //        total_Amount = substring_Total.Split('~')[0];
                //    }





                //}
              

                //EDI_OwnCreated EDI_File_Process = new EDI_OwnCreated();
                //EDI_File_Process.vendor_Name = vendor_Name;
                //EDI_File_Process.total_Amount = total_Amount;
                //EDI_File_Process.invoice_Number = invoice_Number;
                //EDI_File_Process.invoice_Date = invoice_Date;
                //EDI_File_Process.customer_Name = customer_Name;
                //EDI_File_Process.unit_Case_Name = unit_Case_Name;
                //EDI_File_Process.items_Product = list_Object;

                if (string.IsNullOrEmpty(EDI_File_Process.vendor_Name))
                {
                    //error need vendor name
                    lst_operation_Message.Add(new File_Operation_Message() { FilePath = oFiles[i].FilePath, FileName = oFiles[i].FileName, VendorName = oFiles[i].VendorName, Is_success = false, Error_Message = "Vendor name not present" });

                }
                else if (string.IsNullOrEmpty(EDI_File_Process.customer_Name))
                {
                    //error need customer name
                    lst_operation_Message.Add(new File_Operation_Message() { FilePath = oFiles[i].FilePath, FileName = oFiles[i].FileName, VendorName = oFiles[i].VendorName, Is_success = false, Error_Message = "Customer name not present" });
                }
                else if (EDI_File_Process.items_Product == null || EDI_File_Process.items_Product.Count() == 0)
                {
                    //error need no list
                    lst_operation_Message.Add(new File_Operation_Message() { FilePath = oFiles[i].FilePath, FileName = oFiles[i].FileName, VendorName = oFiles[i].VendorName, Is_success = false, Error_Message = "Items not present" });
                }
                else
                {

                    var response_String_file_process = eDI_Decryption_Service.EDI_OwnCreated_Decryption(EDI_File_Process);

                    if (response_String_file_process.Is_success == false)
                    {

                    }
                    else
                    {
                        File_Operation_Message updating = new File_Operation_Message();
                        eDI_OwnCreated.VendorName = oFiles[i].VendorName;
                        eDI_OwnCreated.FileName = oFiles[i].FileName;
                        eDI_OwnCreated.FilePath = oFiles[i].FilePath;

                        eDI_OwnCreated.Is_success = (response_String_file_process.Is_success == true) ? true : false;
                        eDI_OwnCreated.Other_Data = filedata;

                        var response_String_update = eDI_Decryption_Service.Save_EDI_FIle(eDI_OwnCreated, newinsertedid, null);

                        edi_file_to_read.Delete();
                    }

                    lst_operation_Message.Add(new File_Operation_Message() { FilePath = oFiles[i].FilePath, FileName = oFiles[i].FileName, VendorName = oFiles[i].VendorName, Is_success = response_String_file_process.Is_success, Error_Message = response_String_file_process.Return_Message });
                }
                #endregion

                /*
                 * *
                 * *
                 * *
                 * *
                 * *
                 * *
                 * *
                 * */



            }





            return lst_operation_Message;
        }

        private EDI_OwnCreated Process894(string[] edi_File_LineArray)
        {
            EDI_OwnCreated EDI_File_Process = new EDI_OwnCreated();

            String vendor_Name = "", customer_City = "", customer_State = "", vendor_Zip = "", vendor_Phone_Number = "",
            vendor_Email_Address1 = "", vendor_Email_Address2 = "", substring_Without_VCD = "", substring_Without_VCD2 = "",
            substring_Without_INV = "", vendor_Address = "", vendor_Address2 = "", vendor_City_Name = "", vendor_State_Name = "",
            items_Code, substring_Without_CN = "", customer_Name = "", customer_Id = "", customer_Address = "", sustring_Without_CCD = "", sustring_Without_CCD2 = "",
            customer_Email_Address1 = "", customer_Email_Address2 = "", customer_Phone_Number = "", invoice_Number = "", invoice_Date = "";

            string customer_Address2 = "", unit_Case_Name = "", customer_Fax = "", TOTdiscount, total_Amount = "", vendor_Tax = "", number_Of_Items;
            int index_Of_Vendor_Email1, index_Of_Vendor_Email2, index_Of_Email1, index_Of_Email2;


            List<string> item_Code = new List<string>();
            List<string> item_Qunatity = new List<string>();
            List<string> item_Unitcase_Name = new List<string>();
            List<string> item_UnitPrice = new List<string>();
            List<string> item_Discount_Amount = new List<string>();
            List<string> item_Total_Amount = new List<string>();
            List<string> item_Description = new List<string>();
            List<Product_Entity> list_Object = new List<Product_Entity>();

            const string LINE_ITEM_MAIN = "G83*";
            const string FUNCTIONAL_GROUP_HEADER = "GS*";
            const string DELIVERY_RETURN_BASE_ADDRESS = "G82*";
            const string ALLOWANCE_OR_CHANGE = "G72*";

            const string SPLITTER = "*";
            
            
            string dsd_no, quantity, unit_of_measure, upc, product_id_qualifier, product_id_number, upc_case_code, item_list_cost, pack, cashregister_item_description;
            string FUNCTIONALIDCODE,APPLICATION_SENDER_CODE,APPLICATION_RECEIVER_CODE,DATE,TIME,GROUP_CONTROL_NUMBER,RESPONSIBLE_AGENCY_CODE,VERSION_RELEASE_ID_CODE;
            string Credit_Debit_Flag_Code, Supplier_Delivery_Return_Number, Receiver_DUNS_Number, Receiver_Location_Number_CustomerID, supplier_DUNS_Number, Supplier_Location_Number, Delivery_Return_Date;
            string ALLOWANCE_OR_CHARGE_CODE, ALLOW_CHARGE_HANDLING_CODE, ALLOW_CHARGE_RATE, UNIT_OF_MEASURE_CODE, ALLOW_CHARGE_QUANTITY, CHANGE_UNIT_OF_MEASURE_CODE, ALLOWANCE_CHARGE_TOTAL_AMOUNT;


            try
            {
                for (int p = 0; p < edi_File_LineArray.Length; p++)
                {

                    String line_Of_File = edi_File_LineArray[p];
                    if (edi_File_LineArray[p].Contains(LINE_ITEM_MAIN))
                    {

                        if (edi_File_LineArray[p].Replace("~", "").StartsWith(LINE_ITEM_MAIN))
                        {
                            string[] arr = edi_File_LineArray[p].Split(SPLITTER);

                            dsd_no = arr[1];
                            quantity = arr[2];
                            unit_of_measure = arr[3];
                            upc = arr[4];
                            product_id_qualifier = arr[5];
                            product_id_number = arr[6];
                            upc_case_code = arr[7];
                            item_list_cost = arr[8];
                            pack = arr[9];
                            cashregister_item_description = arr[10].Replace("~","");
                        }

                        if (edi_File_LineArray[p].StartsWith(FUNCTIONAL_GROUP_HEADER))
                        {
                            string[] arr = edi_File_LineArray[p].Replace("~", "").Split(SPLITTER);

                            FUNCTIONALIDCODE = arr[1];
                            APPLICATION_SENDER_CODE = arr[2];
                            APPLICATION_RECEIVER_CODE = arr[3];
                            DATE = arr[4];
                            TIME = arr[5];
                            GROUP_CONTROL_NUMBER = arr[6];
                            RESPONSIBLE_AGENCY_CODE = arr[7];
                            VERSION_RELEASE_ID_CODE = arr[8];
                        }

                        if (edi_File_LineArray[p].StartsWith(DELIVERY_RETURN_BASE_ADDRESS))
                        {
                            string[] arr = edi_File_LineArray[p].Replace("~", "").Split(SPLITTER);


                            Credit_Debit_Flag_Code = arr[1]; 
                            Supplier_Delivery_Return_Number = arr[2]; 
                            Receiver_DUNS_Number = arr[3]; 
                            Receiver_Location_Number_CustomerID = arr[4]; 
                            supplier_DUNS_Number = arr[5]; 
                            Supplier_Location_Number = arr[6]; 
                            Delivery_Return_Date = arr[7];
                        }


                        if (edi_File_LineArray[p].StartsWith(ALLOWANCE_OR_CHANGE))
                        {
                            string[] arr = edi_File_LineArray[p].Replace("~", "").Split(SPLITTER);


                            ALLOWANCE_OR_CHARGE_CODE = arr[1]; 
                            ALLOW_CHARGE_HANDLING_CODE = arr[2]; 
                            ALLOW_CHARGE_RATE = arr[3]; 
                            UNIT_OF_MEASURE_CODE = arr[4]; 
                            ALLOW_CHARGE_QUANTITY = arr[5]; 
                            CHANGE_UNIT_OF_MEASURE_CODE = arr[6]; 
                            ALLOWANCE_CHARGE_TOTAL_AMOUNT = arr[7];

                        }


                        int index_Of_ITM1 = edi_File_LineArray[p].IndexOf(LINE_ITEM_MAIN);

                        items_Code = (edi_File_LineArray[p].Substring(index_Of_ITM1 + 5)).Split(SPLITTER)[0];
                                                
                        item_Code.Add(items_Code);

                        int index_Of_ITM = edi_File_LineArray[p].IndexOf("ITM1*");
                        String substring_Without_ITM = edi_File_LineArray[p].Substring(index_Of_ITM + 5);
                        int index_Of_Quantitystar = substring_Without_ITM.IndexOf('*');
                        String substring_Without_Itemcode = substring_Without_ITM.Substring(index_Of_Quantitystar + 1);
                        int index_Of_Star = substring_Without_Itemcode.IndexOf('*');
                        String Qunatity = substring_Without_Itemcode.Split('*')[0];

                        String substring_Contain_PackagingType = substring_Without_Itemcode.Substring(index_Of_Star + 1);

                        int index_Of_Casestar = substring_Contain_PackagingType.IndexOf('*');
                        unit_Case_Name = substring_Contain_PackagingType.Split('*')[0];


                        String unit_Price = substring_Contain_PackagingType.Substring(index_Of_Casestar + 1);

                        int index_Of_Unit_Pricestar = unit_Price.IndexOf('*');
                        String unitprice = unit_Price.Split('*')[0];

                        String discount_Amount = unit_Price.Substring(index_Of_Unit_Pricestar + 1);

                        int discount = discount_Amount.IndexOf('*');
                        String discountamount = discount_Amount.Split('*')[0];



                        String total_Price = discount_Amount.Substring(discount + 1);

                        int totalprice = total_Price.IndexOf('*');
                        String totalAmountprice = total_Price.Split('~')[0];

                        int items_Qunatity = Int16.Parse(Qunatity);

                        string items_Currency = "";

                        char[] ch = totalAmountprice.ToCharArray();
                        for (int k = 0; k < ch.Length; k++)
                        {
                            if (Char.IsDigit(ch[k]) || ch[k] == '.')
                            {

                            }
                            else
                            {
                                items_Currency = ch[k].ToString();
                            }

                        }


                        int indext = totalAmountprice.IndexOf(items_Currency);
                        String items_Total_Amount1 = totalAmountprice.Substring(indext + 1);

                        int indextt = unitprice.IndexOf(items_Currency);
                        String items_Price1 = unitprice.Substring(indextt + 1);

                        int indexttt = discountamount.IndexOf(items_Currency);
                        String items_Discount1 = discountamount.Substring(indexttt + 1);

                        float items_Total_Amount = float.Parse(items_Total_Amount1);
                        float items_Price = float.Parse(items_Price1);
                        float items_Discount = float.Parse(items_Discount1);

                        list_Object.Add(new Product_Entity()
                        {
                            Unit = unit_Case_Name,
                            ItemCode = items_Code,
                            Quantity = items_Qunatity,
                            Total = items_Total_Amount,
                            Price = items_Price,
                            Discount = items_Discount,
                            Currency = items_Currency
                        });
                    }

                    //Item Description

                    if (edi_File_LineArray[p].Contains("ITM1DESC*"))
                    {
                        item_Description.Add((edi_File_LineArray[p].Substring(9)).Split('~')[0]);
                    }


                    //Vendor Name
                    if (edi_File_LineArray[p].Contains("VN*"))
                    {

                        vendor_Name = (edi_File_LineArray[p].Substring(3)).Split('~')[0];

                    }

                    //Vendor Address

                    if (edi_File_LineArray[p].Contains("VADD*"))
                    {
                        vendor_Address = (edi_File_LineArray[p].Substring(5)).Split('~')[0];

                    }

                    //Vendor Address
                    //Vendor City*Vendor State*Vendor Country


                    if (edi_File_LineArray[p].Contains("VADD2*"))
                    {
                        vendor_Address2 = (edi_File_LineArray[p].Substring(6)).Split('~')[0];

                        vendor_City_Name = vendor_Address2.Split('*')[0];
                        int index_Of_Star = vendor_Address2.IndexOf('*');
                        if (index_Of_Star >= 0)
                            vendor_State_Name = vendor_Address2.Substring(index_Of_Star + 1);
                    }

                    //Invoice Details
                    if (edi_File_LineArray[p].Contains("INV*"))
                    {
                        substring_Without_INV = (edi_File_LineArray[4].Substring(4)).Split('~')[0];
                        invoice_Number = substring_Without_INV.Split('*')[0];
                        int index_Of_Star = substring_Without_INV.IndexOf('*');
                        if (index_Of_Star >= 0)
                            invoice_Date = substring_Without_INV.Substring(index_Of_Star + 1);

                    }

                    //Vendor Contact Details
                    //VCD*Phone Number*Email ID*Alternate Email ID *Vendor Fax No
                    if (edi_File_LineArray[p].Contains("VCD*"))
                    {
                        substring_Without_VCD = (edi_File_LineArray[p].Substring(4)).Split('~')[0];
                        substring_Without_VCD2 = substring_Without_VCD;
                        index_Of_Vendor_Email1 = substring_Without_VCD.IndexOf("EID1");
                        index_Of_Vendor_Email2 = substring_Without_VCD.IndexOf("*EID2");
                        vendor_Email_Address1 = substring_Without_VCD.Substring(index_Of_Vendor_Email1 + 5, (index_Of_Vendor_Email2 - index_Of_Vendor_Email1) - 4);
                        vendor_Email_Address2 = substring_Without_VCD2.Substring(substring_Without_VCD2.IndexOf("*EID2") + 6);
                        vendor_Phone_Number = (substring_Without_VCD.Substring(3)).Split('*')[0];
                    }


                    //Vendor Zip 

                    if (edi_File_LineArray[p].Contains("VZ*"))
                    {
                        vendor_Zip = (edi_File_LineArray[p].Substring(3)).Split('~')[0];
                    }



                    //Vendor Tax
                    if (edi_File_LineArray[p].Contains("VTXN*"))
                    {
                        vendor_Tax = (edi_File_LineArray[p].Substring(5)).Split('~')[0];
                    }


                    //Customer Details
                    //CN*Customer Name *Customer ID

                    if (edi_File_LineArray[p].Contains("CN*"))
                    {
                        substring_Without_CN = (edi_File_LineArray[p].Substring(3)).Split('~')[0];
                        customer_Name = substring_Without_CN.Split('*')[0];
                        int index_Of_Star = substring_Without_CN.IndexOf('*');
                        if (index_Of_Star >= 0)
                            customer_Id = substring_Without_CN.Substring(index_Of_Star + 1);
                    }


                    //Customer Address 1

                    if (edi_File_LineArray[p].Contains("CADD*"))
                    {
                        customer_Address = (edi_File_LineArray[p].Substring(5)).Split('~')[0];
                    }



                    //Customer Address 2
                    //Customer City* Customer State* Customer Country

                    if (edi_File_LineArray[p].Contains("CADD2*"))
                    {
                        customer_Address2 = (edi_File_LineArray[p].Substring(6)).Split('~')[0];
                        customer_City = customer_Address2.Split('*')[0];

                        int index_Of_Star = customer_Address2.IndexOf('*');
                        if (index_Of_Star >= 0)
                            customer_State = customer_Address2.Substring(index_Of_Star + 1);
                    }



                    //Customer Contact Details
                    //CCD*Phone Number*Email ID*Alternate Email ID*Customer Fax Number
                    if (edi_File_LineArray[p].Contains("CCD*"))
                    {

                        sustring_Without_CCD = (edi_File_LineArray[p].Substring(4)).Split('~')[0];
                        sustring_Without_CCD2 = sustring_Without_CCD;
                        index_Of_Email1 = sustring_Without_CCD.IndexOf("EID1");
                        index_Of_Email2 = sustring_Without_CCD.IndexOf("*EID2");
                        customer_Email_Address1 = sustring_Without_CCD.Substring(index_Of_Email1 + 5, (index_Of_Email2 - index_Of_Email1) - 4);
                        customer_Email_Address2 = (sustring_Without_CCD2.Substring(sustring_Without_CCD2.IndexOf("*EID2") + 6)).Split('*')[0];
                        int index_Of_CF = sustring_Without_CCD.IndexOf("CF*");
                        customer_Fax = sustring_Without_CCD.Substring(index_Of_CF + 3);
                        customer_Phone_Number = (sustring_Without_CCD.Substring(3)).Split('*')[0];
                    }


                    //Total Amount Details
                    //Number of items*Discount Amount * Total Amount
                    if (edi_File_LineArray[p].Contains("TOT*"))
                    {
                        int a = p;

                        int indexof_TOT = edi_File_LineArray[p].IndexOf("TOT*");
                        String substring_Without_TOT = edi_File_LineArray[p].Substring(indexof_TOT + 4);
                        number_Of_Items = substring_Without_TOT.Split('*')[0];

                        int indexof_Star1 = substring_Without_TOT.IndexOf('*');
                        String substring_Discount_Total = substring_Without_TOT.Substring(indexof_Star1 + 1);
                        TOTdiscount = substring_Discount_Total.Split('*')[0];

                        int indexof_Star2 = substring_Discount_Total.IndexOf('*');
                        String substring_Total = substring_Discount_Total.Substring(indexof_Star2 + 1);

                        total_Amount = substring_Total.Split('~')[0];
                    }





                }
            }
            catch (Exception ex)
            {

            }
        }

        public EDI_OwnCreated Process800(string[] edi_File_LineArray)
        {
            EDI_OwnCreated EDI_File_Process = new EDI_OwnCreated();

            String vendor_Name = "", customer_City = "", customer_State = "", vendor_Zip = "", vendor_Phone_Number = "",
               vendor_Email_Address1 = "", vendor_Email_Address2 = "", substring_Without_VCD = "", substring_Without_VCD2 = "",
                substring_Without_INV = "", vendor_Address = "", vendor_Address2 = "", vendor_City_Name = "", vendor_State_Name = "",
                items_Code, substring_Without_CN = "", customer_Name = "", customer_Id = "", customer_Address = "", sustring_Without_CCD = "", sustring_Without_CCD2 = "",
                customer_Email_Address1 = "", customer_Email_Address2 = "", customer_Phone_Number = "", invoice_Number = "", invoice_Date = "";

            string customer_Address2 = "", unit_Case_Name = "", customer_Fax = "", TOTdiscount, total_Amount = "", vendor_Tax = "", number_Of_Items;

            int index_Of_Vendor_Email1, index_Of_Vendor_Email2, index_Of_Email1, index_Of_Email2;

            List<string> item_Code = new List<string>();
            List<string> item_Qunatity = new List<string>();
            List<string> item_Unitcase_Name = new List<string>();
            List<string> item_UnitPrice = new List<string>();
            List<string> item_Discount_Amount = new List<string>();
            List<string> item_Total_Amount = new List<string>();
            List<string> item_Description = new List<string>();
            List<Product_Entity> list_Object = new List<Product_Entity>();


            try
            {
                for (int p = 0; p < edi_File_LineArray.Length; p++)
                {

                    String line_Of_File = edi_File_LineArray[p];
                    if (edi_File_LineArray[p].Contains("ITM1*"))
                    {

                        int index_Of_ITM1 = edi_File_LineArray[p].IndexOf("ITM1*");

                        items_Code = (edi_File_LineArray[p].Substring(index_Of_ITM1 + 5)).Split('*')[0];

                        // String d = edi_File_LineArray[p].Substring(edi_File_LineArray[p].IndexOf("ITM1*"), edi_File_LineArray[p].Length);

                        item_Code.Add(items_Code);

                        int index_Of_ITM = edi_File_LineArray[p].IndexOf("ITM1*");
                        String substring_Without_ITM = edi_File_LineArray[p].Substring(index_Of_ITM + 5);
                        int index_Of_Quantitystar = substring_Without_ITM.IndexOf('*');
                        String substring_Without_Itemcode = substring_Without_ITM.Substring(index_Of_Quantitystar + 1);
                        int index_Of_Star = substring_Without_Itemcode.IndexOf('*');
                        String Qunatity = substring_Without_Itemcode.Split('*')[0];

                        String substring_Contain_PackagingType = substring_Without_Itemcode.Substring(index_Of_Star + 1);

                        int index_Of_Casestar = substring_Contain_PackagingType.IndexOf('*');
                        unit_Case_Name = substring_Contain_PackagingType.Split('*')[0];


                        String unit_Price = substring_Contain_PackagingType.Substring(index_Of_Casestar + 1);

                        int index_Of_Unit_Pricestar = unit_Price.IndexOf('*');
                        String unitprice = unit_Price.Split('*')[0];

                        String discount_Amount = unit_Price.Substring(index_Of_Unit_Pricestar + 1);

                        int discount = discount_Amount.IndexOf('*');
                        String discountamount = discount_Amount.Split('*')[0];



                        String total_Price = discount_Amount.Substring(discount + 1);

                        int totalprice = total_Price.IndexOf('*');
                        String totalAmountprice = total_Price.Split('~')[0];

                        int items_Qunatity = Int16.Parse(Qunatity);

                        string items_Currency = "";

                        char[] ch = totalAmountprice.ToCharArray();
                        for (int k = 0; k < ch.Length; k++)
                        {
                            if (Char.IsDigit(ch[k]) || ch[k] == '.')
                            {

                            }
                            else
                            {
                                items_Currency = ch[k].ToString();
                            }

                        }


                        int indext = totalAmountprice.IndexOf(items_Currency);
                        String items_Total_Amount1 = totalAmountprice.Substring(indext + 1);

                        int indextt = unitprice.IndexOf(items_Currency);
                        String items_Price1 = unitprice.Substring(indextt + 1);

                        int indexttt = discountamount.IndexOf(items_Currency);
                        String items_Discount1 = discountamount.Substring(indexttt + 1);

                        float items_Total_Amount = float.Parse(items_Total_Amount1);
                        float items_Price = float.Parse(items_Price1);
                        float items_Discount = float.Parse(items_Discount1);

                        list_Object.Add(new Product_Entity()
                        {
                            Unit = unit_Case_Name,
                            ItemCode = items_Code,
                            Quantity = items_Qunatity,
                            Total = items_Total_Amount,
                            Price = items_Price,
                            Discount = items_Discount,
                            Currency = items_Currency
                        });
                    }

                    //Item Description

                    if (edi_File_LineArray[p].Contains("ITM1DESC*"))
                    {
                        item_Description.Add((edi_File_LineArray[p].Substring(9)).Split('~')[0]);
                    }


                    //Vendor Name
                    if (edi_File_LineArray[p].Contains("VN*"))
                    {

                        vendor_Name = (edi_File_LineArray[p].Substring(3)).Split('~')[0];

                    }

                    //Vendor Address

                    if (edi_File_LineArray[p].Contains("VADD*"))
                    {
                        vendor_Address = (edi_File_LineArray[p].Substring(5)).Split('~')[0];

                    }

                    //Vendor Address
                    //Vendor City*Vendor State*Vendor Country


                    if (edi_File_LineArray[p].Contains("VADD2*"))
                    {
                        vendor_Address2 = (edi_File_LineArray[p].Substring(6)).Split('~')[0];

                        vendor_City_Name = vendor_Address2.Split('*')[0];
                        int index_Of_Star = vendor_Address2.IndexOf('*');
                        if (index_Of_Star >= 0)
                            vendor_State_Name = vendor_Address2.Substring(index_Of_Star + 1);
                    }

                    //Invoice Details
                    if (edi_File_LineArray[p].Contains("INV*"))
                    {
                        substring_Without_INV = (edi_File_LineArray[4].Substring(4)).Split('~')[0];
                        invoice_Number = substring_Without_INV.Split('*')[0];
                        int index_Of_Star = substring_Without_INV.IndexOf('*');
                        if (index_Of_Star >= 0)
                            invoice_Date = substring_Without_INV.Substring(index_Of_Star + 1);

                    }

                    //Vendor Contact Details
                    //VCD*Phone Number*Email ID*Alternate Email ID *Vendor Fax No
                    if (edi_File_LineArray[p].Contains("VCD*"))
                    {
                        substring_Without_VCD = (edi_File_LineArray[p].Substring(4)).Split('~')[0];
                        substring_Without_VCD2 = substring_Without_VCD;
                        index_Of_Vendor_Email1 = substring_Without_VCD.IndexOf("EID1");
                        index_Of_Vendor_Email2 = substring_Without_VCD.IndexOf("*EID2");
                        vendor_Email_Address1 = substring_Without_VCD.Substring(index_Of_Vendor_Email1 + 5, (index_Of_Vendor_Email2 - index_Of_Vendor_Email1) - 4);
                        vendor_Email_Address2 = substring_Without_VCD2.Substring(substring_Without_VCD2.IndexOf("*EID2") + 6);
                        vendor_Phone_Number = (substring_Without_VCD.Substring(3)).Split('*')[0];
                    }


                    //Vendor Zip 

                    if (edi_File_LineArray[p].Contains("VZ*"))
                    {
                        vendor_Zip = (edi_File_LineArray[p].Substring(3)).Split('~')[0];
                    }



                    //Vendor Tax
                    if (edi_File_LineArray[p].Contains("VTXN*"))
                    {
                        vendor_Tax = (edi_File_LineArray[p].Substring(5)).Split('~')[0];
                    }


                    //Customer Details
                    //CN*Customer Name *Customer ID

                    if (edi_File_LineArray[p].Contains("CN*"))
                    {
                        substring_Without_CN = (edi_File_LineArray[p].Substring(3)).Split('~')[0];
                        customer_Name = substring_Without_CN.Split('*')[0];
                        int index_Of_Star = substring_Without_CN.IndexOf('*');
                        if (index_Of_Star >= 0)
                            customer_Id = substring_Without_CN.Substring(index_Of_Star + 1);
                    }


                    //Customer Address 1

                    if (edi_File_LineArray[p].Contains("CADD*"))
                    {
                        customer_Address = (edi_File_LineArray[p].Substring(5)).Split('~')[0];
                    }



                    //Customer Address 2
                    //Customer City* Customer State* Customer Country

                    if (edi_File_LineArray[p].Contains("CADD2*"))
                    {
                        customer_Address2 = (edi_File_LineArray[p].Substring(6)).Split('~')[0];
                        customer_City = customer_Address2.Split('*')[0];

                        int index_Of_Star = customer_Address2.IndexOf('*');
                        if (index_Of_Star >= 0)
                            customer_State = customer_Address2.Substring(index_Of_Star + 1);
                    }



                    //Customer Contact Details
                    //CCD*Phone Number*Email ID*Alternate Email ID*Customer Fax Number
                    if (edi_File_LineArray[p].Contains("CCD*"))
                    {

                        sustring_Without_CCD = (edi_File_LineArray[p].Substring(4)).Split('~')[0];
                        sustring_Without_CCD2 = sustring_Without_CCD;
                        index_Of_Email1 = sustring_Without_CCD.IndexOf("EID1");
                        index_Of_Email2 = sustring_Without_CCD.IndexOf("*EID2");
                        customer_Email_Address1 = sustring_Without_CCD.Substring(index_Of_Email1 + 5, (index_Of_Email2 - index_Of_Email1) - 4);
                        customer_Email_Address2 = (sustring_Without_CCD2.Substring(sustring_Without_CCD2.IndexOf("*EID2") + 6)).Split('*')[0];
                        int index_Of_CF = sustring_Without_CCD.IndexOf("CF*");
                        customer_Fax = sustring_Without_CCD.Substring(index_Of_CF + 3);
                        customer_Phone_Number = (sustring_Without_CCD.Substring(3)).Split('*')[0];
                    }


                    //Total Amount Details
                    //Number of items*Discount Amount * Total Amount
                    if (edi_File_LineArray[p].Contains("TOT*"))
                    {
                        int a = p;

                        int indexof_TOT = edi_File_LineArray[p].IndexOf("TOT*");
                        String substring_Without_TOT = edi_File_LineArray[p].Substring(indexof_TOT + 4);
                        number_Of_Items = substring_Without_TOT.Split('*')[0];

                        int indexof_Star1 = substring_Without_TOT.IndexOf('*');
                        String substring_Discount_Total = substring_Without_TOT.Substring(indexof_Star1 + 1);
                        TOTdiscount = substring_Discount_Total.Split('*')[0];

                        int indexof_Star2 = substring_Discount_Total.IndexOf('*');
                        String substring_Total = substring_Discount_Total.Substring(indexof_Star2 + 1);

                        total_Amount = substring_Total.Split('~')[0];
                    }





                }


                
                EDI_File_Process.vendor_Name = vendor_Name;
                EDI_File_Process.total_Amount = total_Amount;
                EDI_File_Process.invoice_Number = invoice_Number;
                EDI_File_Process.invoice_Date = invoice_Date;
                EDI_File_Process.customer_Name = customer_Name;
                EDI_File_Process.unit_Case_Name = unit_Case_Name;
                EDI_File_Process.items_Product = list_Object;

            }
            catch (Exception ex)
            {
                return null;
            }
            return EDI_File_Process;
        }

        public string GetVendorEDIFormat(string VendorName)
        {
            string edi_format_code = null;
            try
            {
                EDI_Decryption_Service oEDI_Decryption_Service = new EDI_Decryption_Service(configuration);
                Operation_Message oMsg = oEDI_Decryption_Service.GetVendorConfiguration(VendorName);

                if (oMsg.Is_success == true)
                {
                    edi_format_code = ((EDI_Formats)oMsg.Data_Select).file_code;
                }

            }
            catch (Exception ex)
            {

            }
            return edi_format_code;
        }
    }

    

}