﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EDI_Web_API.Services;
using Microsoft.Extensions.Configuration;
using EDI_Web_API.Entities;

namespace EDI_Web_API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class MappingController : Controller
    {
        private IConfiguration configuration;
        public MappingController(IConfiguration iConfig)
        {
            configuration = iConfig;
        }
        [HttpGet, Route("GetAll_Vendors")]
        public ActionResult<string> Get_AllVendors()
        {
            Mapping_Service mapping_Service = new Mapping_Service(configuration);
            return Json(mapping_Service.GetAll_Vendors());
        }

        [HttpGet, Route("GetAll_VendorMappingFields")]
        public ActionResult<string> GetAll_VendorMappingFields()
        {
            Mapping_Service mapping_Service = new Mapping_Service(configuration);
            return Json(mapping_Service.GetAll_VendorMappingFields());
        }

        [HttpGet, Route("Get_VendorMappingFields")]
        public ActionResult<string> Get_VendorMappingFields(long vendor_Id)
        {
            Mapping_Service mapping_Service = new Mapping_Service(configuration);
            return Json(mapping_Service.Get_VendorMappingFields(vendor_Id));
        }

        [HttpGet, Route("Delete_VendorMappingFields")]
        public ActionResult<string> Delete_VendorMappingFields(long vendor_Id)
        {
            Mapping_Service mapping_Service = new Mapping_Service(configuration);
            return Json(mapping_Service.Delete_VendorMappingFields(vendor_Id));
        }

        [HttpPost, Route("Mapping_InsertUpdate")]
        public JsonResult Mapping_InsertUpdate([FromBody]Mapping_Entity mapping_Entity)
        {
            Mapping_Service mapping_Service = new Mapping_Service(configuration);
            List<Vendor_FieldsMapping_Type> list_Vendor_FieldsMapping_Type = new List<Vendor_FieldsMapping_Type>();

            Vendor_FieldsMapping_Type vendor_FieldsMapping_Type = new Vendor_FieldsMapping_Type();

            vendor_FieldsMapping_Type.Sequence = 1;
            vendor_FieldsMapping_Type.Fields_Id = (long)Mapping_Fields.Invoice_Id;
            vendor_FieldsMapping_Type.Field_Alias = mapping_Entity.Invoice_Id_Alias;
            vendor_FieldsMapping_Type.Mapping_Id = mapping_Entity.Mapping_Id;
            list_Vendor_FieldsMapping_Type.Add(vendor_FieldsMapping_Type);

            vendor_FieldsMapping_Type = new Vendor_FieldsMapping_Type();
            vendor_FieldsMapping_Type.Sequence = 2;
            vendor_FieldsMapping_Type.Fields_Id = (long)Mapping_Fields.Invoice_Date;
            vendor_FieldsMapping_Type.Field_Alias = mapping_Entity.Invoice_Date_Alias;
            list_Vendor_FieldsMapping_Type.Add(vendor_FieldsMapping_Type);

            vendor_FieldsMapping_Type = new Vendor_FieldsMapping_Type();
            vendor_FieldsMapping_Type.Sequence = 3;
            vendor_FieldsMapping_Type.Fields_Id = (long)Mapping_Fields.Customer_Id;
            vendor_FieldsMapping_Type.Field_Alias = mapping_Entity.Customer_Id_Alias;
            list_Vendor_FieldsMapping_Type.Add(vendor_FieldsMapping_Type);

            //vendor_FieldsMapping_Type = new Vendor_FieldsMapping_Type();
            //vendor_FieldsMapping_Type.Fields_Id = (long)Mapping_Fields.Customer_Id;
            //vendor_FieldsMapping_Type.Field_Alias = mapping_Entity.Customer_Id_Alias;
            //list_Vendor_FieldsMapping_Type.Add(vendor_FieldsMapping_Type);

            vendor_FieldsMapping_Type = new Vendor_FieldsMapping_Type();
            vendor_FieldsMapping_Type.Sequence = 4;
            vendor_FieldsMapping_Type.Fields_Id = (long)Mapping_Fields.Loop;
            vendor_FieldsMapping_Type.Field_Alias = "Loop_Alias";// mapping_Entity.Loop_Alias;
            list_Vendor_FieldsMapping_Type.Add(vendor_FieldsMapping_Type);

            vendor_FieldsMapping_Type = new Vendor_FieldsMapping_Type();
            vendor_FieldsMapping_Type.Sequence = 5;
            vendor_FieldsMapping_Type.Fields_Id = (long)Mapping_Fields.Product_Id;
            vendor_FieldsMapping_Type.Field_Alias = mapping_Entity.Product_Id_Alias;
            list_Vendor_FieldsMapping_Type.Add(vendor_FieldsMapping_Type);

            vendor_FieldsMapping_Type = new Vendor_FieldsMapping_Type();
            vendor_FieldsMapping_Type.Sequence = 6;
            vendor_FieldsMapping_Type.Fields_Id = (long)Mapping_Fields.Product_Name;
            vendor_FieldsMapping_Type.Field_Alias = mapping_Entity.Product_Name_Alias;
            list_Vendor_FieldsMapping_Type.Add(vendor_FieldsMapping_Type);

            vendor_FieldsMapping_Type = new Vendor_FieldsMapping_Type();
            vendor_FieldsMapping_Type.Sequence = 7;
            vendor_FieldsMapping_Type.Fields_Id = (long)Mapping_Fields.Product_Description;
            vendor_FieldsMapping_Type.Field_Alias = mapping_Entity.Product_Description_Alias;
            list_Vendor_FieldsMapping_Type.Add(vendor_FieldsMapping_Type);

            vendor_FieldsMapping_Type = new Vendor_FieldsMapping_Type();
            vendor_FieldsMapping_Type.Sequence = 8;
            vendor_FieldsMapping_Type.Fields_Id = (long)Mapping_Fields.Product_Quantity;
            vendor_FieldsMapping_Type.Field_Alias = /*mapping_Entity.Product_Quantity_Alias*/ "glogbal";
            list_Vendor_FieldsMapping_Type.Add(vendor_FieldsMapping_Type);

            vendor_FieldsMapping_Type = new Vendor_FieldsMapping_Type();
            vendor_FieldsMapping_Type.Sequence = 9;
            vendor_FieldsMapping_Type.Fields_Id = (long)Mapping_Fields.Product_Discount;
            vendor_FieldsMapping_Type.Field_Alias = mapping_Entity.Product_Discount_Alias;
            list_Vendor_FieldsMapping_Type.Add(vendor_FieldsMapping_Type);

            vendor_FieldsMapping_Type = new Vendor_FieldsMapping_Type();
            vendor_FieldsMapping_Type.Sequence = 10;
            vendor_FieldsMapping_Type.Fields_Id = (long)Mapping_Fields.Product_Amount;
            vendor_FieldsMapping_Type.Field_Alias = mapping_Entity.Product_Amount_Alias;
            list_Vendor_FieldsMapping_Type.Add(vendor_FieldsMapping_Type);

            vendor_FieldsMapping_Type = new Vendor_FieldsMapping_Type();
            vendor_FieldsMapping_Type.Sequence = 11;
            vendor_FieldsMapping_Type.Fields_Id = (long)Mapping_Fields.Product_Unit_Alias;
            vendor_FieldsMapping_Type.Field_Alias = /*"Product_Unit_Alias";//*/ mapping_Entity.Product_Unit_Alias;
            list_Vendor_FieldsMapping_Type.Add(vendor_FieldsMapping_Type);

            vendor_FieldsMapping_Type = new Vendor_FieldsMapping_Type();
            vendor_FieldsMapping_Type.Sequence = 12;
            vendor_FieldsMapping_Type.Fields_Id = (long)Mapping_Fields.Invoice_Amount_Alias;
            vendor_FieldsMapping_Type.Field_Alias = mapping_Entity.Invoice_Amount_Alias;// mapping_Entity.Product_Unit_Alias;
            list_Vendor_FieldsMapping_Type.Add(vendor_FieldsMapping_Type);

            Mapping_Insert_Update mapping_Insert_Update = new Mapping_Insert_Update();
            mapping_Insert_Update.Vendor_Id = mapping_Entity.Vendor_Id;
            mapping_Insert_Update.Mode = mapping_Entity.Mode;
            mapping_Insert_Update.List_Vendor_FieldsMapping_Type = list_Vendor_FieldsMapping_Type;

            return Json(mapping_Service.Mapping_InsertUpdate(mapping_Insert_Update));
        }
    }
}