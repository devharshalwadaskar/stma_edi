﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using EDI_Web_API.Services;
using EDI_Web_API.Entities;

namespace EDI_Web_API.Controllers
{

    [Produces("application/json")]
    [Route("api/[controller]")]
    public class VendorController : Controller
    {
        private IConfiguration configuration;
        public VendorController(IConfiguration iConfig)
        {
            configuration = iConfig;
        }

        //GetVendors
        [HttpGet, Route("Get_Vendors")]
        public ActionResult<string> Get_Vendors()
        {
            Vendor_Service vendor_Service = new Vendor_Service(configuration);
            return Json(vendor_Service.Get_Vendors());



        }


        //


        //Get Specific Vendors Details
        [HttpGet, Route("Get_VendorDetails")]
        public ActionResult<string> Get_VendorDetails(long vendor_Id)
        {
            Vendor_Service vendor_Service = new Vendor_Service(configuration);
            return Json(vendor_Service.Get_VendorDetails(vendor_Id));



        }



        //DeleteVendors

        [HttpGet, Route("Delete_Vendor")]
        public ActionResult<string> Delete_Vendor(long vendor_Id)
        {

            Vendor_Service vendor_Service = new Vendor_Service(configuration);
            return Json(vendor_Service.Delete_Vendor(vendor_Id));

        }

        //UpdateVendors

        [HttpPost, Route("Update_Vendor")]
        public JsonResult Update_Vendor([FromBody]Vendors vendor)
        {
            Operation_Message operation_Message = new Operation_Message();

            Vendor_Service vendor_Service = new Vendor_Service(configuration);


            return Json(vendor_Service.Update_Vendor(vendor));
        }


        //Insert Vendor

        [HttpPost, Route("Insert_Vendor")]
        public JsonResult Insert_Vendor([FromBody]Vendors vendor)
        {
            Vendor_Service vendor_Service = new Vendor_Service(configuration);
            return Json(vendor_Service.Insert_Vendor(vendor));
        }


        //Get Zip

        [HttpGet, Route("GetAll_Zip")]
        public JsonResult GetAll_Zip(long state_Id, string search_Text)
        {
            Vendor_Service vendor_Service = new Vendor_Service(configuration);
            return Json(vendor_Service.GetAll_Zip(state_Id, search_Text));
        }

    }
}