﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EDI_Web_API.Entities;
using EDI_Web_API.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace EDI_Web_API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        private IConfiguration configuration; 
        public CustomerController(IConfiguration iConfig)
        {
            configuration = iConfig;
        }

        [HttpGet, Route("GetAll_Customer_Types")]
        public ActionResult<string> GetAll_Customer_Types()
        {
            Customer_Service customer_Service = new Customer_Service(configuration);
            return Json(customer_Service.GetAll_Customer_Types()); 
        }

        [HttpGet, Route("GetAll_Customers")]
        public ActionResult<string> GetAll_Customers() 
        {
            Customer_Service customer_Service = new Customer_Service(configuration);
            return Json(customer_Service.GetAll_Customers());
        }
               
        [HttpGet, Route("Get_Customer")]
        public ActionResult<string> Get_Customer(long customer_Id)
        {
            Customer_Service customer_Service = new Customer_Service(configuration);
            return Json(customer_Service.Get_Customer(customer_Id ));
        }

        [HttpGet, Route("Delete_Customer")]
        public ActionResult<string> Delete_Customer(long customer_Id)
        {
            Customer_Service customer_Service = new Customer_Service(configuration);
            return Json(customer_Service.Delete_Customer(customer_Id));
        }

        [HttpPost, Route("InsertUpdate_Customer")]
        public JsonResult Mapping_InsertUpdate([FromBody]Customer_Entity customer_Entity)
        {
            Customer_Service customer_Service = new Customer_Service(configuration);
            return Json(customer_Service.InsertUpdate_Customer(customer_Entity));
        }

        [HttpGet, Route("GetAll_States")]
        public ActionResult<string> GetAll_States(long customer_Id)
        {
            Customer_Service customer_Service = new Customer_Service(configuration);
            return Json(customer_Service.GetAll_States());
        }
    }
}