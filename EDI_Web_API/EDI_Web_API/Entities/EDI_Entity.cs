﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EDI_Web_API.Entities
{
    public class EDI_Entity
    {
        
    }
    public class FileManager
    {
        public int rownum { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string VendorName { get; set; }
        public bool ComingFromVendor { get; set; }
        public bool GoingToVendor { get; set; }
        public bool Isprocessed { get; set; }
        public bool ErrorInProcess { get; set; }
    }

    public class File_Operation_Message
    {
        public Int16 Operation_Type { get; set; }
        public Int64 Returned_Id { get; set; } // Id returned by the function
        public string Return_Message { get; set; } //Error Message 
        public int Return_Message_Id { get; set; } //Error Message  
        public bool Is_success { get; set; } // Success status either 0 or 1 
        public string Other_Data { get; set; } //Keep other data if any present
        public object Data_Select { get; set; } // Data in case of select
        public string Error_Message { get; set; } //Error Message 

        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string VendorName { get; set; }

       
    }
    
    public class EDI_Formats
    {
        public long edi_format_id { get; set; }
        public string edi_format_name { get; set; }
        public string edi_format { get; set; }
        public string file_code { get; set; }
    }
}
