﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EDI_Web_API.Entities
{
    public class Customer_Entity
    {
        public long Customer_Id { get; set; }
        public string Customer_Name { get; set; }
        public string Customer_Code { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Email_Address { get; set; }
        public string Secondary_Email_Address { get; set; } 
        public string Phone_Number { get; set; }
        public string Fax_Number { get; set; }
        public string Zip { get; set; }
        public bool Status { get; set; }
        public long Customer_Type_Id { get; set; }
        public string Customer_Type_Name { get; set; }
        public long State_Id { get; set; }
        public string State { get; set; }
        public string Tax_Number { get; set; }
        public char Mode { get; set; } 
    }

    public class Customer_Type
    {
        public long Customer_Type_Id { get; set; }
        public string Customer_Type_Name { get; set; }
    }

    public class States 
    {
        public long State_Id { get; set; }  
        public string State { get; set; }
    } 
}
