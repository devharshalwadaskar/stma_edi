﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EDI_Web_API.Entities
{
    public class Common_Entity
    {

    }
    public class Operation_Message
    {
        public Int16 Operation_Type { get; set; }
        public Int64 Returned_Id { get; set; } // Id returned by the function
        public string Return_Message { get; set; } //Error Message 
        public int Return_Message_Id { get; set; } //Error Message  
        public bool Is_success { get; set; } // Success status either 0 or 1 
        public string Other_Data { get; set; } //Keep other data if any present
        public object Data_Select { get; set; } // Data in case of select
    }

    enum Mapping_Fields
    {
        Customer_Id = 1,
        Invoice_Id = 2,
        Invoice_Date = 3,
        Loop = 4,
        Product_Id = 5,
        Product_Name = 6,
        Product_Description = 7,
        Product_Quantity = 8,
        Product_Discount = 9,
        Product_Amount = 10,
        Product_Unit_Alias = 11,
        Invoice_Amount_Alias = 12
    }
}
