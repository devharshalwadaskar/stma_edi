﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EDI_Web_API.Entities
{
    public class Product_Entity
    {
        public string Unit { get; set; }
        public string ItemCode { get; set; }
        public float Quantity { get; set; }
        public float Total { get; set; }

        public float Price { get; set; }
        public float Discount { get; set; }
        public string Currency { get; set; }
    }
    public class EDI_OwnCreated
    {
        public List<Product_Entity> items_Product { get; set; }
        public String vendor_Name { get; set; }
        public String total_Amount { get; set; }
        public String invoice_Number { get; set; }
        public String invoice_Date { get; set; }
        public String customer_Name { get; set; }
        public String unit_Case { get; set; }
        public string unit_Case_Name { get; set; }
    }
}
