﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EDI_Web_API.Entities
{
    public class User_Entity
    {
        public long User_Id { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Email_Address { get; set; }
        public string Secondary_Email_Address { get; set; }
        public string Phone_Number { get; set; }
        public string Address { get; set; }
        public string Other_Info { get; set; }
        public string Password { get; set; }
        public long Role_Id { get; set; }
        public string Role_Name { get; set; }
        public List<Role> Role_List { get; set; }
        public char Mode { get; set; }
    }
    public class Role 
    {
        public long Role_Id { get; set; }
        public string Role_Name { get; set; } 
        public bool Checked { get; set; }
    }
}
