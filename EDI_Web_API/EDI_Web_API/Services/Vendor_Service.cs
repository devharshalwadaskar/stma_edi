﻿using Microsoft.EntityFrameworkCore;
using EDI_Web_API.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;


namespace EDI_Web_API.Services
{
    public class Vendor_Service : Microsoft.EntityFrameworkCore.DbContext
    {

        private static IConfiguration configuration;
        public Vendor_Service(IConfiguration iConfig)
        {
            configuration = iConfig;
        }




        // Inserts  vendor 
        public Operation_Message Insert_Vendor(Vendors vendor)
        {
            Operation_Message _operation_Message = new Operation_Message();


            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        sqlConnection.Open();
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "Insert_Vendor";
                        command.Parameters.AddWithValue("@Vendor_Name", vendor.Vendor_Name);
                        command.Parameters.AddWithValue("@Contact_Person", vendor.Contact_Person);
                        command.Parameters.AddWithValue("@Address", vendor.Address);
                        command.Parameters.AddWithValue("@City", vendor.City);
                        command.Parameters.AddWithValue("@State_Id", vendor.State_Id);
                        command.Parameters.AddWithValue("@Country", vendor.Country);
                        command.Parameters.AddWithValue("@Email_Address", vendor.Phone_Number);
                        command.Parameters.AddWithValue("@Secondary_Email_Address", vendor.Secondary_Email_Address);
                        command.Parameters.AddWithValue("@Fax_Number", vendor.Fax_Number);
                        command.Parameters.AddWithValue("@Phone_Number", vendor.Phone_Number);
                        command.Parameters.AddWithValue("@Website", vendor.Website);
                        command.Parameters.AddWithValue("@Tax_Number", vendor.Tax_Number);
                        command.Parameters.AddWithValue("@Vendor_Code", vendor.Vendor_Code);
                        command.Parameters.AddWithValue("@Zip", vendor.Zip_Code);

                        int count_Product_Invoice = command.ExecuteNonQuery();
                        sqlConnection.Close();

                        if (count_Product_Invoice >= 1)
                        {
                            _operation_Message.Return_Message = "Added Successfully";
                            _operation_Message.Is_success = true;

                        }
                        else
                        {
                            _operation_Message.Return_Message = "Added not Successfully";

                        }
                    }
                }
            }

            catch (Exception exception_Message)
            {
                _operation_Message.Return_Message = exception_Message.ToString();
            }
            return _operation_Message;
        }



        // Delete Vendor details 



        public Operation_Message Delete_Vendor(long vendor_Id)
        {


            Operation_Message _operation_Message = new Operation_Message();
            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "Delete_Vendor";
                        sqlConnection.Open();

                        SqlParameter[] sqlParameter = new SqlParameter[3];
                        sqlParameter[0] = new SqlParameter("@Vendor_Id", vendor_Id);
                        sqlParameter[1] = new SqlParameter("@MESSAGE_ID", SqlDbType.Int);
                        sqlParameter[1].Direction = ParameterDirection.Output;
                        sqlParameter[2] = new SqlParameter("@MESSAGE", SqlDbType.VarChar, 8000);
                        sqlParameter[2].Direction = ParameterDirection.Output;
                        if (sqlParameter != null)
                        {
                            command.Parameters.AddRange(sqlParameter);
                        }
                        int i = command.ExecuteNonQuery();   // returns the no. of rows affected by the operation
                        string message = Convert.ToString(sqlParameter[2].Value);
                        int message_Id = 0;
                        if (sqlParameter[1].Value != null)
                        {
                            message_Id = (int)sqlParameter[1].Value;
                        }
                        long id = 0;
                        if (sqlParameter[0].Value != null)
                        {
                            _operation_Message.Returned_Id = id;
                        }
                        _operation_Message.Is_success = true;
                        _operation_Message.Return_Message_Id = message_Id; //returns the Message_Id from the operation
                        _operation_Message.Return_Message = message;

                        //int count_Product_Invoice = command.ExecuteNonQuery();
                        //sqlConnection.Close();

                        //if (count_Product_Invoice >= 1)
                        //{
                        //    _operation_Message.Return_Message = "deleted  Successfully";
                        //    _operation_Message.Is_success = true;

                        //}
                        //else
                        //{
                        //    _operation_Message.Return_Message = "deleted not ";
                        //}
                    }
                }
            }
            catch (Exception _ex)
            {
                _operation_Message.Is_success = false;
                _operation_Message.Returned_Id = 0;
                _operation_Message.Return_Message_Id = 0;
                _operation_Message.Return_Message = _ex.Message + "\n StackTrace: " + _ex.StackTrace;
            }
            return _operation_Message;
        }

        //Update Vendor
        public Operation_Message Update_Vendor(Vendors vendor)
        {
            Operation_Message operation_Message = new Operation_Message();
            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        sqlConnection.Open();
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "Update_Vendor";
                        command.Parameters.AddWithValue("@Vendor_Id", vendor.Vendor_Id);
                        command.Parameters.AddWithValue("@Vendor_Name", vendor.Vendor_Name);
                        command.Parameters.AddWithValue("@Contact_Person", vendor.Contact_Person);
                        command.Parameters.AddWithValue("@Address", vendor.Address);
                        command.Parameters.AddWithValue("@City", vendor.City);
                        command.Parameters.AddWithValue("@State_Id", vendor.State_Id);
                        command.Parameters.AddWithValue("@Country", vendor.Country);
                        command.Parameters.AddWithValue("@Email_Address", vendor.Phone_Number);
                        command.Parameters.AddWithValue("@Secondary_Email_Address", vendor.Secondary_Email_Address);
                        command.Parameters.AddWithValue("@Fax_Number", vendor.Fax_Number);
                        command.Parameters.AddWithValue("@Phone_Number", vendor.Phone_Number);
                        command.Parameters.AddWithValue("@Website", vendor.Website);
                        command.Parameters.AddWithValue("@Tax_Number", vendor.Tax_Number);

                        command.Parameters.AddWithValue("@Vendor_Code", vendor.Vendor_Code);
                        command.Parameters.AddWithValue("@Zip", vendor.Zip_Code);
                        command.Parameters.Add(new SqlParameter("@MESSAGE_ID", SqlDbType.Int));
                        command.Parameters["@MESSAGE_ID"].Direction = ParameterDirection.Output;
                        command.Parameters.Add(new SqlParameter("@MESSAGE", SqlDbType.VarChar, 5000));
                        command.Parameters["@MESSAGE"].Direction = ParameterDirection.Output;
                        int i = command.ExecuteNonQuery();   // returns the no. of rows affected by the operation

                        string message = Convert.ToString(command.Parameters["@MESSAGE"].Value);
                        int message_Id = 0;
                        if (command.Parameters["@MESSAGE_ID"].Value != null)
                        {
                            message_Id = (int)command.Parameters["@MESSAGE_ID"].Value;
                        }
                        long id = 0;
                        if (command.Parameters["@MESSAGE_ID"].Value != null)
                        {
                            operation_Message.Returned_Id = id;
                        }
                        operation_Message.Is_success = true;
                        operation_Message.Return_Message_Id = message_Id; //returns the Message_Id from the operation
                        operation_Message.Return_Message = message;       //returns the Message returned by the operation
                        sqlConnection.Close();

                    }
                }
            }
            catch (Exception exception_Message)
            {
                operation_Message.Return_Message = exception_Message.ToString();
            }
            return operation_Message;
        }


        //view vendor list
        public List<Vendors> Get_Vendors()
        {

            Operation_Message _operation_Message = new Operation_Message();
            List<Vendors> list_Vendor = new List<Vendors>();
            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        DataSet ds = new DataSet();
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "Get_Vendors";
                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;

                        da.Fill(ds);
                        command.Connection.Close();
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                Vendors vendor = new Vendors();
                                vendor.Vendor_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Vendor_Id"]); //Convert.ToInt64(ds.Tables[0].Rows[i]["Vendor_Id"]);
                                vendor.Vendor_Name = Convert.ToString(ds.Tables[0].Rows[i]["Vendor_Name"]);
                                vendor.Address = Convert.ToString(ds.Tables[0].Rows[i]["Address"]);
                                vendor.City = Convert.ToString(ds.Tables[0].Rows[i]["City"]);
                                vendor.Country = Convert.ToString(ds.Tables[0].Rows[i]["Country"]);
                                vendor.Zip_Code = Convert.ToString(ds.Tables[0].Rows[i]["Zip"]);
                                vendor.Phone_Number = Convert.ToString(ds.Tables[0].Rows[i]["Phone_Number"]);
                                vendor.Email_Address = Convert.ToString(ds.Tables[0].Rows[i]["Email_Address"]);
                                vendor.Secondary_Email_Address = Convert.ToString(ds.Tables[0].Rows[i]["Secondary_Email_Address"]);
                                vendor.Fax_Number = Convert.ToString(ds.Tables[0].Rows[i]["Fax_Number"]);

                                vendor.Tax_Number = Convert.ToString(ds.Tables[0].Rows[i]["Tax_Number"]);
                                vendor.Contact_Person = Convert.ToString(ds.Tables[0].Rows[i]["Contact_Person"]);
                                vendor.State = Convert.ToString(ds.Tables[0].Rows[i]["State"]);
                                vendor.State_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["State_Id"]);
                                vendor.Website = Convert.ToString(ds.Tables[0].Rows[i]["Website"]);
                                vendor.Vendor_Code = Convert.ToString(ds.Tables[0].Rows[i]["Vendor_Code"]);
                                list_Vendor.Add(vendor);
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list_Vendor;
        }


        //Get specific vendor details
        public Vendors Get_VendorDetails(long vendor_Id)
        {

            Operation_Message _operation_Message = new Operation_Message();
            Vendors list_Vendor = new Vendors();
            Vendors vendor = new Vendors();
            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        DataSet ds = new DataSet();
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "Get_VendorDetails";
                        command.Parameters.AddWithValue("@Vendor_Id", vendor_Id);
                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;

                        da.Fill(ds);
                        command.Connection.Close();
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                vendor.Vendor_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Vendor_Id"]); //Convert.ToInt64(ds.Tables[0].Rows[i]["Vendor_Id"]);
                                vendor.Vendor_Name = Convert.ToString(ds.Tables[0].Rows[i]["Vendor_Name"]);
                                vendor.Address = Convert.ToString(ds.Tables[0].Rows[i]["Address"]);
                                vendor.City = Convert.ToString(ds.Tables[0].Rows[i]["City"]);
                                vendor.Country = Convert.ToString(ds.Tables[0].Rows[i]["Country"]);
                                vendor.Zip_Code = Convert.ToString(ds.Tables[0].Rows[i]["Zip"]);
                                vendor.Phone_Number = Convert.ToString(ds.Tables[0].Rows[i]["Phone_Number"]);
                                vendor.Email_Address = Convert.ToString(ds.Tables[0].Rows[i]["Email_Address"]);
                                vendor.Secondary_Email_Address = Convert.ToString(ds.Tables[0].Rows[i]["Secondary_Email_Address"]);
                                vendor.Fax_Number = Convert.ToString(ds.Tables[0].Rows[i]["Fax_Number"]);
                                vendor.Tax_Number = Convert.ToString(ds.Tables[0].Rows[i]["Tax_Number"]);
                                vendor.Contact_Person = Convert.ToString(ds.Tables[0].Rows[i]["Contact_Person"]);
                                vendor.State = Convert.ToString(ds.Tables[0].Rows[i]["State"]);
                                vendor.State_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["State_Id"]);
                                vendor.Website = Convert.ToString(ds.Tables[0].Rows[i]["Website"]);
                                vendor.Vendor_Code = Convert.ToString(ds.Tables[0].Rows[i]["Vendor_Code"]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return vendor;
        }
        //GetAll_Zip

        public List<Zips> GetAll_Zip(long state_Id, string search_Text)
        {
            List<Zips> lst_Zips = new List<Zips>();
            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        DataSet ds = new DataSet();
                        DataSet ds_temp = new DataSet();
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "GetAll_Zip";
                        command.Parameters.AddWithValue("@State_Id", state_Id);
                        command.Parameters.AddWithValue("@Search_Text", search_Text);
                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;

                        da.Fill(ds);
                        ds_temp = ds;
                        command.Connection.Close();
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                            {
                                Zips zip = new Zips();
                                zip.Zip_Code_Id = Convert.ToInt64(ds.Tables[0].Rows[j]["Zip_Code_Id"]);
                                zip.Zip_Code = Convert.ToString(ds.Tables[0].Rows[j]["Zip_Code"]);
                                lst_Zips.Add(zip);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst_Zips;
        }

    }
}
