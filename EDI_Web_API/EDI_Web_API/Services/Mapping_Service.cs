﻿using EDI_Web_API.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace EDI_Web_API.Services
{
    public class Mapping_Service : DbContext
    {
        private static IConfiguration configuration;
        public Mapping_Service(IConfiguration iConfig)
        {
            configuration = iConfig;
        }
        public List<Vendor_Ddl> GetAll_Vendors()
        {
            List<Vendor_Ddl> lst_Vendor_Ddl = new List<Vendor_Ddl>();
            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        DataSet ds = new DataSet();
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "GetAll_Vendors";
                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;

                        da.Fill(ds);
                        command.Connection.Close();
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                Vendor_Ddl vendor_Ddl = new Vendor_Ddl();
                                vendor_Ddl.Vendor_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Vendor_Id"]); //Convert.ToInt64(ds.Tables[0].Rows[i]["Vendor_Id"]);
                                vendor_Ddl.Vendor_Name = Convert.ToString(ds.Tables[0].Rows[i]["Vendor_Name"]);//Convert.ToInt64(ds.Tables[0].Rows[i]["Vendor_Name"]);
                                lst_Vendor_Ddl.Add(vendor_Ddl);
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst_Vendor_Ddl;
        }

        public List<Mapping_Entity> GetAll_VendorMappingFields()
        {
            List<Mapping_Entity> lst_Mapping_Entity = new List<Mapping_Entity>();
            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        DataSet ds = new DataSet();
                        DataSet ds_temp = new DataSet();
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "GetAll_VendorMappingFields";
                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;

                        da.Fill(ds);
                        ds_temp = ds;
                        command.Connection.Close();
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                            {
                                Mapping_Entity mapping_Entity = new Mapping_Entity();
                                mapping_Entity.Vendor_Id = Convert.ToInt64(ds.Tables[0].Rows[0]["Vendor_Id"]);
                                mapping_Entity.Vendor_Name = Convert.ToString(ds.Tables[0].Rows[0]["Vendor_Name"]);

                                for (int i = 0; i < ds_temp.Tables[0].Rows.Count; i++)
                                {
                                    goto repeated_check;
                                repeated_check: if (ds_temp.Tables[0].Rows.Count > 0)
                                    {
                                        if ((long)ds_temp.Tables[0].Rows[i]["Vendor_Id"] == mapping_Entity.Vendor_Id)
                                        {
                                            long field_Id = Convert.ToInt64(ds_temp.Tables[0].Rows[i]["Fields_Id"]);
                                            if (field_Id == (long)Mapping_Fields.Customer_Id)
                                            {
                                                mapping_Entity.Customer_Id_Alias = ds_temp.Tables[0].Rows[i]["Field_Alias"].ToString();
                                                ds_temp.Tables[0].Rows.RemoveAt(i);
                                                i = 0;
                                                goto repeated_check;
                                            }
                                            else if (field_Id == (long)Mapping_Fields.Invoice_Id)
                                            {
                                                mapping_Entity.Invoice_Id_Alias = ds_temp.Tables[0].Rows[i]["Field_Alias"].ToString();
                                                ds_temp.Tables[0].Rows.RemoveAt(i);
                                                i = 0;
                                                goto repeated_check;
                                            }
                                            else if (field_Id == (long)Mapping_Fields.Invoice_Date)
                                            {
                                                mapping_Entity.Invoice_Date_Alias = ds_temp.Tables[0].Rows[i]["Field_Alias"].ToString();
                                                ds_temp.Tables[0].Rows.RemoveAt(i);
                                                i = 0;
                                                goto repeated_check;
                                            }
                                            else if (field_Id == (long)Mapping_Fields.Loop)
                                            {
                                                mapping_Entity.Loop_Alias = ds_temp.Tables[0].Rows[i]["Field_Alias"].ToString();
                                                ds_temp.Tables[0].Rows.RemoveAt(i);
                                                i = 0;
                                                goto repeated_check;
                                            }
                                            else if (field_Id == (long)Mapping_Fields.Product_Id)
                                            {
                                                mapping_Entity.Product_Id_Alias = ds_temp.Tables[0].Rows[i]["Field_Alias"].ToString();
                                                ds_temp.Tables[0].Rows.RemoveAt(i);
                                                i = 0;
                                                goto repeated_check;
                                            }
                                            else if (field_Id == (long)Mapping_Fields.Product_Name)
                                            {
                                                mapping_Entity.Product_Name_Alias = ds_temp.Tables[0].Rows[i]["Field_Alias"].ToString();
                                                ds_temp.Tables[0].Rows.RemoveAt(i);
                                                i = 0;
                                            }
                                            else if (field_Id == (long)Mapping_Fields.Product_Description)
                                            {
                                                mapping_Entity.Product_Description_Alias = ds_temp.Tables[0].Rows[i]["Field_Alias"].ToString();
                                                ds_temp.Tables[0].Rows.RemoveAt(i);
                                                i = 0;
                                                goto repeated_check;
                                            }
                                            else if (field_Id == (long)Mapping_Fields.Product_Quantity)
                                            {
                                                mapping_Entity.Product_Quantity_Alias = ds_temp.Tables[0].Rows[i]["Field_Alias"].ToString();
                                                ds_temp.Tables[0].Rows.RemoveAt(i);
                                                i = 0;
                                                goto repeated_check;
                                            }
                                            else if (field_Id == (long)Mapping_Fields.Product_Discount)
                                            {
                                                mapping_Entity.Product_Discount_Alias = ds_temp.Tables[0].Rows[i]["Field_Alias"].ToString();
                                                ds_temp.Tables[0].Rows.RemoveAt(i);
                                                i = 0;
                                                goto repeated_check;
                                            }
                                            else if (field_Id == (long)Mapping_Fields.Product_Amount)
                                            {
                                                mapping_Entity.Product_Amount_Alias = ds_temp.Tables[0].Rows[i]["Field_Alias"].ToString();
                                                ds_temp.Tables[0].Rows.RemoveAt(i);
                                                i = 0;
                                            }
                                            else if (field_Id == (long)Mapping_Fields.Product_Unit_Alias)
                                            {
                                                mapping_Entity.Product_Unit_Alias = ds_temp.Tables[0].Rows[i]["Field_Alias"].ToString();
                                                ds_temp.Tables[0].Rows.RemoveAt(i);
                                                i = 0;
                                                goto repeated_check;
                                            }
                                            else if (field_Id == (long)Mapping_Fields.Invoice_Amount_Alias)
                                            {
                                                mapping_Entity.Product_Unit_Alias = ds_temp.Tables[0].Rows[i]["Field_Alias"].ToString();
                                                ds_temp.Tables[0].Rows.RemoveAt(i);
                                                i = 0;
                                                goto repeated_check;
                                            }
                                        }
                                    }
                                }
                                lst_Mapping_Entity.Add(mapping_Entity);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst_Mapping_Entity;
        }

        public Mapping_Entity Get_VendorMappingFields(long vendor_Id)
        {
            Mapping_Entity mapping_Entity = new Mapping_Entity();
            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        DataSet ds = new DataSet();
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "Get_VendorMappingFields";
                        command.Parameters.AddWithValue("@Vendor_Id", vendor_Id);
                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;

                        da.Fill(ds);
                        command.Connection.Close();
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            mapping_Entity.Vendor_Id = Convert.ToInt64(ds.Tables[0].Rows[0]["Vendor_Id"]);
                            mapping_Entity.Vendor_Name = Convert.ToString(ds.Tables[0].Rows[0]["Vendor_Name"]);

                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                long field_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Fields_Id"]);
                                if (field_Id == (long)Mapping_Fields.Customer_Id)
                                {
                                    mapping_Entity.Customer_Id_Alias = ds.Tables[0].Rows[i]["Field_Alias"].ToString();
                                    mapping_Entity.Mapping_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Mapping_Id"]);
                                }
                                else if (field_Id == (long)Mapping_Fields.Invoice_Id)
                                {
                                    mapping_Entity.Invoice_Id_Alias = ds.Tables[0].Rows[i]["Field_Alias"].ToString();
                                    mapping_Entity.Mapping_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Mapping_Id"]);
                                }
                                else if (field_Id == (long)Mapping_Fields.Invoice_Date)
                                {
                                    mapping_Entity.Invoice_Date_Alias = ds.Tables[0].Rows[i]["Field_Alias"].ToString();
                                    mapping_Entity.Mapping_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Mapping_Id"]);
                                }
                                else if (field_Id == (long)Mapping_Fields.Loop)
                                {
                                    mapping_Entity.Loop_Alias = ds.Tables[0].Rows[i]["Field_Alias"].ToString();
                                    mapping_Entity.Mapping_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Mapping_Id"]);
                                }
                                else if (field_Id == (long)Mapping_Fields.Product_Id)
                                {
                                    mapping_Entity.Product_Id_Alias = ds.Tables[0].Rows[i]["Field_Alias"].ToString();
                                    mapping_Entity.Mapping_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Mapping_Id"]);
                                }
                                else if (field_Id == (long)Mapping_Fields.Product_Name)
                                {
                                    mapping_Entity.Product_Name_Alias = ds.Tables[0].Rows[i]["Field_Alias"].ToString();
                                    mapping_Entity.Mapping_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Mapping_Id"]);
                                }
                                else if (field_Id == (long)Mapping_Fields.Product_Description)
                                {
                                    mapping_Entity.Product_Description_Alias = ds.Tables[0].Rows[i]["Field_Alias"].ToString();
                                    mapping_Entity.Mapping_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Mapping_Id"]);
                                }
                                else if (field_Id == (long)Mapping_Fields.Product_Quantity)
                                {
                                    mapping_Entity.Product_Quantity_Alias = ds.Tables[0].Rows[i]["Field_Alias"].ToString();
                                    mapping_Entity.Mapping_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Mapping_Id"]);
                                }
                                else if (field_Id == (long)Mapping_Fields.Product_Discount)
                                {
                                    mapping_Entity.Product_Discount_Alias = ds.Tables[0].Rows[i]["Field_Alias"].ToString();
                                    mapping_Entity.Mapping_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Mapping_Id"]);
                                }
                                else if (field_Id == (long)Mapping_Fields.Product_Amount)
                                {
                                    mapping_Entity.Product_Amount_Alias = ds.Tables[0].Rows[i]["Field_Alias"].ToString();
                                    mapping_Entity.Mapping_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Mapping_Id"]);
                                }
                                else if (field_Id == (long)Mapping_Fields.Product_Unit_Alias)
                                {
                                    mapping_Entity.Product_Unit_Alias = ds.Tables[0].Rows[i]["Field_Alias"].ToString();
                                    mapping_Entity.Mapping_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Mapping_Id"]);
                                }
                                else if (field_Id == (long)Mapping_Fields.Invoice_Amount_Alias)
                                {
                                    mapping_Entity.Invoice_Amount_Alias = ds.Tables[0].Rows[i]["Field_Alias"].ToString();
                                    mapping_Entity.Mapping_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Mapping_Id"]);
                                }

                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return mapping_Entity;
        }

        // Delete Vendor details 
        public Operation_Message Delete_VendorMappingFields(long vendor_Id)
        {
            Operation_Message _operation_Message = new Operation_Message();
            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "Delete_VendorMappingFields";
                        sqlConnection.Open();
                        SqlParameter[] sqlParameter = new SqlParameter[3];
                        sqlParameter[0] = new SqlParameter("@Vendor_Id", vendor_Id);
                        sqlParameter[1] = new SqlParameter("@MESSAGE_ID", SqlDbType.Int);
                        sqlParameter[1].Direction = ParameterDirection.Output;
                        sqlParameter[2] = new SqlParameter("@MESSAGE", SqlDbType.VarChar, 8000);
                        sqlParameter[2].Direction = ParameterDirection.Output;

                        if (sqlParameter != null)
                        {
                            command.Parameters.AddRange(sqlParameter);
                        }
                        int i = command.ExecuteNonQuery();   // returns the no. of rows affected by the operation
                        string message = Convert.ToString(sqlParameter[2].Value);
                        int message_Id = 0;
                        if (sqlParameter[1].Value != null)
                        {
                            message_Id = (int)sqlParameter[1].Value;
                        }
                        long id = 0;
                        if (sqlParameter[0].Value != null)
                        {
                            _operation_Message.Returned_Id = id;
                        }
                        _operation_Message.Is_success = true;
                        _operation_Message.Return_Message_Id = message_Id; //returns the Message_Id from the operation
                        _operation_Message.Return_Message = message;       //returns the Message returned by the operation
                    }
                }

            }
            catch (Exception _ex)
            {
                _operation_Message.Is_success = false;
                _operation_Message.Returned_Id = 0;
                _operation_Message.Return_Message_Id = 0;
                _operation_Message.Return_Message = _ex.Message + "\n StackTrace: " + _ex.StackTrace;
            }
            return _operation_Message;
        }

        // Inserts and updates vendor details (Used In vendor Controller For Insert And Update Operations)
        public Operation_Message Mapping_InsertUpdate(Mapping_Insert_Update mapping_Insert_Update)
        {
            Operation_Message _operation_Message = new Operation_Message();
            try
            {
                DataTable dt_Fields = mapping_Insert_Update.List_Vendor_FieldsMapping_Type.ConvertToDataTable();
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "Insert_Update_VendorMappingFields";
                        sqlConnection.Open();
                        SqlParameter[] sqlParameter = new SqlParameter[5];
                        sqlParameter[0] = new SqlParameter("@Vendor_FieldsMapping_Type", SqlDbType.Structured) { Value = dt_Fields, TypeName = "dbo.Vendor_FieldsMapping_Type" };

                        sqlParameter[1] = new SqlParameter("@Vendor_Id", mapping_Insert_Update.Vendor_Id);
                        sqlParameter[2] = new SqlParameter("@MODE", mapping_Insert_Update.Mode);
                        sqlParameter[3] = new SqlParameter("@MESSAGE_ID", SqlDbType.Int);
                        sqlParameter[3].Direction = ParameterDirection.Output;
                        sqlParameter[4] = new SqlParameter("@MESSAGE", SqlDbType.VarChar, 8000);
                        sqlParameter[4].Direction = ParameterDirection.Output;

                        if (sqlParameter != null)
                        {
                            command.Parameters.AddRange(sqlParameter);
                        }
                        int i = command.ExecuteNonQuery();   // returns the no. of rows affected by the operation
                        string message = Convert.ToString(sqlParameter[4].Value);
                        int message_Id = 0;
                        if (sqlParameter[3].Value != null)
                        {
                            message_Id = (int)sqlParameter[3].Value;
                        }
                        long id = 0;
                        if (sqlParameter[0].Value != null)
                        {
                            _operation_Message.Returned_Id = id;
                        }
                        _operation_Message.Is_success = true;
                        _operation_Message.Return_Message_Id = message_Id; //returns the Message_Id from the operation
                        _operation_Message.Return_Message = message;       //returns the Message returned by the operation
                    }
                }

            }
            catch (Exception _ex)
            {
                _operation_Message.Is_success = false;
                _operation_Message.Returned_Id = 0;
                _operation_Message.Return_Message_Id = 0;
                _operation_Message.Return_Message = _ex.Message + "\n StackTrace: " + _ex.StackTrace;
            }
            return _operation_Message;
        }
    }
}
