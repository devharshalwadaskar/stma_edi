﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace EDI_Web_API.Services
{
    public static class Utility
    {
        public static DataTable ConvertToDataTable<T>(this List<T> _iList)
        {
            DataTable _dataTable = new DataTable();
            try
            {
                PropertyDescriptorCollection _propertyDescriptorCollection =
                    TypeDescriptor.GetProperties(typeof(T));
                for (int i = 0; i < _propertyDescriptorCollection.Count; i++)
                {
                    PropertyDescriptor propertyDescriptor = _propertyDescriptorCollection[i];
                    Type _type = propertyDescriptor.PropertyType;

                    if (_type.IsGenericType && _type.GetGenericTypeDefinition() == typeof(Nullable<>))
                        _type = Nullable.GetUnderlyingType(_type);


                    _dataTable.Columns.Add(propertyDescriptor.Name, _type);
                }
                object[] _values = new object[_propertyDescriptorCollection.Count];
                foreach (T _iListItem in _iList)
                {
                    for (int i = 0; i < _values.Length; i++)
                    {
                        _values[i] = _propertyDescriptorCollection[i].GetValue(_iListItem);
                    }
                    _dataTable.Rows.Add(_values);
                }
            }
            catch (Exception _ex)
            {
                return null;
            }
            return _dataTable;
        }
    }
}
