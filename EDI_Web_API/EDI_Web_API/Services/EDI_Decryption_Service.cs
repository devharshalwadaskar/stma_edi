﻿using EDI_Web_API.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace EDI_Web_API.Services
{
    public class EDI_Decryption_Service : DbContext
    {
        private static IConfiguration configuration;
        public EDI_Decryption_Service(IConfiguration iConfig) 
        {
            configuration = iConfig;
        }

        public Operation_Message EDI_OwnCreated_Decryption(EDI_OwnCreated eDI_OwnCreated)
        {
            Operation_Message _operation_Message = new Operation_Message();
            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    int messageid;
                    string Message = "";
                    using (var command = sqlConnection.CreateCommand())
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "Insert_Invoice";
                        sqlConnection.Open();

                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Vendorname", eDI_OwnCreated.vendor_Name);
                        command.Parameters.AddWithValue("@Invoice_No", eDI_OwnCreated.invoice_Number);
                        command.Parameters.AddWithValue("@Invoice_Date", eDI_OwnCreated.invoice_Date);

                        int count_Product_Invoice = 0;
                        int index_Total_Amount = eDI_OwnCreated.total_Amount.IndexOf('$');
                        string substring_Total_Amount = eDI_OwnCreated.total_Amount.Substring(index_Total_Amount + 1);

                        command.Parameters.AddWithValue("@Total_Amount", float.Parse(substring_Total_Amount));
                        command.Parameters.AddWithValue("@Customername", eDI_OwnCreated.customer_Name);
                        command.Parameters.Add(new SqlParameter("@MESSAGE_ID", SqlDbType.BigInt) { Direction = ParameterDirection.Output });
                        command.Parameters.Add(new SqlParameter("@MESSAGE", SqlDbType.VarChar) { Size = 4000, Direction = ParameterDirection.Output });



                        int count_Invoice = command.ExecuteNonQuery();

                        messageid = Convert.ToInt32(command.Parameters["@MESSAGE_ID"].Value);
                        Message = Convert.ToString(command.Parameters["@MESSAGE"].Value);

                        sqlConnection.Close();
                        if (messageid == 0)
                        {
                            _operation_Message.Is_success = false;
                            _operation_Message.Return_Message_Id = 0;
                            _operation_Message.Return_Message = Message;

                            return _operation_Message;
                        }
                        else
                        {
                            _operation_Message.Is_success = true;

                            for (int j = 0; j < eDI_OwnCreated.items_Product.Count; j++)
                            {
                                SqlCommand insertinto_Product_Invoice;
                                insertinto_Product_Invoice = new SqlCommand("Insert_Update_Product_Invoice_Details", sqlConnection);
                                insertinto_Product_Invoice.CommandType = CommandType.StoredProcedure;
                                sqlConnection.Open();
                                insertinto_Product_Invoice.Parameters.AddWithValue("@Discount", eDI_OwnCreated.items_Product[j].Discount);
                                insertinto_Product_Invoice.Parameters.AddWithValue("@ItemCode", eDI_OwnCreated.items_Product[j].ItemCode);
                                insertinto_Product_Invoice.Parameters.AddWithValue("@Quantity", eDI_OwnCreated.items_Product[j].Quantity);
                                insertinto_Product_Invoice.Parameters.AddWithValue("@Vendorname", eDI_OwnCreated.vendor_Name);
                                insertinto_Product_Invoice.Parameters.AddWithValue("@Total", eDI_OwnCreated.items_Product[j].Total);

                                count_Product_Invoice = insertinto_Product_Invoice.ExecuteNonQuery();
                                sqlConnection.Close();
                            }


                            if (count_Product_Invoice >= 1 && count_Invoice >= 1)
                            {
                                _operation_Message.Return_Message = "Added Successfully";
                                _operation_Message.Is_success = true;
                            }
                            else
                            {
                                _operation_Message.Return_Message = "Added not Successfully";
                                _operation_Message.Is_success = false;
                            }
                        }
                    }
                }
            }
            catch (Exception exception_Message)
            {
                _operation_Message.Return_Message = exception_Message.ToString();
            }
            return _operation_Message;
        }

        public Operation_Message Save_EDI_FIle(File_Operation_Message eDI_OwnCreated, long processed_edi_file_id = 0, byte[] file_binary = null)
        {
            Operation_Message _operation_Message = new Operation_Message();
            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "insert_edi_file";
                        sqlConnection.Open();


                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@processed_edi_file_id", processed_edi_file_id);
                        command.Parameters.AddWithValue("@vendor_name", eDI_OwnCreated.VendorName);
                        command.Parameters.AddWithValue("@edi_file_name", eDI_OwnCreated.FileName);
                        command.Parameters.AddWithValue("@file_path", eDI_OwnCreated.FilePath);
                        command.Parameters.AddWithValue("@result", eDI_OwnCreated.Is_success);
                        command.Parameters.AddWithValue("@File_data", eDI_OwnCreated.Other_Data);
                        command.Parameters.AddWithValue("@file_binary", file_binary);
                        command.Parameters.Add(new SqlParameter("@outId", SqlDbType.BigInt) { Direction = ParameterDirection.Output });

                        int result = command.ExecuteNonQuery();

                        _operation_Message.Returned_Id = Convert.ToInt64(command.Parameters["@outId"].Value);
                        sqlConnection.Close();

                    }
                }
            }
            catch (Exception exception_Message)
            {
                _operation_Message.Return_Message = exception_Message.ToString();
            }
            return _operation_Message;
        }

        public Operation_Message GetVendorConfiguration(string vendorShortName)
        {
            EDI_Formats oEdi = new EDI_Formats();
            Operation_Message _operation_Message = new Operation_Message();
            try
            {
              
                using (var sqlConnection = new SqlConnection(configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        DataSet ds = new DataSet();
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "GetVendorConfiguration";
                        
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@internal_vendor_short_name", vendorShortName);

                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;

                        da.Fill(ds);
                        command.Connection.Close();


                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                            {
                                oEdi.edi_format_id = Convert.ToInt64(ds.Tables[0].Rows[j]["edi_format_id"]);
                                oEdi.edi_format_name = Convert.ToString(ds.Tables[0].Rows[j]["edi_format_name"]);
                                oEdi.file_code = Convert.ToString(ds.Tables[0].Rows[j]["file_code"]);
                                
                                _operation_Message.Data_Select = oEdi;
                            }
                        }

                    }
                }
            }
            catch (Exception exception_Message)
            {
                _operation_Message.Return_Message = exception_Message.ToString();
            }
            return _operation_Message;
        }
    }

   
}

