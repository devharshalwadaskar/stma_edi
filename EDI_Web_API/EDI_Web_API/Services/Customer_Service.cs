﻿using EDI_Web_API.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;

namespace EDI_Web_API.Services
{
    public class Customer_Service : DbContext
    {
        private static IConfiguration configuration;
        public Customer_Service(IConfiguration iConfig)
        {
            configuration = iConfig;
        }

        //Bind States
        public List<States> GetAll_States() 
        {
            List<States> lst_States = new List<States>(); 
            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        DataSet ds = new DataSet();
                        DataSet ds_temp = new DataSet();
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "GetAll_States";
                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;

                        da.Fill(ds);
                        ds_temp = ds;
                        command.Connection.Close();
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                            {
                                States state = new States();
                                state.State_Id = Convert.ToInt64(ds.Tables[0].Rows[j]["State_Id"]);
                                state.State = Convert.ToString(ds.Tables[0].Rows[j]["State"]);
                                lst_States.Add(state);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst_States;
        }

        //Bind Customer Type
        public List<Customer_Type> GetAll_Customer_Types()
        { 
            List<Customer_Type> lst_Customer_Types = new List<Customer_Type>();
            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        DataSet ds = new DataSet();
                        DataSet ds_temp = new DataSet();
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "GetAll_Customer_Types";
                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;

                        da.Fill(ds);
                        ds_temp = ds;
                        command.Connection.Close();
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                            {
                                Customer_Type customer_Entity = new Customer_Type();
                                customer_Entity.Customer_Type_Id = Convert.ToInt64(ds.Tables[0].Rows[j]["Customer_Type_Id"]);
                                customer_Entity.Customer_Type_Name = Convert.ToString(ds.Tables[0].Rows[j]["Customer_Type"]);
                                lst_Customer_Types.Add(customer_Entity);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst_Customer_Types;
        }

        public List<Customer_Entity> GetAll_Customers()
        {
            List<Customer_Entity> lst_Customer_Entity = new List<Customer_Entity>();
            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        DataSet ds = new DataSet();
                        DataSet ds_temp = new DataSet();
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "GetAll_Customers";
                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;

                        da.Fill(ds);
                        ds_temp = ds;
                        command.Connection.Close();
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                            {
                                Customer_Entity customer_Entity = new Customer_Entity(); 
                                customer_Entity.Customer_Id = Convert.ToInt64(ds.Tables[0].Rows[j]["Customer_Id"]);
                                customer_Entity.Customer_Name = Convert.ToString(ds.Tables[0].Rows[j]["Customer_Name"]);
                                customer_Entity.Customer_Code = Convert.ToString(ds.Tables[0].Rows[j]["Customer_Code"]);
                                customer_Entity.Address = Convert.ToString(ds.Tables[0].Rows[j]["Address"]);
                                customer_Entity.City = DBNull.Value.Equals(ds.Tables[0].Rows[j]["City"])?"": Convert.ToString(ds.Tables[0].Rows[j]["City"]);
                                customer_Entity.Country = DBNull.Value.Equals(ds.Tables[0].Rows[j]["Country"])?"": Convert.ToString(ds.Tables[0].Rows[j]["Country"]);
                                customer_Entity.State_Id =  Convert.ToInt64(ds.Tables[0].Rows[j]["State_Id"]);
                                customer_Entity.State = DBNull.Value.Equals(ds.Tables[0].Rows[j]["State"])?"": Convert.ToString(ds.Tables[0].Rows[j]["State"]);
                                customer_Entity.Email_Address = DBNull.Value.Equals(ds.Tables[0].Rows[j]["Email_Address"]) ?"": Convert.ToString(ds.Tables[0].Rows[j]["Email_Address"]);
                                customer_Entity.Secondary_Email_Address = DBNull.Value.Equals(ds.Tables[0].Rows[j]["Secondary_Email_Address"])?"": Convert.ToString(ds.Tables[0].Rows[j]["Secondary_Email_Address"]);
                                customer_Entity.Phone_Number = DBNull.Value.Equals(ds.Tables[0].Rows[j]["Phone_Number"])?"" : Convert.ToString(ds.Tables[0].Rows[j]["Phone_Number"]);
                                customer_Entity.Fax_Number = DBNull.Value.Equals(ds.Tables[0].Rows[j]["Fax_Number"])?"": Convert.ToString(ds.Tables[0].Rows[j]["Fax_Number"]);
                                customer_Entity.Zip = DBNull.Value.Equals(ds.Tables[0].Rows[j]["Zip"])?"": Convert.ToString(ds.Tables[0].Rows[j]["Zip"]);
                                customer_Entity.Status = Convert.ToBoolean(ds.Tables[0].Rows[j]["Status"]);
                                customer_Entity.Zip = DBNull.Value.Equals(ds.Tables[0].Rows[j]["Zip"])? "" : Convert.ToString(ds.Tables[0].Rows[j]["Zip"]);
                                customer_Entity.Tax_Number = Convert.ToString(ds.Tables[0].Rows[j]["Tax_Number"]);
                                customer_Entity.Customer_Type_Id = Convert.ToInt32(ds.Tables[0].Rows[j]["Customer_Type_Id"]);
                                customer_Entity.Customer_Type_Name = Convert.ToString(ds.Tables[0].Rows[j]["Customer_Type"]);

                                lst_Customer_Entity.Add(customer_Entity);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst_Customer_Entity;
        }

        public Customer_Entity Get_Customer(long customer_Id)
        {
            Customer_Entity customer_Entity = new Customer_Entity();
            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        DataSet ds = new DataSet();
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "Get_Customer";
                        command.Parameters.AddWithValue("@customer_Id", customer_Id);
                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;

                        da.Fill(ds);
                        command.Connection.Close();
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            customer_Entity.Customer_Id = Convert.ToInt64(ds.Tables[0].Rows[0]["Customer_Id"]);
                            customer_Entity.Customer_Name = Convert.ToString(ds.Tables[0].Rows[0]["Customer_Name"]);
                            customer_Entity.Customer_Code = Convert.ToString(ds.Tables[0].Rows[0]["Customer_Code"]);
                            customer_Entity.Address = Convert.ToString(ds.Tables[0].Rows[0]["Address"]);
                            customer_Entity.City = DBNull.Value.Equals(ds.Tables[0].Rows[0]["City"]) ? "" : Convert.ToString(ds.Tables[0].Rows[0]["City"]);
                            customer_Entity.Country = DBNull.Value.Equals(ds.Tables[0].Rows[0]["Country"]) ? "" : Convert.ToString(ds.Tables[0].Rows[0]["Country"]);
                            customer_Entity.State_Id = Convert.ToInt64(ds.Tables[0].Rows[0]["State_Id"]);
                            customer_Entity.State = DBNull.Value.Equals(ds.Tables[0].Rows[0]["State"]) ? "" : Convert.ToString(ds.Tables[0].Rows[0]["State"]);
                            customer_Entity.Email_Address = DBNull.Value.Equals(ds.Tables[0].Rows[0]["Email_Address"]) ? "" : Convert.ToString(ds.Tables[0].Rows[0]["Email_Address"]);
                            customer_Entity.Secondary_Email_Address = DBNull.Value.Equals(ds.Tables[0].Rows[0]["Secondary_Email_Address"]) ? "" : Convert.ToString(ds.Tables[0].Rows[0]["Secondary_Email_Address"]);
                            customer_Entity.Phone_Number = DBNull.Value.Equals(ds.Tables[0].Rows[0]["Phone_Number"]) ? "" : Convert.ToString(ds.Tables[0].Rows[0]["Phone_Number"]);
                            customer_Entity.Fax_Number = DBNull.Value.Equals(ds.Tables[0].Rows[0]["Fax_Number"]) ? "" : Convert.ToString(ds.Tables[0].Rows[0]["Fax_Number"]);
                            customer_Entity.Zip = DBNull.Value.Equals(ds.Tables[0].Rows[0]["Zip"]) ? "" : Convert.ToString(ds.Tables[0].Rows[0]["Zip"]);
                            customer_Entity.Status = Convert.ToBoolean(ds.Tables[0].Rows[0]["Status"]);
                            customer_Entity.Zip = DBNull.Value.Equals(ds.Tables[0].Rows[0]["Zip"]) ? "" : Convert.ToString(ds.Tables[0].Rows[0]["Zip"]);
                            customer_Entity.Tax_Number = Convert.ToString(ds.Tables[0].Rows[0]["Tax_Number"]);
                            customer_Entity.Customer_Type_Id = Convert.ToInt32(ds.Tables[0].Rows[0]["Customer_Type_Id"]);
                            customer_Entity.Customer_Type_Name = Convert.ToString(ds.Tables[0].Rows[0]["Customer_Type"]);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return customer_Entity;
        }

        // Delete Vendor details 
        public Operation_Message Delete_Customer(long customer_Id)
        {
            Operation_Message _operation_Message = new Operation_Message();
            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "Delete_Customer";
                        sqlConnection.Open();
                        SqlParameter[] sqlParameter = new SqlParameter[3];
                        sqlParameter[0] = new SqlParameter("@customer_Id", customer_Id);
                        sqlParameter[1] = new SqlParameter("@MESSAGE_ID", SqlDbType.Int);
                        sqlParameter[1].Direction = ParameterDirection.Output;
                        sqlParameter[2] = new SqlParameter("@MESSAGE", SqlDbType.VarChar, 8000);
                        sqlParameter[2].Direction = ParameterDirection.Output;

                        if (sqlParameter != null)
                        {
                            command.Parameters.AddRange(sqlParameter);
                        }
                        int i = command.ExecuteNonQuery();   // returns the no. of rows affected by the operation
                        string message = Convert.ToString(sqlParameter[2].Value);
                        int message_Id = 0;
                        if (sqlParameter[1].Value != null)
                        {
                            message_Id = (int)sqlParameter[1].Value;
                        }
                        long id = 0;
                        if (sqlParameter[0].Value != null)
                        {
                            _operation_Message.Returned_Id = id;
                        }
                        _operation_Message.Is_success = true;
                        _operation_Message.Return_Message_Id = message_Id; //returns the Message_Id from the operation
                        _operation_Message.Return_Message = message;       //returns the Message returned by the operation
                    }
                }

            }
            catch (Exception _ex)
            {
                _operation_Message.Is_success = false;
                _operation_Message.Returned_Id = 0;
                _operation_Message.Return_Message_Id = 0;
                _operation_Message.Return_Message = _ex.Message + "\n StackTrace: " + _ex.StackTrace;
            }
            return _operation_Message;
        }

        // Inserts and updates customer details (Used In vendor Controller For Insert And Update Operations)
        public Operation_Message InsertUpdate_Customer(Customer_Entity customer_Entity)
        {
            Operation_Message _operation_Message = new Operation_Message();
            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "Insert_Update_Customer";
                        sqlConnection.Open();

                        command.Parameters.Add(new SqlParameter("@Customer_Id", customer_Entity.Customer_Id));
                        command.Parameters.Add(new SqlParameter("@Customer_Name", customer_Entity.Customer_Name));
                        command.Parameters.Add(new SqlParameter("@Customer_Code", customer_Entity.Customer_Code));
                        command.Parameters.Add(new SqlParameter("@Address", customer_Entity.Address));
                        command.Parameters.Add(new SqlParameter("@City", customer_Entity.City));
                        command.Parameters.Add(new SqlParameter("@Country", customer_Entity.Country));
                        command.Parameters.Add(new SqlParameter("@State_Id", customer_Entity.State_Id));
                        command.Parameters.Add(new SqlParameter("@Email_Address", customer_Entity.Email_Address));
                        command.Parameters.Add(new SqlParameter("@Secondary_Email_Address", customer_Entity.Secondary_Email_Address));
                        command.Parameters.Add(new SqlParameter("@Phone_Number", customer_Entity.Phone_Number));
                        command.Parameters.Add(new SqlParameter("@Fax_Number", customer_Entity.Fax_Number));
                        command.Parameters.Add(new SqlParameter("@Zip", customer_Entity.Zip));
                        command.Parameters.Add(new SqlParameter("@Status", customer_Entity.Status));
                        command.Parameters.Add(new SqlParameter("@Tax_Number", customer_Entity.Tax_Number));
                        command.Parameters.Add(new SqlParameter("@Customer_Type_Id", customer_Entity.Customer_Type_Id));
                        command.Parameters.Add(new SqlParameter("@MODE", customer_Entity.Mode));
                        command.Parameters.Add(new SqlParameter("@MESSAGE_ID", SqlDbType.Int));
                        command.Parameters["@MESSAGE_ID"].Direction = ParameterDirection.Output;
                        command.Parameters.Add(new SqlParameter("@MESSAGE", SqlDbType.VarChar,5000));
                        command.Parameters["@MESSAGE"].Direction = ParameterDirection.Output;


                        int i = command.ExecuteNonQuery();   // returns the no. of rows affected by the operation

                        string message = Convert.ToString(command.Parameters["@MESSAGE"].Value);
                        int message_Id = 0;
                        if (command.Parameters["@MESSAGE_ID"].Value != null)
                        {
                            message_Id = (int)command.Parameters["@MESSAGE_ID"].Value;
                        }
                        long id = 0;
                        if (command.Parameters["@MESSAGE_ID"].Value != null)
                        {
                            _operation_Message.Returned_Id = id;
                        }
                        _operation_Message.Is_success = true;
                        _operation_Message.Return_Message_Id = message_Id; //returns the Message_Id from the operation
                        _operation_Message.Return_Message = message;       //returns the Message returned by the operation
                    }
                }

            }
            catch (Exception _ex)
            {
                _operation_Message.Is_success = false;
                _operation_Message.Returned_Id = 0;
                _operation_Message.Return_Message_Id = 0;
                _operation_Message.Return_Message = _ex.Message + "\n StackTrace: " + _ex.StackTrace;
            }
            return _operation_Message;
        }
    }
}
