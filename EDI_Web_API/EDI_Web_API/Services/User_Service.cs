﻿using EDI_Web_API.Entities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace EDI_Web_API.Services
{
    public class User_Service : DbContext
    {
        private static IConfiguration configuration;
        public User_Service(IConfiguration iConfig) 
        {
            configuration = iConfig;
        }

        public List<Role> GetAll_Roles()
        {
            List<Role> lst_Role = new List<Role>();
            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        DataSet ds = new DataSet();
                        DataSet ds_temp = new DataSet();
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "GetAll_Roles";
                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;

                        da.Fill(ds);
                        ds_temp = ds;
                        command.Connection.Close();
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                            {
                                Role role = new Role();
                                role.Role_Id = Convert.ToInt64(ds.Tables[0].Rows[j]["Role_Id"]);
                                role.Role_Name = Convert.ToString(ds.Tables[0].Rows[j]["Role"]);
                                lst_Role.Add(role);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst_Role;
        }

        public User_Entity Get_User(long user_Id)
        {
            User_Entity user_Entity = new User_Entity();

            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        DataSet ds = new DataSet();
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "Get_User";
                        command.Parameters.AddWithValue("@user_Id", user_Id);
                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;

                        da.Fill(ds);
                        command.Connection.Close();
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            user_Entity.User_Id = Convert.ToInt64(ds.Tables[0].Rows[0]["User_Id"]);
                            user_Entity.First_Name = Convert.ToString(ds.Tables[0].Rows[0]["First_Name"]);
                            user_Entity.Last_Name = Convert.ToString(ds.Tables[0].Rows[0]["Last_Name"]);
                            user_Entity.Address = Convert.ToString(ds.Tables[0].Rows[0]["Address"]);
                            user_Entity.Email_Address = Convert.ToString(ds.Tables[0].Rows[0]["Email_Address"]);
                            user_Entity.Secondary_Email_Address = DBNull.Value.Equals(ds.Tables[0].Rows[0]["Secondary_Email_Address"]) ? "" : Convert.ToString(ds.Tables[0].Rows[0]["Secondary_Email_Address"]);
                            user_Entity.Other_Info = DBNull.Value.Equals(ds.Tables[0].Rows[0]["Other_Info"]) ? "" : Convert.ToString(ds.Tables[0].Rows[0]["Other_Info"]);
                            user_Entity.Password = DBNull.Value.Equals(ds.Tables[0].Rows[0]["Password"]) ? "" : Convert.ToString(ds.Tables[0].Rows[0]["Password"]);
                            user_Entity.Email_Address = DBNull.Value.Equals(ds.Tables[0].Rows[0]["Email_Address"]) ? "" : Convert.ToString(ds.Tables[0].Rows[0]["Email_Address"]);
                            user_Entity.Secondary_Email_Address = DBNull.Value.Equals(ds.Tables[0].Rows[0]["Secondary_Email_Address"]) ? "" : Convert.ToString(ds.Tables[0].Rows[0]["Secondary_Email_Address"]);
                            user_Entity.Phone_Number = DBNull.Value.Equals(ds.Tables[0].Rows[0]["Phone_Number"]) ? "" : Convert.ToString(ds.Tables[0].Rows[0]["Phone_Number"]);
                            user_Entity.Role_Name = DBNull.Value.Equals(ds.Tables[0].Rows[0]["Role"]) ? "" : Convert.ToString(ds.Tables[0].Rows[0]["Role"]);
                            user_Entity.Role_Id = Convert.ToInt64(ds.Tables[0].Rows[0]["Role_Id"]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return user_Entity;
        }

        public List<User_Entity> GetAll_Users()
        {
            List<User_Entity> lst_users = new List<User_Entity>();

            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        DataSet ds = new DataSet();
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "GetAll_Users";
                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;

                        da.Fill(ds);
                        command.Connection.Close();
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                User_Entity user = new User_Entity();

                                user.User_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["User_Id"]);
                                user.First_Name = Convert.ToString(ds.Tables[0].Rows[i]["First_Name"]);
                                user.Last_Name = Convert.ToString(ds.Tables[0].Rows[i]["Last_Name"]);
                                user.Address = Convert.ToString(ds.Tables[0].Rows[i]["Address"]);
                                user.Email_Address = Convert.ToString(ds.Tables[0].Rows[i]["Email_Address"]);
                                user.Secondary_Email_Address = DBNull.Value.Equals(ds.Tables[0].Rows[i]["Secondary_Email_Address"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Secondary_Email_Address"]);
                                user.Other_Info = DBNull.Value.Equals(ds.Tables[0].Rows[i]["Other_Info"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Other_Info"]);
                                user.Password = DBNull.Value.Equals(ds.Tables[0].Rows[i]["Password"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Password"]);
                                user.Email_Address = DBNull.Value.Equals(ds.Tables[0].Rows[i]["Email_Address"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Email_Address"]);
                                user.Secondary_Email_Address = DBNull.Value.Equals(ds.Tables[0].Rows[i]["Secondary_Email_Address"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Secondary_Email_Address"]);
                                user.Role_Name = DBNull.Value.Equals(ds.Tables[0].Rows[0]["Role"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Role"]);
                                user.Role_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Role_Id"]);
                                lst_users.Add(user);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst_users;
        }

        // Delete Vendor details 
        public Operation_Message Delete_User(long user_Id)
        {
            Operation_Message _operation_Message = new Operation_Message();
            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "Delete_User";
                        sqlConnection.Open();
                        SqlParameter[] sqlParameter = new SqlParameter[3];
                        sqlParameter[0] = new SqlParameter("@User_Id", user_Id);
                        sqlParameter[1] = new SqlParameter("@MESSAGE_ID", SqlDbType.Int);
                        sqlParameter[1].Direction = ParameterDirection.Output;
                        sqlParameter[2] = new SqlParameter("@MESSAGE", SqlDbType.VarChar, 8000);
                        sqlParameter[2].Direction = ParameterDirection.Output;

                        if (sqlParameter != null)
                        {
                            command.Parameters.AddRange(sqlParameter);
                        }
                        int i = command.ExecuteNonQuery();   // returns the no. of rows affected by the operation
                        string message = Convert.ToString(sqlParameter[2].Value);
                        int message_Id = 0;
                        if (sqlParameter[1].Value != null)
                        {
                            message_Id = (int)sqlParameter[1].Value;
                        }
                        long id = 0;
                        if (sqlParameter[0].Value != null)
                        {
                            _operation_Message.Returned_Id = id;
                        }
                        _operation_Message.Is_success = true;
                        _operation_Message.Return_Message_Id = message_Id; //returns the Message_Id from the operation
                        _operation_Message.Return_Message = message;       //returns the Message returned by the operation
                    }
                }

            }
            catch (Exception _ex)
            {
                _operation_Message.Is_success = false;
                _operation_Message.Returned_Id = 0;
                _operation_Message.Return_Message_Id = 0;
                _operation_Message.Return_Message = _ex.Message + "\n StackTrace: " + _ex.StackTrace;
            }
            return _operation_Message;
        }

        // Inserts and updates user details (Used In vendor Controller For Insert And Update Operations)
        public Operation_Message InsertUpdate_User(User_Entity user_Entity)
        {
            Operation_Message _operation_Message = new Operation_Message();
            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "Insert_Update_User";
                        sqlConnection.Open();

                        command.Parameters.Add(new SqlParameter("@User_Id", user_Entity.User_Id));
                        command.Parameters.Add(new SqlParameter("@First_Name", user_Entity.First_Name));
                        command.Parameters.Add(new SqlParameter("@Last_Name", user_Entity.Last_Name));
                        command.Parameters.Add(new SqlParameter("@Address", user_Entity.Address));
                        command.Parameters.Add(new SqlParameter("@Phone_Number", user_Entity.Phone_Number));
                        command.Parameters.Add(new SqlParameter("@Email_Address", user_Entity.Email_Address));
                        command.Parameters.Add(new SqlParameter("@Secondary_Email_Address", user_Entity.Secondary_Email_Address));
                        command.Parameters.Add(new SqlParameter("@Other_Info", user_Entity.Other_Info));
                        command.Parameters.Add(new SqlParameter("@Password", user_Entity.Password));
                        command.Parameters.Add(new SqlParameter("@Role_Id", user_Entity.Role_Id));
                        command.Parameters.Add(new SqlParameter("@MODE", user_Entity.Mode));
                        command.Parameters.Add(new SqlParameter("@MESSAGE_ID", SqlDbType.Int));
                        command.Parameters["@MESSAGE_ID"].Direction = ParameterDirection.Output;
                        command.Parameters.Add(new SqlParameter("@MESSAGE", SqlDbType.VarChar, 5000));
                        command.Parameters["@MESSAGE"].Direction = ParameterDirection.Output;

                        int i = command.ExecuteNonQuery();   // returns the no. of rows affected by the operation

                        string message = Convert.ToString(command.Parameters["@MESSAGE"].Value);
                        int message_Id = 0;
                        if (command.Parameters["@MESSAGE_ID"].Value != null)
                        {
                            message_Id = (int)command.Parameters["@MESSAGE_ID"].Value;
                        }
                        long id = 0;
                        if (command.Parameters["@MESSAGE_ID"].Value != null)
                        {
                            _operation_Message.Returned_Id = id;
                        }
                        _operation_Message.Is_success = true;
                        _operation_Message.Return_Message_Id = message_Id; //returns the Message_Id from the operation
                        _operation_Message.Return_Message = message;       //returns the Message returned by the operation
                    }
                }

            }
            catch (Exception _ex)
            {
                _operation_Message.Is_success = false;
                _operation_Message.Returned_Id = 0;
                _operation_Message.Return_Message_Id = 0;
                _operation_Message.Return_Message = _ex.Message + "\n StackTrace: " + _ex.StackTrace;
            }
            return _operation_Message;
        }

        public List<User_Entity> Search_User(string name)
        {
            List<User_Entity> lst_Users = new List<User_Entity>();

            try
            {
                var s = configuration.GetSection("ConnectionString").GetSection("EDI_ConnectionString").Value;
                using (var sqlConnection = new SqlConnection(s))
                {
                    using (var command = sqlConnection.CreateCommand())
                    {
                        DataSet ds = new DataSet();
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = "Search_User";
                        command.Parameters.AddWithValue("@user", name);
                        sqlConnection.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = command;

                        da.Fill(ds);
                        command.Connection.Close();
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                User_Entity user = new User_Entity();

                                user.User_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["User_Id"]);
                                user.First_Name = Convert.ToString(ds.Tables[0].Rows[i]["First_Name"]);
                                user.Last_Name = Convert.ToString(ds.Tables[0].Rows[i]["Last_Name"]);
                                user.Address = Convert.ToString(ds.Tables[0].Rows[i]["Address"]);
                                user.Email_Address = Convert.ToString(ds.Tables[0].Rows[i]["Email_Address"]);
                                user.Secondary_Email_Address = DBNull.Value.Equals(ds.Tables[0].Rows[i]["Secondary_Email_Address"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Secondary_Email_Address"]);
                                user.Other_Info = DBNull.Value.Equals(ds.Tables[0].Rows[i]["Other_Info"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Other_Info"]);
                                user.Password = DBNull.Value.Equals(ds.Tables[0].Rows[i]["Password"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Password"]);
                                user.Email_Address = DBNull.Value.Equals(ds.Tables[0].Rows[i]["Email_Address"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Email_Address"]);
                                user.Secondary_Email_Address = DBNull.Value.Equals(ds.Tables[0].Rows[i]["Secondary_Email_Address"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Secondary_Email_Address"]);
                                user.Role_Name = DBNull.Value.Equals(ds.Tables[0].Rows[0]["Role"]) ? "" : Convert.ToString(ds.Tables[0].Rows[i]["Role"]);
                                user.Role_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Role_Id"]);
                                lst_Users.Add(user);
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst_Users;
        }

    }
}
