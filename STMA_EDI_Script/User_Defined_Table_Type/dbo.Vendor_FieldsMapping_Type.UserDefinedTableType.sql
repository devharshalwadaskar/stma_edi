USE [STMA_EDI]
GO
/****** Object:  UserDefinedTableType [dbo].[Vendor_FieldsMapping_Type]    Script Date: 13-04-2020 10:27:26 ******/
CREATE TYPE [dbo].[Vendor_FieldsMapping_Type] AS TABLE(
	[Sequence] [bigint] NOT NULL,
	[Mapping_Id] [bigint] NOT NULL,
	[Fields_Id] [bigint] NOT NULL,
	[Field_Alias] [nvarchar](max) NOT NULL
)
GO
