
	internal_vendor_short_name varchar(20) UNIQUE NOT NULL,USE [STMA_EDI]
GO
/****** Object:  Table [dbo].[Vendor]    Script Date: 13-04-2020 10:21:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vendor](
	[Vendor_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Vendor_Name] [nvarchar](max) NULL,
	[Vendor_Code] [nvarchar](max) NULL,
	[Address] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[Country] [nvarchar](max) NULL,
	[Zip] [nvarchar](50) NULL,
	[Phone_Number] [nvarchar](50) NULL,
	[Email_Address] [nvarchar](50) NULL,
	[Secondary_Email_Address] [nvarchar](50) NULL,
	[Fax_Number] [nvarchar](50) NULL,
	[Tax_Number] [nvarchar](50) NULL,
	[Contact_Person] [nvarchar](50) NULL,
	[State_Id] [bigint] NULL,
	[Website] [nvarchar](50) NULL,
	[Status] [bit] NULL,
	internal_vendor_short_name varchar(20) UNIQUE NOT NULL,
 CONSTRAINT [PK_Vendor] PRIMARY KEY CLUSTERED 
(
	[Vendor_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
