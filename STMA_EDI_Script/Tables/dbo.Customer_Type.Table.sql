USE [STMA_EDI]
GO
/****** Object:  Table [dbo].[Customer_Type]    Script Date: 13-04-2020 10:21:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Type](
	[Customer_Type_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Customer_Type] [nvarchar](max) NOT NULL,
	[Is_Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_Customer_Type] PRIMARY KEY CLUSTERED 
(
	[Customer_Type_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
