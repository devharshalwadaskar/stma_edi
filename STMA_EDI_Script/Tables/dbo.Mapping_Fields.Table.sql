USE [STMA_EDI]
GO
/****** Object:  Table [dbo].[Mapping_Fields]    Script Date: 13-04-2020 10:21:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mapping_Fields](
	[Fields_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Field_Name] [nvarchar](max) NOT NULL,
	[Status] [bit] NOT NULL,
	[Is_Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_Mapping_Fields] PRIMARY KEY CLUSTERED 
(
	[Fields_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
