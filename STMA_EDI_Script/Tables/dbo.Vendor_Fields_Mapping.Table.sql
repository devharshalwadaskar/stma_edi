USE [STMA_EDI]
GO
/****** Object:  Table [dbo].[Vendor_Fields_Mapping]    Script Date: 13-04-2020 10:21:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vendor_Fields_Mapping](
	[Mapping_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Vendor_Id] [bigint] NOT NULL,
	[Fields_Id] [bigint] NOT NULL,
	[Field_Alias] [nvarchar](max) NULL,
	[Customer_Id_Alias] [nvarchar](max) NULL,
	[Invoice_Id_Alias] [nvarchar](max) NULL,
	[Invoice_Date_Alias] [nvarchar](max) NULL,
	[Loop_Alias] [nvarchar](max) NULL,
	[Product_Id_Alias] [nvarchar](max) NULL,
	[Product_Name_Alias] [nvarchar](max) NULL,
	[Product_Description_Alias] [nvarchar](max) NULL,
	[Product_Quantity_Alias] [nvarchar](max) NULL,
	[Product_Discount_Alias] [nvarchar](max) NULL,
	[Product_Amount_Alias] [nvarchar](max) NULL,
	[Product_Unit_Alias] [nvarchar](max) NULL,
	[Status] [bit] NULL,
	[Is_Deleted] [bigint] NOT NULL,
 CONSTRAINT [PK_Vendor_Fields_Mapping] PRIMARY KEY CLUSTERED 
(
	[Mapping_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Vendor_Fields_Mapping]  WITH CHECK ADD  CONSTRAINT [FK_Vendor_Fields_Mapping_Mapping_Fields] FOREIGN KEY([Fields_Id])
REFERENCES [dbo].[Mapping_Fields] ([Fields_Id])
GO
ALTER TABLE [dbo].[Vendor_Fields_Mapping] CHECK CONSTRAINT [FK_Vendor_Fields_Mapping_Mapping_Fields]
GO
ALTER TABLE [dbo].[Vendor_Fields_Mapping]  WITH CHECK ADD  CONSTRAINT [FK_Vendor_Fields_Mapping_Vendor] FOREIGN KEY([Vendor_Id])
REFERENCES [dbo].[Vendor] ([Vendor_Id])
GO
ALTER TABLE [dbo].[Vendor_Fields_Mapping] CHECK CONSTRAINT [FK_Vendor_Fields_Mapping_Vendor]
GO
