USE [STMA_EDI]
GO
/****** Object:  Table [dbo].[Product_Vendor_Mapping]    Script Date: 13-04-2020 10:21:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product_Vendor_Mapping](
	[Product_Vendor_Mapping_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Vendor_Id] [bigint] NOT NULL,
	[Product_Id] [bigint] NULL,
	[Cost] [numeric](18, 2) NULL,
	[MSA_Unit_Id] [bigint] NULL,
	[MSA_PackagingTypeID] [bigint] NULL,
	[ItemCode] [nvarchar](max) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Mapping_Vendor_Product] PRIMARY KEY CLUSTERED 
(
	[Product_Vendor_Mapping_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Product_Vendor_Mapping]  WITH CHECK ADD  CONSTRAINT [FK_Mapping_Vendor_Product_Product] FOREIGN KEY([Product_Id])
REFERENCES [dbo].[Product] ([Product_Id])
GO
ALTER TABLE [dbo].[Product_Vendor_Mapping] CHECK CONSTRAINT [FK_Mapping_Vendor_Product_Product]
GO
ALTER TABLE [dbo].[Product_Vendor_Mapping]  WITH CHECK ADD  CONSTRAINT [FK_Mapping_Vendor_Product_Vendor] FOREIGN KEY([Vendor_Id])
REFERENCES [dbo].[Vendor] ([Vendor_Id])
GO
ALTER TABLE [dbo].[Product_Vendor_Mapping] CHECK CONSTRAINT [FK_Mapping_Vendor_Product_Vendor]
GO
