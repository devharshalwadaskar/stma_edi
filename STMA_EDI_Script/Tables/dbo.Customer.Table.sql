USE [STMA_EDI]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 13-04-2020 10:21:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[Customer_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Customer_Name] [nvarchar](50) NULL,
	[Customer_Code] [nvarchar](max) NULL,
	[Address] [nvarchar](max) NULL,
	[City] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[State_Id] [bigint] NOT NULL,
	[Email_Address] [nvarchar](50) NULL,
	[Secondary_Email_Address] [nvarchar](50) NULL,
	[Phone_Number] [nvarchar](50) NULL,
	[Fax_Number] [nvarchar](50) NULL,
	[Zip] [nvarchar](50) NULL,
	[Status] [bit] NULL,
	[Tax_Number] [nvarchar](50) NULL,
	[Customer_Type_Id] [bigint] NULL,
	[Is_Deleted] [bit] NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[Customer_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_Customer_Type] FOREIGN KEY([Customer_Type_Id])
REFERENCES [dbo].[Customer_Type] ([Customer_Type_Id])
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_Customer_Type]
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_State_Master] FOREIGN KEY([State_Id])
REFERENCES [dbo].[State_Master] ([State_Id])
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_State_Master]
GO
