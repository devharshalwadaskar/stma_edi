USE [STMA_EDI]
GO
/****** Object:  Table [dbo].[Form_Role_Mapping]    Script Date: 13-04-2020 10:21:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Form_Role_Mapping](
	[Form_Role_Mapping_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Role_Id] [bigint] NOT NULL,
	[Form_Id] [bigint] NOT NULL,
	[Create] [bit] NOT NULL,
	[Edit] [bit] NOT NULL,
	[View] [bit] NOT NULL,
	[Delete] [bit] NOT NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_Form_Role_Mapping] PRIMARY KEY CLUSTERED 
(
	[Form_Role_Mapping_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Form_Role_Mapping]  WITH CHECK ADD  CONSTRAINT [FK_Form_Role_Mapping_Form_Master] FOREIGN KEY([Form_Id])
REFERENCES [dbo].[Form_Master] ([Form_Id])
GO
ALTER TABLE [dbo].[Form_Role_Mapping] CHECK CONSTRAINT [FK_Form_Role_Mapping_Form_Master]
GO
