USE [STMA_EDI]
GO
/****** Object:  Table [dbo].[Zip_Code_Master]    Script Date: 13-04-2020 10:21:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Zip_Code_Master](
	[Zip_Code_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[State_Id] [bigint] NULL,
	[Zip_Code] [bigint] NOT NULL,
	[State] [nvarchar](max) NOT NULL,
	[City] [nvarchar](max) NOT NULL,
	[Zip_Code_Type] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Zip_Code_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
