USE [STMA_EDI]
GO
/****** Object:  Table [dbo].[Zip_Code_Master]    Script Date: 13-04-2020 10:21:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create table processed_edi_files
(
	ID bigint identity(1,1) primary key,
	vendor_id bigint not null,
	edi_file_name varchar(4000),
	file_path varchar(max),
	process_date datetime,
	result varchar(4000),
	File_data varchar(MAX),
	file_binary varbinary(MAX)
)
GO
