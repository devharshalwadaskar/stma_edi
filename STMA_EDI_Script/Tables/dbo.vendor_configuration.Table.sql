USE [STMA_EDI]
GO
/****** Object:  Table [dbo].[Vendor]    Script Date: 13-04-2020 10:21:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create table vendor_configuration
(
	vendor_config_id BIGINT identity(1,1) primary key,
	vendor_id BIGINT not null unique  FOREIGN KEY REFERENCES vendor(vendor_id),
	ssh_key varchar(1000) not null,
	edi_format_id bigint not null FOREIGN KEY REFERENCES edi_formats(edi_format_id),
	is_deleted bit
)

GO
