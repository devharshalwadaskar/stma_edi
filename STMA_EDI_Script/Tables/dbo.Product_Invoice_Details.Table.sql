USE [STMA_EDI]
GO
/****** Object:  Table [dbo].[Product_Invoice_Details]    Script Date: 13-04-2020 10:21:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product_Invoice_Details](
	[Details_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Invoice_ID] [bigint] NOT NULL,
	[Product_ID] [bigint] NULL,
	[MSA_Unit_ID] [bigint] NULL,
	[Product_Vendor_Mapping_Id] [bigint] NULL,
	[Quantity] [numeric](18, 2) NULL,
	[MSA_Price] [numeric](18, 2) NULL,
	[Total_Price] [numeric](18, 2) NULL,
	[Discount] [numeric](18, 2) NULL,
	[Status] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Product_Invoice_Details]  WITH CHECK ADD  CONSTRAINT [FK_Product_Invoice_Details_Invoice] FOREIGN KEY([Invoice_ID])
REFERENCES [dbo].[Invoice] ([Invoice_ID])
GO
ALTER TABLE [dbo].[Product_Invoice_Details] CHECK CONSTRAINT [FK_Product_Invoice_Details_Invoice]
GO
ALTER TABLE [dbo].[Product_Invoice_Details]  WITH CHECK ADD  CONSTRAINT [FK_Product_Invoice_Details_Product] FOREIGN KEY([Product_ID])
REFERENCES [dbo].[Product] ([Product_Id])
GO
ALTER TABLE [dbo].[Product_Invoice_Details] CHECK CONSTRAINT [FK_Product_Invoice_Details_Product]
GO
