USE [STMA_EDI]
GO
/****** Object:  Table [dbo].[Invoice]    Script Date: 13-04-2020 10:21:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoice](
	[Invoice_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Invoice_No] [nvarchar](max) NULL,
	[Invoice_Date] [nvarchar](max) NULL,
	[Vendor_ID] [bigint] NULL,
	[Customer_ID] [bigint] NULL,
	[Total_Amount] [numeric](18, 2) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Invoice] PRIMARY KEY CLUSTERED 
(
	[Invoice_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
