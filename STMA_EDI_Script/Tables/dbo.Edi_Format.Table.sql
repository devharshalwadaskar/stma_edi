USE [STMA_EDI]
GO
/****** Object:  Table [dbo].[Vendor]    Script Date: 13-04-2020 10:21:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create table edi_formats
(
	edi_format_id BIGINT IDENTITY(1,1) PRIMARY KEY,
	edi_format_name varchar(1000) not null unique,
	edi_format varchar(MAX)
)
GO
