USE [STMA_EDI]
GO
/****** Object:  Table [dbo].[MSA_Units]    Script Date: 13-04-2020 10:21:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MSA_Units](
	[MSA_Unit_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[MSA_Unit] [numeric](18, 2) NULL,
	[MSA_PackagingType] [nvarchar](50) NULL
) ON [PRIMARY]
GO
