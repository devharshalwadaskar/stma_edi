USE [STMA_EDI]
GO
/****** Object:  Table [dbo].[MSA_PackagingType]    Script Date: 13-04-2020 10:21:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MSA_PackagingType](
	[MSA_PackagingTypeID] [bigint] IDENTITY(1,1) NOT NULL,
	[PackagingType] [nchar](10) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_MSA_Unit] PRIMARY KEY CLUSTERED 
(
	[MSA_PackagingTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
