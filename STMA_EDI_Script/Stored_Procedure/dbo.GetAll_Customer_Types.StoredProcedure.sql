USE [STMA_EDI]
GO
/****** Object:  StoredProcedure [dbo].[GetAll_Customer_Types]    Script Date: 13-04-2020 10:26:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[GetAll_Customer_Types]
As
Begin
         SELECT Customer_Type_Id,Customer_Type FROM  Customer_Type
			  Where  Is_Deleted=0
End
GO
