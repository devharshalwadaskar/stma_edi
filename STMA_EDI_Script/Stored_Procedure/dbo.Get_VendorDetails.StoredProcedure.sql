USE [STMA_EDI]
GO
/****** Object:  StoredProcedure [dbo].[Get_VendorDetails]    Script Date: 13-04-2020 10:26:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Get_VendorDetails]

 

     (@Vendor_Id  int= null)

 

AS
BEGIN

 

    Select 
               dbo.Vendor.Vendor_Id,dbo.Vendor.Vendor_Name,dbo.Vendor.Contact_Person,dbo.Vendor.Address,dbo.Vendor.City,dbo.Vendor.Country,
              dbo.Vendor.Zip,dbo.Vendor.Phone_Number,dbo.Vendor.Email_Address,
               dbo.Vendor.Secondary_Email_Address,dbo.Vendor.Fax_Number,dbo.Vendor.Tax_Number,
               dbo.Vendor.Website,dbo.Vendor.Vendor_Code,dbo.Vendor.Status,
               
                  Vendor.State_Id,State_Master.State AS State
                from dbo.Vendor
                inner join State_Master on Vendor.State_Id = State_Master.State_Id 
                    
                    where Vendor.Vendor_Id=@Vendor_Id AND Status=0
                     
END
GO
