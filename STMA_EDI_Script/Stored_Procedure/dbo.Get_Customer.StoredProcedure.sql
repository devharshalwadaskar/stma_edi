USE [STMA_EDI]
GO
/****** Object:  StoredProcedure [dbo].[Get_Customer]    Script Date: 13-04-2020 10:26:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[Get_Customer] 

@Customer_Id             BIGINT  

As 
  Begin 
         
         SELECT  Customer.Customer_Id,Customer.Customer_Name,Customer.Customer_Code,
		 Customer.Address,Customer.City, Customer.State_Id,State_Master.State,Customer.Country,
		 Customer.Customer_Type_Id,Customer.Email_Address,Customer.Email_Address,
		 Customer.Phone_Number,Customer.Phone_Number,Customer.Secondary_Email_Address,
		 Customer.Tax_Number,Customer.Fax_Number,Customer.Zip,Customer.Status,
		 Customer_Type.Customer_Type_Id,Customer_Type.Customer_Type FROM  Customer
		  inner join Customer_Type 
			on Customer.Customer_Type_Id=Customer_Type.Customer_Type_Id
		  inner join State_Master on Customer.State_Id = State_Master.State_Id

					  Where Customer_Id=@Customer_Id
					        AND Customer.Is_Deleted=0
  End 

GO
