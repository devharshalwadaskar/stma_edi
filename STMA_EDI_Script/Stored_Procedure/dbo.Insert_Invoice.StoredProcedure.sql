USE [STMA_EDI]
GO
/****** Object:  StoredProcedure [dbo].[Insert_Invoice]    Script Date: 04/10/2020 1:00:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Insert_Invoice]
(
    @Vendorname               NVARCHAR(50)  = NULL,
	@lastID_Product           NVARCHAR(50)  = NULL,
	@VendorID                 INT           = NULL,
	@Customername             NVARCHAR(50)  = NULL,		 
	@Invoice_No               NVARCHAR(MAX) = NULL, 
	@Invoice_Date             NVARCHAR(MAX) = NULL, 
	@Total_Amount             NUMERIC(18,2) = NULL,
	@Vendor_Id                BIGINT        = NULL,
    @Customer_ID              BIGINT        = NULL,
	@MESSAGE_ID				  BIGINT OUTPUT,
	@MESSAGE				  VARCHAR(MAX) OUTPUT
)
	   
AS
BEGIN

		  SELECT @MESSAGE = ''
		  SELECT @MESSAGE_ID = 0

		   SET @Vendor_Id   = (SELECT Vendor_Id  FROM  dbo.Vendor
                   WHERE lower(Vendor_Name) =lower(@Vendorname ))                                                
		   SET @Customer_ID = (SELECT Customer_ID  FROM  dbo.Customer
                   WHERE lower(Customer_name) =lower(@Customername )) 

		   IF  @Vendor_Id  IS NOT NULL AND @Customer_ID IS NOT NULL
		   BEGIN 
				IF(select count(*) from Invoice where Invoice_No = @Invoice_No) > 0
				BEGIN
					SET @MESSAGE_ID = 0
					SET @MESSAGE = 'Invoice Number:' + @Invoice_No + ' already exists'
					RETURN
				END

			   insert into  dbo.Invoice(Invoice_No,Invoice_Date,Vendor_ID,Customer_ID,Total_Amount,Status) 
			   values(  @Invoice_No, @Invoice_Date ,@Vendor_Id  , @Customer_ID ,  @Total_Amount,00)

			     SELECT @MESSAGE_ID = 1
		   END
		   ELSE
		   BEGIN
				IF @Customer_ID IS NULL AND @Vendor_Id IS NULL
				BEGIN
					SET @MESSAGE_ID = 0
					SET @MESSAGE = 'Invalid Vendor & customer.'
				END
				ELSE IF @Vendor_Id IS NULL
				BEGIN
					SET @MESSAGE_ID = 0
					SET @MESSAGE = 'Invalid Vendor.'
				END
				ELSE IF @Customer_ID IS NULL
				BEGIN
					SET @MESSAGE_ID = 0
					SET @MESSAGE = 'Invalid customer.'
				END
				
			END 
		 



END
