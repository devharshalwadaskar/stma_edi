USE [STMA_EDI]
GO
/****** Object:  StoredProcedure [dbo].[Insert_Update_VendorMappingFields]    Script Date: 13-04-2020 10:26:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Insert_Update_VendorMappingFields] (
	@Vendor_FieldsMapping_Type [dbo].[Vendor_FieldsMapping_Type] READONLY
	,@Vendor_Id BIGINT
	,@MESSAGE_ID INT OUTPUT
	,@MESSAGE NVARCHAR(MAX) OUTPUT
	,@MODE NVARCHAR(2)
	)
AS
BEGIN
	DECLARE @Sequence BIGINT = 0
	DECLARE @Mapping_Selected_Id BIGINT = NULL
	DECLARE @Mapping_Id BIGINT = NULL
	DECLARE @Fields_Id BIGINT = 0
	DECLARE @Field_Alias NVARCHAR(max) = NULL
	DECLARE @counter INT = 0
	DECLARE @no_Of_Rows INT = 0

	SET @counter = 1
	SET @no_Of_Rows = (
			SELECT Count(*)
			FROM @Vendor_FieldsMapping_Type
			)

	IF (
			(@MODE = 'I')
			AND @no_Of_Rows > 0
			)
	BEGIN
		WHILE (@counter <= @no_Of_Rows)
		BEGIN
			SET @Fields_Id = (
					SELECT Fields_Id
					FROM @Vendor_FieldsMapping_Type
					WHERE sequence = @counter
					)
			SET @Field_Alias = (
					SELECT Field_Alias
					FROM @Vendor_FieldsMapping_Type
					WHERE sequence = @counter
					)

			IF NOT EXISTS (
					SELECT Mapping_Id
					FROM Vendor_Fields_Mapping
					WHERE Fields_Id = @Fields_Id
						AND Vendor_Id = @Vendor_Id
						AND Is_Deleted = 0
					)
			BEGIN
				INSERT INTO Vendor_Fields_Mapping (
					Vendor_Id
					,Fields_Id
					,Field_Alias
					,STATUS
					,Is_Deleted
					)
				VALUES (
					@Vendor_Id
					,@Fields_Id
					,@Field_Alias
					,1
					,0
					)

				SET @MESSAGE_ID = 1

				SELECT @MESSAGE = 'Mapping inserted successfully.';
			END
			ELSE
			BEGIN
				SET @MESSAGE_ID = 5

				SELECT @MESSAGE = 'Mapping alreay exists.';
			END

			SET @counter = @counter + 1;
		END
	END

	IF (
			(@MODE = 'U')
			AND @no_Of_Rows > 0
			)
	BEGIN
		DECLARE @Mapping_Id_Max BIGINT = 0
		DECLARE @Mapping_Id_Top BIGINT = 0

		WHILE (@counter <= @no_Of_Rows)
		BEGIN
			SET @Fields_Id = (
					SELECT Fields_Id
					FROM @Vendor_FieldsMapping_Type
					WHERE sequence = @counter
					)
			SET @Field_Alias = (
					SELECT Field_Alias
					FROM @Vendor_FieldsMapping_Type
					WHERE sequence = @counter
					)
			SET @Mapping_Id = (
			SELECT Mapping_Id
			FROM @Vendor_FieldsMapping_Type
			WHERE sequence = @counter
			)
			SET @Mapping_Id_Max = (
					SELECT MAX(Mapping_Id)
					FROM Vendor_Fields_Mapping
					WHERE Fields_Id = @Fields_Id
						AND Vendor_Id = @Vendor_Id
						AND Is_Deleted = 0
					)
			SET @Mapping_Id_Top= (
					SELECT TOP 1 Mapping_Id
					FROM Vendor_Fields_Mapping
					WHERE Fields_Id = @Fields_Id
						AND Vendor_Id = @Vendor_Id
						AND Is_Deleted = 0
						ORDER BY Mapping_Id ASC
					)
			SET @Mapping_Selected_Id = (
					SELECT Mapping_Id
					FROM Vendor_Fields_Mapping
					WHERE Fields_Id = @Fields_Id
						AND Vendor_Id = @Vendor_Id
						AND Is_Deleted = 0
					)

			SELECT '---'

			PRINT @Fields_Id
			PRINT @Field_Alias
			PRINT @Mapping_Id

			IF (@Mapping_Id_Top = @Mapping_Selected_Id AND @Mapping_Selected_Id>0)
			BEGIN
				UPDATE Vendor_Fields_Mapping
				SET Field_Alias = @Field_Alias
				WHERE Mapping_Id = @Mapping_Selected_Id --AND Fields_Id= @Fields_Id AND  Vendor_Id = @Vendor_Id

				SET @MESSAGE_ID = 2

				SELECT @MESSAGE = 'Mapping updated successfully.';
			END
			ELSE
			BEGIN
			      SET @MESSAGE_ID =5

				SELECT @MESSAGE = 'Mapping already exists.';


				--INSERT INTO Vendor_Fields_Mapping (
				--	Vendor_Id
				--	,Fields_Id
				--	,Field_Alias
				--	,STATUS
				--	,Is_Deleted
				--	)
				--VALUES (
				--	@Vendor_Id
				--	,@Fields_Id
				--	,@Field_Alias
				--	,1
				--	,0
				--	)

				--SET @MESSAGE_ID = 1

				--SELECT @MESSAGE = 'Mapping updated successfully.';
			END

			SET @counter = @counter + 1;
		END
	END
END
	--Declare @Vendor_FieldsMapping_Type [dbo].[Vendor_FieldsMapping_Type]  
	--Declare @Vendor_Id                 BIGINT = 9
	--Declare @MESSAGE_ID              INT 
	--Declare @MESSAGE                 NVARCHAR(MAX) 
	--Declare @MODE                    NVARCHAR(2)='U' 
	--INSERT INTO @Vendor_FieldsMapping_Type (sequence, Mapping_Id, Fields_Id, Field_Alias) 
	--values (1,0,2,'Inv Id'), 
	-- (2,0,3,'Inv Date'),
	-- (3,0,3,'Inv Date'),
	-- (4,0,3,'Cust Name1'),
	-- (5,0,3,'Loop_Alias'),
	-- (6,0,3,'Prod Id'),
	-- (7,0,3,'Prod Name'),
	-- (8,0,3,'Prod Desc'),
	-- (9,0,3,'Prod Quantity'),
	-- (10,0,3,'Prod Discount'),
	-- (11,0,3,'Prod Amount')
	-- select * from @Vendor_FieldsMapping_Type 
	-- select '---' 
	--exec [dbo].[Insert_Update_VendorMappingFields] 
	--@Vendor_FieldsMapping_Type  = @Vendor_FieldsMapping_Type,
	-- @MESSAGE_ID = @MESSAGE_ID output, @MESSAGE = @MESSAGE output,
	-- @Vendor_Id  =9,@Mode='U'
	--select '---'
GO
