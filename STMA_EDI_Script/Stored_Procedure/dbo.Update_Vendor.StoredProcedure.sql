USE [STMA_EDI]
GO
/****** Object:  StoredProcedure [dbo].[Update_Vendor]    Script Date: 13-04-2020 10:26:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Update_Vendor] (
	--  @Vendor_Name                     NVARCHAR(MAX) = NULL ,
	@Contact_Person NVARCHAR(MAX) = NULL
	,@Address NVARCHAR(MAX) = NULL
	,@City NVARCHAR(MAX) = NULL
	,@State_Id BIGINT = NULL
	,@Country NVARCHAR(MAX) = NULL
	,@Email_Address NVARCHAR(MAX) = NULL
	,@Secondary_Email_Address NVARCHAR(MAX) = NULL
	,@Fax_Number NVARCHAR(MAX) = NULL
	,@Phone_Number NVARCHAR(MAX) = NULL
	,@Website NVARCHAR(MAX) = NULL
	,@Tax_Number NVARCHAR(MAX) = NULL
	,@Vendor_Code NVARCHAR(MAX) = NULL
	,@Zip NVARCHAR(MAX) = NULL
	,@Vendor_Id INT
	,@Vendor_Name NVARCHAR(MAX) = NULL
	,@MESSAGE_ID INT OUTPUT
	,@MESSAGE NVARCHAR(MAX) OUTPUT
	)
AS
BEGIN
	BEGIN
		DECLARE @Vendor_Name_Count INT  
		SET @Vendor_Name_Count = (SELECT COUNT(*) FROM Vendor
		 WHERE Vendor_Name=@Vendor_Name AND Vendor_Id<>@Vendor_Id AND Status=0)

		BEGIN
		    IF (@Vendor_Name_Count = 0)
			BEGIN
			UPDATE dbo.Vendor
			SET Vendor_Name = @Vendor_Name
				,Contact_Person = @Contact_Person
				,Address = @Address
				,City = @City
				,Country = @Country
				,State_Id = @State_Id
				,Zip = @Zip
				,Phone_Number = @Phone_Number
				,Email_Address = @Email_Address
				,Secondary_Email_Address = @Secondary_Email_Address
				,Fax_Number = @Fax_Number
				,Tax_Number = @Tax_Number
				,Website = @Website
				,Vendor_Code = @Vendor_Code
			WHERE Vendor_Id = @Vendor_Id
			SET @MESSAGE_ID=2
                SELECT @MESSAGE = 'Vendor updated successfully.'; 
			END
			ELSE 
			BEGIN
				SET @MESSAGE_ID=5
				SELECT @MESSAGE = 'Vendor already exists.'; 
			END
		END
	END
END
GO
