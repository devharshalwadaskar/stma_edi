USE [STMA_EDI]
GO
/****** Object:  StoredProcedure [dbo].[Get_User_Forms_Role_Mappings]    Script Date: 13-04-2020 10:26:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Get_User_Forms_Role_Mappings]
@User_Id BIGINT 
AS

BEGIN
     DECLARE @Role_Id BIGINT

	 SET @Role_Id = (SELECT MAX(Form_Role_Mapping.Role_Id) FROM Form_Role_Mapping
	 INNER JOIN Form_Master ON Form_Role_Mapping.Form_Id = Form_Master.Form_Id
	 INNER JOIN Role_Master ON Role_Master.Role_Id = Form_Role_Mapping.Role_Id
	 WHERE Form_Master.Status=0 AND
	       Form_Role_Mapping.Status=0 )

     SELECT Form_Role_Mapping_Id,Role_Master.Role_Id,Role_Master.Role,Form_Master.Form_Id,Form_Master.Form_Name,[Create],Edit,[View],[Delete] FROM Form_Role_Mapping
	 INNER JOIN Form_Master ON Form_Role_Mapping.Form_Id = Form_Master.Form_Id
	 INNER JOIN Role_Master ON Role_Master.Role_Id = Form_Role_Mapping.Role_Id
	 WHERE Form_Master.Status=0 AND
	       Form_Role_Mapping.Status=0 AND
		   Form_Role_Mapping.Role_Id = @Role_Id
END
GO
