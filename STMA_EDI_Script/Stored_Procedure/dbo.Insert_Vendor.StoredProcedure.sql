USE [STMA_EDI]
GO
/****** Object:  StoredProcedure [dbo].[Insert_Vendor]    Script Date: 13-04-2020 10:26:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Insert_Vendor]

 	
	(
      @Vendor_Name              NVARCHAR(MAX) = NULL ,
	   @Contact_Person            NVARCHAR(MAX) = NULL ,
	   @Address                  NVARCHAR(MAX) = NULL ,
	   @City                     NVARCHAR(MAX) = NULL ,
	   @State_Id                    NVARCHAR(MAX) = NULL ,
	   @Country                  NVARCHAR(MAX) = NULL ,
	   @Email_Address            NVARCHAR(MAX) = NULL ,
	   @Secondary_Email_Address  NVARCHAR(MAX) = NULL ,
	   @Fax_Number               NVARCHAR(MAX) = NULL ,
	   @Phone_Number             NVARCHAR(MAX) = NULL ,
	   @Website                  NVARCHAR(MAX) = NULL ,
	   @Tax_Number			     NVARCHAR(MAX) = NULL ,
	   @Vendor_Code                     NVARCHAR(MAX) = NULL ,
	   @Zip                      NVARCHAR(MAX) = NULL 
	    

	   )
	   
AS
BEGIN

		begin

	            if @Vendor_Name   is not null And    @Address  is not null  And   @City is not null And  @State_Id is not null And  @Country is not null And  
				   @Email_Address  is not null And   @Phone_Number is not null And @Vendor_Code is not null And @Zip is not null 
				begin 
					insert into  dbo.Vendor(Vendor_Name,Contact_Person,Address,City,Country,State_Id,Zip,Phone_Number,Email_Address,
					Secondary_Email_Address,Fax_Number,Tax_Number,Website,Vendor_Code,Status) 
					values( @Vendor_Name,@Contact_Person ,@Address,@City,@Country,	   @State_Id,@Zip,
					@Phone_Number,@Email_Address,@Secondary_Email_Address,@Fax_Number,@Tax_Number,  @Website ,@Vendor_Code,00  )
                 end
				 
			
		end



END
GO
