USE [STMA_EDI]
GO
/****** Object:  StoredProcedure [dbo].[Delete_Customer]    Script Date: 13-04-2020 10:26:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Delete_Customer]

  @Customer_Id AS BIGINT,
  @MESSAGE_ID			NUMERIC OUTPUT,
  @MESSAGE			VARCHAR(MAX) OUTPUT
AS

BEGIN
      DECLARE @Customer_Count BIGINT
	  SET  @Customer_Count = COUNT(@Customer_Id)  
      IF @Customer_Id > 0
	  BEGIN     
			DELETE FROM Customer 
		--	UPDATE CUSTOMER SET Is_Deleted=0
	        WHERE Customer_Id = @Customer_Id
			SET @MESSAGE_ID = 3
			SET @MESSAGE = 'Customer Field Mapping Deleted Sucessfully. '
	  END
	  ELSE
	  BEGIN
	       SET @MESSAGE_ID = 0
			SET @MESSAGE = 'Failed to delete Customer Field Mapping.'
	  END
END
GO
