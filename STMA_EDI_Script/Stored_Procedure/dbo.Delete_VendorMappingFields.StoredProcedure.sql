USE [STMA_EDI]
GO
/****** Object:  StoredProcedure [dbo].[Delete_VendorMappingFields]    Script Date: 13-04-2020 10:26:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Delete_VendorMappingFields]

  @Vendor_Id AS BIGINT,
  @MESSAGE_ID			NUMERIC OUTPUT,
  @MESSAGE			VARCHAR(MAX) OUTPUT
AS

BEGIN
      DECLARE @Mapping_Count BIGINT
	  SET  @Mapping_Count = COUNT(@Vendor_Id)  
      IF @Vendor_Id > 0
	  BEGIN     
			DELETE FROM Vendor_Fields_Mapping 
	        WHERE Vendor_Id = @Vendor_Id
			SET @MESSAGE_ID = 3
			SET @MESSAGE = 'Vendor Field Mapping Deleted Sucessfully. '
	  END
	  ELSE
	  BEGIN
	       SET @MESSAGE_ID = 0
			SET @MESSAGE = 'Failed to delete Vendor Field Mapping.'
	  END
END
GO
