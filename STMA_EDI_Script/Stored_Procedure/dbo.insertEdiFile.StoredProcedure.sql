alter proc insert_edi_file
(
	@processed_edi_file_id bigint,
	@vendor_name varchar(4000)  = NULL,
	@edi_file_name varchar(4000)  = NULL,
	@file_path varchar(max)  = NULL,
	@result varchar(4000)  = NULL,
	@File_data varchar(MAX) = NULL,
	@file_binary varbinary(MAX),
	@outId	bigint OUT
)
AS
BEGIN

	declare @vendor_id bigint 
	set @vendor_id = ISNULL((select Vendor_Id from Vendor where ltrim(rtrim(lower(internal_vendor_short_name))) = ltrim(rtrim(lower(@vendor_name)))),0)

	IF @processed_edi_file_id = 0 
	BEGIN
			INSERT INTO processed_edi_files(
			vendor_id,
			edi_file_name ,
			file_path,
			process_date ,
			result ,
			File_data ,
			file_binary)
			SELECT @vendor_id, @edi_file_name, @file_path, getdate(), @result, @File_data,@file_binary

			SET @outId = @@IDENTITY
	END
	ELSE
	BEGIN
		UPDATE processed_edi_files 
		SET 
			result = @result 
		WHERE ID = @processed_edi_file_id

		SET @outId = @processed_edi_file_id
	END

END