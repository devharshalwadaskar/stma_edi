USE [STMA_EDI]
GO
/****** Object:  StoredProcedure [dbo].[Get_VendorMappingFields]    Script Date: 13-04-2020 10:26:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[Get_VendorMappingFields] 
@Vendor_Id             BIGINT  

As 
  Begin 
        SELECT        Vendor.Vendor_Id, Vendor.Vendor_Name,
		              Mapping_Fields.Fields_Id, Mapping_Fields.Field_Name, Vendor_Fields_Mapping.Field_Alias,
					  Vendor_Fields_Mapping.Mapping_Id
           FROM       Vendor_Fields_Mapping INNER JOIN
                         Mapping_Fields ON Vendor_Fields_Mapping.Fields_Id = Mapping_Fields.Fields_Id INNER JOIN
                         Vendor ON Vendor_Fields_Mapping.Vendor_Id = Vendor.Vendor_Id

					  Where Vendor_Fields_Mapping.Vendor_Id=@Vendor_Id
					        AND Vendor_Fields_Mapping.Is_Deleted=0
						    AND Mapping_Fields.Is_Deleted=0
  End 

  --exec Get_VendorMappingFields 4
GO
