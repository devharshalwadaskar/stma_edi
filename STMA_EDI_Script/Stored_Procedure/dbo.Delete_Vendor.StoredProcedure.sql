USE [STMA_EDI]
GO
/****** Object:  StoredProcedure [dbo].[Delete_Vendor]    Script Date: 13-04-2020 10:26:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Delete_Vendor]

  @Vendor_Id int,
  @MESSAGE_ID			NUMERIC OUTPUT,
   @MESSAGE			VARCHAR(MAX) OUTPUT

AS

BEGIN
      
      IF @Vendor_Id  > 0
	  BEGIN     
		 update  Vendor set Status=1
	        WHERE   Vendor_Id =@Vendor_Id
			SET @MESSAGE_ID = 3
			SET @MESSAGE = 'vendor Deleted Sucessfully. '
	  END
	  ELSE
	  BEGIN
	       SET @MESSAGE_ID = 0
			SET @MESSAGE = 'Failed to delete vendor.'
	  END
END
GO
