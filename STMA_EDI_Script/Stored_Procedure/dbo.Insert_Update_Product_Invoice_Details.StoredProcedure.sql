USE [STMA_EDI]
GO
/****** Object:  StoredProcedure [dbo].[Insert_Update_Product_Invoice_Details]    Script Date: 13-04-2020 10:26:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 

create PROCEDURE [dbo].[Insert_Update_Product_Invoice_Details]

 

     -- Add the parameters for the stored procedure here
    (
       @UnitCase                 NVARCHAR(50)  = NULL , 
       @ItemCode                 NVARCHAR(50)  = NULL , 
       @Price                    numeric(18,2) = NULL ,
       @Quantity                 numeric(18,2) = NULL ,
       @Discount                 numeric(18,2) = NULL ,
       @Total                    numeric(18,2) = NULL ,
       @Currency                 NVARCHAR(50)  = NULL ,
       @Vendorname               NVARCHAR(50)  = NULL ,
       @lastID_Product           NVARCHAR(50)  = NULL ,
       @VendorID                 int           = NULL ,
       @Customername             NVARCHAR(50)  = NULL ,
       @Invoice_No               NVARCHAR(MAX) = NULL , 
       @Invoice_Date             NVARCHAR(MAX) = NULL , 
       @Total_Amount             numeric(18,0) = NULL ,
       @Vendor_Id                bigint        = NULL ,
       @Customer_ID              bigint        = NULL ,
       @Invoiced_ID              bigint        = NULL ,
       @Producted_ID             bigint        = NULL ,
       @unit_case                nchar(15)     = NULL ,
       @unit_case_ID             bigint        = NULL ,
       @VendorIDtemp             int           = NULL ,
       @Product_Id               int           = NULL ,
       @Product_Vendor_Mapping_Id int          = NULL,
       @MSA_PackagingTypeID_Temp int           = NULL

 

       )
       
AS
BEGIN

 


SET  @VendorIDtemp   = (SELECT Vendor_Id  FROM  dbo.Vendor
                    WHERE lower(Vendor_Name) =lower(@Vendorname ))          
                                                          
                    if @VendorIDtemp is not null
                    begin
SET @Invoiced_ID  =(SELECT MAX(Invoice_ID) FROM dbo.Invoice ) 
SET @Product_Id =(select  Product_Id from Product_Vendor_Mapping 
where dbo.Product_Vendor_Mapping.Vendor_Id= @VendorIDtemp And ItemCode=   @ItemCode)

 

SET @Product_Vendor_Mapping_Id =(select  Product_Vendor_Mapping_Id from Product_Vendor_Mapping 
where dbo.Product_Vendor_Mapping.Vendor_Id= @VendorIDtemp And ItemCode=   @ItemCode)

 

SET  @MSA_PackagingTypeID_Temp   =(select  MSA_PackagingTypeID from Product_Vendor_Mapping 
where dbo.Product_Vendor_Mapping.Vendor_Id= @VendorIDtemp And ItemCode=   @ItemCode)

 

insert into Product_Invoice_Details(Invoice_ID,Product_ID,MSA_Unit_ID,Product_Vendor_Mapping_Id,Quantity,Total_Price,Discount,Status)
values(@Invoiced_ID,@Product_Id,@MSA_PackagingTypeID_Temp ,@Product_Vendor_Mapping_Id,@Quantity  ,@Total ,@Discount,0 )
    
    end
END
GO
