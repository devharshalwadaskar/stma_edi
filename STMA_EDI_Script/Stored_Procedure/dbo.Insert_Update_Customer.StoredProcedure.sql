USE [STMA_EDI]
GO
/****** Object:  StoredProcedure [dbo].[Insert_Update_Customer]    Script Date: 13-04-2020 10:26:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[Insert_Update_Customer] ( 
@Customer_Id                 BIGINT= NULL , 
@Customer_Name                 NVARCHAR(MAX)= NULL , 
@Customer_Code                 NVARCHAR(MAX) = NULL, 
@Address                 NVARCHAR(MAX)= NULL , 
@City                 NVARCHAR(MAX) = NULL, 
@Country                 NVARCHAR(MAX) = NULL, 
@State_Id                 BIGINT = NULL, 
@Email_Address                 NVARCHAR(MAX) = NULL, 
@Secondary_Email_Address                 NVARCHAR(MAX) = NULL, 
@Phone_Number                 NVARCHAR(MAX) = NULL, 
@Fax_Number                 NVARCHAR(MAX) = NULL, 
@Zip                 NVARCHAR(MAX) = NULL, 
@Tax_Number                 NVARCHAR(MAX) = NULL, 
@Status                 BIT, 
@Customer_Type_Id            BIGINT = NULL,    
@MESSAGE_ID              INT output, 
@MESSAGE                 NVARCHAR(MAX) output, 
@MODE                    NVARCHAR(2)) 
As 
  Begin 

			  IF (@MODE = 'I' ) 
					 BEGIN
						  IF NOT Exists(Select Customer_Id From Customer Where TRIM(LOWER(Customer_Name))=TRIM(LOWER(@Customer_Name) )AND Is_Deleted=0)
							 BEGIN
								   INSERT INTO Customer 
											(
											 Customer_Name, 
											 Customer_Code, 
											 Address, 
											 City, 
											 State_Id, 
											 Country, 
											 Email_Address, 
											 Secondary_Email_Address, 
											 Phone_Number, 
											 Fax_Number, 
											 Zip, 
											 Status,
											 Tax_Number,
											 Customer_Type_Id ,
											 Is_Deleted
											 ) 
									 VALUES  
											 (
											 @Customer_Name, 
											 @Customer_Code, 
											 @Address, 
											 @City, 
											 @State_Id, 
											 @Country, 
											 @Email_Address, 
											 @Secondary_Email_Address, 
											 @Phone_Number, 
											 @Fax_Number, 
											 @Zip, 
											 @Status,
											 @Tax_Number,
											 @Customer_Type_Id ,
											 0
											 ) 
								SET @MESSAGE_ID=1
								SELECT @MESSAGE = 'Customer inserted successfully.'; 
							 END
							 ELSE 
							 BEGIN
								 SET @MESSAGE_ID=2
								SELECT @MESSAGE = 'Customer already exist.'; 
							 END
					END
			 

		IF (@MODE = 'U' ) 
        BEGIN
				IF @Customer_Id > 0
				BEGIN
						  IF NOT  Exists(Select Customer_Id From Customer
						         Where Customer_Id<>@Customer_Id AND TRIM(LOWER(Customer_Name))=TRIM(LOWER(@Customer_Name) )AND Is_Deleted=0)
							 BEGIN
								   UPDATE Customer 
											SET
											 Customer_Name = @Customer_Name, 
											 Customer_Code=@Customer_Code, 
											 Address=@Address, 
											 City=@City, 
											 State_Id=@State_Id, 
											 Country=@Country, 
											 Email_Address=@Email_Address, 
											 Secondary_Email_Address=@Secondary_Email_Address, 
											 Phone_Number=@Phone_Number, 
											 Fax_Number=@Fax_Number, 
											 Zip=@Zip, 
											 Tax_Number=@Tax_Number,
											 Customer_Type_Id =@Customer_Type_Id
											 Where Customer_Id=@Customer_Id
					         
								SET @MESSAGE_ID=1
								SELECT @MESSAGE = 'Customer Updated successfully.'; 
							 END
							 ELSE 
							 BEGIN
								 SET @MESSAGE_ID=2
								SELECT @MESSAGE = 'Customer already exist.'; 
							 END
			    END
				ELSE
	            BEGIN
	                 SET @MESSAGE_ID=2
                     SELECT @MESSAGE = 'Customer does not exist.'; 
	            END
		END
			
	 
  End 



GO
