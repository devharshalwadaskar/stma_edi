﻿using Newtonsoft.Json;
using SchedularTest.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace SchedularTest.Controller
{
    class API_Caller
    {
        static HttpClient _client = null;
        static string URI = cAPIConnection.APIConnection;

        public List<File_Operation_Message> postFile(List<FileManager> oMgr)
        {
            List<File_Operation_Message> _insertUpdate_Return = null;
            try
            {
                _client = new HttpClient();
                _client.BaseAddress = new Uri(URI);

                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json"); _client.DefaultRequestHeaders.Accept.Add(contentType);
                // _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", key);
                string stringData = JsonConvert.SerializeObject(oMgr);
                var contentData = new StringContent(stringData, System.Text.Encoding.UTF8, "application/json");
                // HttpResponseMessage response = _client.PostAsync("/api/EDI_Decryption/OwnCreated_New", contentData).Result;
                HttpResponseMessage response = _client.PostAsync("/api/EDI_Decryption/OwnCreated_New", contentData).Result;
                _insertUpdate_Return = JsonConvert.DeserializeObject<List<File_Operation_Message>>(response.Content.ReadAsStringAsync().Result);
                if (_insertUpdate_Return != null)
                {
                    for (int i = 0; i < _insertUpdate_Return.Count; i++)
                    {
                        //string IN_PROCESS = "IN_PROCESS";
                        //replace ftp path with local path
                        string outpath = _insertUpdate_Return[i].FilePath.Substring(0, _insertUpdate_Return[i].FilePath.IndexOf(Constants.IN_PROCESS)) + Constants.VENDOR_OUT + @"\" + _insertUpdate_Return[i].FileName.Substring(0, _insertUpdate_Return[i].FileName.IndexOf(".")) + "_OUTPUT" + ".txt";
                        outpath = outpath.Replace(Constants.FTP_PATH, Constants.directory_name + @"\").Replace("/", "\\");

                        StreamWriter OurStream;
                        OurStream = File.CreateText(outpath);

                        string in_file_path = _insertUpdate_Return[i].FilePath.Replace(Constants.IN_PROCESS, Constants.VENDOR_IN).Replace(Constants.FTP_PATH, Constants.directory_name + @"\").Replace("/","\\");
                        if (_insertUpdate_Return[i].Is_success == false)
                        {
                            
                            OurStream.WriteLine("Error Date :" + DateTime.Now + " Error => " + _insertUpdate_Return[i].Error_Message);
                            File.Delete(in_file_path);
                        }
                        else
                        {
                                OurStream.WriteLine("Success :" + DateTime.Now);
                            File.Delete(in_file_path);
                        }

                        OurStream.Close();
                    }
                }
                else
                {
                    _insertUpdate_Return[0].Is_success = false;
                    _insertUpdate_Return[0].Error_Message = (string.IsNullOrEmpty(_insertUpdate_Return[0].Return_Message)) ? "Failed to insert data" : _insertUpdate_Return[0].Return_Message;
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message);
            }

            return _insertUpdate_Return;
        }


        //public File_Operation_Message postFile(List<FileManager> oMgr)
        //{
        //    File_Operation_Message _insertUpdate_Return = null;
        //    try
        //    {
        //        _client.BaseAddress = new Uri(URI);

        //        MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json"); _client.DefaultRequestHeaders.Accept.Add(contentType);
        //        // _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", key);
        //        string stringData = JsonConvert.SerializeObject(oMgr);
        //        var contentData = new StringContent(stringData, System.Text.Encoding.UTF8, "application/json");
        //        // HttpResponseMessage response = _client.PostAsync("/api/EDI_Decryption/OwnCreated_New", contentData).Result;
        //        HttpResponseMessage response = _client.PostAsync("/api/EDI_Decryption/OwnCreated_New", contentData).Result;
        //        _insertUpdate_Return = JsonConvert.DeserializeObject<File_Operation_Message>(response.Content.ReadAsStringAsync().Result);
        //        if (_insertUpdate_Return != null)
        //        {
        //            if (_insertUpdate_Return.Is_success == false)
        //            {
        //                _insertUpdate_Return.Is_success = false;
        //                //oMgr.ErrorInProcess = true;

        //                _insertUpdate_Return.Error_Message = (string.IsNullOrEmpty(_insertUpdate_Return.Return_Message)) ? "Failed to insert data" : _insertUpdate_Return.Return_Message;

        //                string IN_PROCESS = "IN_PROCESS";
        //                //replace ftp path with local path
        //                string outpath = _insertUpdate_Return.FilePath.Substring(0, _insertUpdate_Return.FilePath.IndexOf(IN_PROCESS)) + Constants.VENDOR_OUT + @"\" + _insertUpdate_Return.FileName.Substring(0, _insertUpdate_Return.FileName.IndexOf(".")) + "_OUTPUT" + ".txt";
        //                outpath = outpath.Replace(Constants.FTP_PATH, Constants.directory_name + @"\").Replace("/", "\\");

        //                //  FileInfo oOutFile = new FileInfo(outpath);
        //                StreamWriter OurStream;
        //                OurStream = File.CreateText(outpath);

        //                if (_insertUpdate_Return == null)
        //                {
        //                    OurStream.WriteLine("Error :" + DateTime.Now);
        //                }
        //                else
        //                {
        //                    if (_insertUpdate_Return.Is_success == false)
        //                    {
        //                        // generate error file
        //                        OurStream.WriteLine("Error :" + DateTime.Now);
        //                    }
        //                    else
        //                    {
        //                        //generate success file
        //                        OurStream.WriteLine("Success :" + DateTime.Now);
        //                    }
        //                    OurStream.Close();
        //                }

        //            }
        //            else
        //            {
        //                //oMgr.ErrorInProcess = false;
        //                _insertUpdate_Return.Error_Message = (string.IsNullOrEmpty(_insertUpdate_Return.Return_Message)) ? "Failed to insert data" : _insertUpdate_Return.Return_Message;

        //            }
        //        }
        //        else
        //        {
        //            _insertUpdate_Return.Is_success = false;
        //            _insertUpdate_Return.Error_Message = (string.IsNullOrEmpty(_insertUpdate_Return.Return_Message)) ? "Failed to insert data" : _insertUpdate_Return.Return_Message;
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    return _insertUpdate_Return;
        //}

        //public File_Operation_Message postFile(FileManager oMgr)
        //{
        //    File_Operation_Message _insertUpdate_Return = null;
        //    try
        //    {
        //        _client.BaseAddress = new Uri(URI);

        //        MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json"); _client.DefaultRequestHeaders.Accept.Add(contentType);
        //       // _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", key);
        //        string stringData = JsonConvert.SerializeObject(oMgr);
        //        var contentData = new StringContent(stringData, System.Text.Encoding.UTF8, "application/json");
        //       // HttpResponseMessage response = _client.PostAsync("/api/EDI_Decryption/OwnCreated_New", contentData).Result;
        //        HttpResponseMessage response = _client.PostAsync("/api/EDI_Decryption/OwnCreated_New", contentData).Result;
        //        _insertUpdate_Return = JsonConvert.DeserializeObject<File_Operation_Message>(response.Content.ReadAsStringAsync().Result);
        //        if (_insertUpdate_Return != null)
        //        {
        //            if (_insertUpdate_Return.Is_success == false)
        //            {
        //                _insertUpdate_Return.Is_success = false;
        //                oMgr.ErrorInProcess = true;

        //                _insertUpdate_Return.Error_Message = (string.IsNullOrEmpty(_insertUpdate_Return.Return_Message)) ? "Failed to insert data" : _insertUpdate_Return.Return_Message;

        //                string IN_PROCESS = "IN_PROCESS";
        //                //replace ftp path with local path
        //                string outpath = _insertUpdate_Return.FilePath.Substring(0, _insertUpdate_Return.FilePath.IndexOf(IN_PROCESS)) + Constants.VENDOR_OUT + @"\" + _insertUpdate_Return.FileName.Substring(0, _insertUpdate_Return.FileName.IndexOf(".")) + "_OUTPUT" + ".txt";
        //                outpath = outpath.Replace(Constants.FTP_PATH, Constants.directory_name + @"\").Replace("/", "\\");

        //                //  FileInfo oOutFile = new FileInfo(outpath);
        //                StreamWriter OurStream;
        //                OurStream = File.CreateText(outpath);

        //                if (_insertUpdate_Return == null)
        //                {
        //                    OurStream.WriteLine("Error :" + DateTime.Now);
        //                }
        //                else
        //                {
        //                    if (_insertUpdate_Return.Is_success == false)
        //                    {
        //                        // generate error file
        //                        OurStream.WriteLine("Error :" + DateTime.Now);
        //                    }
        //                    else
        //                    {
        //                        //generate success file
        //                        OurStream.WriteLine("Success :" + DateTime.Now);
        //                    }
        //                    OurStream.Close();
        //                }

        //            }
        //            else
        //            {
        //                oMgr.ErrorInProcess = false;
        //                _insertUpdate_Return.Error_Message = (string.IsNullOrEmpty(_insertUpdate_Return.Return_Message)) ? "Failed to insert data" : _insertUpdate_Return.Return_Message;

        //            }
        //        }
        //        else
        //        {
        //            _insertUpdate_Return.Is_success = false;
        //            _insertUpdate_Return.Error_Message = (string.IsNullOrEmpty(_insertUpdate_Return.Return_Message)) ? "Failed to insert data" : _insertUpdate_Return.Return_Message;
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    return _insertUpdate_Return;
        //}
    }
}
