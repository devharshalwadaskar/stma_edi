﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SchedularTest.Controller
{
    static class CreateFolders
    {
        public static void CreateFolderStructure()
        {
            try
            {
                string[] VendorArr = { "Vendor_1", "Vendor_2", "Vendor_3", "Vendor_4", "Vendor_5", "Vendor_6", "Vendor_7", "Vendor_8", "Vendor_9", "Vendor_10", "Vendor_11", "Vendor_12" };

                foreach (var item in VendorArr)
                {
                    if(!Directory.Exists(Constants.directory_name + @"\" + item))
                    {
                        Directory.CreateDirectory(Constants.directory_name + @"\" + item);
                        Directory.CreateDirectory(Constants.directory_name + @"\" + item + @"\" + Constants.VENDOR_IN);
                        Directory.CreateDirectory(Constants.directory_name + @"\" + item + @"\" + Constants.VENDOR_OUT);
                        Directory.CreateDirectory(Constants.directory_name + @"\" + item + @"\" + Constants.IN_PROCESS);
                    }
                    else if (!Directory.Exists(Constants.directory_name + @"\" + item + @"\" + Constants.VENDOR_IN))
                    {
                        Directory.CreateDirectory(Constants.directory_name + @"\" + item + @"\" + Constants.VENDOR_IN);
                    }
                    else if (!Directory.Exists(Constants.directory_name + @"\" + item + @"\" + Constants.VENDOR_OUT))
                    {
                        Directory.CreateDirectory(Constants.directory_name + @"\" + item + @"\" + Constants.VENDOR_OUT);
                    }
                    else if (!Directory.Exists(Constants.directory_name + @"\" + item + @"\" + Constants.IN_PROCESS))
                    {
                        Directory.CreateDirectory(Constants.directory_name + @"\" + item + @"\" + Constants.IN_PROCESS);
                    }
                }

			}
			catch (Exception ex)
			{

			}
        }
    }
}
