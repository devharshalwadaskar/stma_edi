﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SchedularTest
{
    
    public static class ErrorLogger
    {
        public static string Get_GlobalErrorMessage { get { return "Something's not right. The content you requested is temporarily unavailable. We apologize for the inconvenience, please check back in a few minutes."; } }
        public static void Log(string message)
        {
            var hostingEnvironment_RootPath = Constants.directory_name;
            var file_Path = Path.Combine(hostingEnvironment_RootPath, "Error_Logs\\Error.txt");
            var directory_Path = Path.Combine(hostingEnvironment_RootPath, "Error_Logs");
            try
            {
                if (!Directory.Exists(directory_Path))
                {
                    Directory.CreateDirectory(directory_Path);
                }
                string new_Line = Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine;
                File.AppendAllText(file_Path, new_Line + message + Environment.NewLine + "Date :" + DateTime.Now.ToString());
            }
            catch (Exception _ex)
            {
                throw _ex;
            }
        }
    }
}
