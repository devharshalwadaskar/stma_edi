#pragma checksum "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "724b2cab967a9ac76f2296206ad6d18f884555e7"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Mapping_List), @"mvc.1.0.view", @"/Views/Mapping/List.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Minal\EDI\EDI_App\EDI_App\Views\_ViewImports.cshtml"
using EDI_App;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Minal\EDI\EDI_App\EDI_App\Views\_ViewImports.cshtml"
using EDI_App.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"724b2cab967a9ac76f2296206ad6d18f884555e7", @"/Views/Mapping/List.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f824bc411d252f926515e3d6fb20919076682b2d", @"/Views/_ViewImports.cshtml")]
    public class Views_Mapping_List : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<EDI_App.Models.Mapping_M>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Pages/Common_Ids.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Pages/js/Mapping/Mapping_List.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("    ViewData[\"Title\"] = \"List\";\r\n}\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "724b2cab967a9ac76f2296206ad6d18f884555e73817", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "724b2cab967a9ac76f2296206ad6d18f884555e74856", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
<main>
    <div class=""container-fluid"" style="" margin-left: 213px;width: 88%;"">
        <div class=""col-lg-12"" style=""margin-top:32px"">
            <div class=""card shadow-lg border-0 rounded-lg mt-12"">
                <div class=""col-sm-12 card-header"">
                    <div class=""col-sm-10"" style=""float:left"">
                        <h3 class=""text-center font-weight-light my-6"">Vendor Fields Mapping</h3>
                    </div>
                    <div class=""col-sm-2"" style=""float:right"">
                        <button class=""btn btn-primary btn-sm float-right """);
            BeginWriteAttribute("onclick", " onclick=\"", 780, "\"", 838, 3);
            WriteAttributeValue("", 790, "location.href=\'", 790, 15, true);
#nullable restore
#line 15 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
WriteAttributeValue("", 805, Url.Action("Create", "Mapping"), 805, 32, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 837, "\'", 837, 1, true);
            EndWriteAttribute();
            WriteLiteral(@" id=""btn_FieldMapping""><i class=""fa fa-plus"" aria-hidden=""true""></i> New</button>
                    </div>
                </div>
                <div class=""card-body"">
                    <div class=""col-sm-12"">
                        <table class=""table table-hover table-sm functional-team-view"" id=""tbl_MappingList"">
                            <thead>
                                <tr>
                                    <th>Vendor Name</th>
                                    <th>Customer Id</th>
                                    <th>Invoice Id</th>
                                    <th>Invoice Date</th>
                                    <th>Product ID</th>
                                    <th>Product Name</th>
                                    <th class=""text-center""></th>
                                </tr>
                            </thead>
                            <tbody>

");
#nullable restore
#line 34 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
                                 foreach (var item in Model)
                                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                    <tr class=\"listrow\">\r\n                                        <td style=\"text-align: justify;\" id=\"lnk_testCaseName\">\r\n                                            ");
#nullable restore
#line 38 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
                                       Write(Html.HiddenFor(modelItem => item.Mapping_Id));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                            <a href=\"javascript:void(0)\" title=\"Edit.\"");
            BeginWriteAttribute("id", " id=\"", 2205, "\"", 2225, 1);
#nullable restore
#line 39 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
WriteAttributeValue("", 2210, item.Vendor_Id, 2210, 15, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            BeginWriteAttribute("onclick", " onclick=\"", 2226, "\"", 2309, 4);
            WriteAttributeValue("", 2236, "location.href=\'", 2236, 15, true);
#nullable restore
#line 39 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
WriteAttributeValue("", 2251, Url.Action("Edit", "Mapping"), 2251, 30, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 2281, "?vendor_Id=\'+", 2281, 13, true);
#nullable restore
#line 39 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
WriteAttributeValue("", 2294, item.Vendor_Id, 2294, 15, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                                ");
#nullable restore
#line 40 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
                                           Write(Html.DisplayFor(modelItem => item.Vendor_Name));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                            </a>\r\n                                        </td>\r\n                                        <td style=\"text-align: justify;\">\r\n                                            ");
#nullable restore
#line 44 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.Customer_Id_Alias));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                        </td>\r\n                                        <td>\r\n                                            ");
#nullable restore
#line 47 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.Invoice_Id_Alias));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                        </td>\r\n                                        <td style=\"text-align: justify;\">\r\n                                            ");
#nullable restore
#line 50 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.Invoice_Date_Alias));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                        </td>\r\n                                        <td style=\"text-align: justify;\">\r\n                                            ");
#nullable restore
#line 53 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.Product_Id_Alias));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                        </td>\r\n                                        <td style=\"text-align: justify;\">\r\n                                            ");
#nullable restore
#line 56 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.Product_Name_Alias));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                                        </td>
                                        <td class=""action-icon"">
                                            <div class=""text-center"" style=""width: 78px;"">
                                                <a href=""javascript:void(0)""");
            BeginWriteAttribute("id", " id=\"", 3817, "\"", 3838, 1);
#nullable restore
#line 60 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
WriteAttributeValue("", 3822, item.Mapping_Id, 3822, 16, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"edit_Mapping_Id\">\r\n                                                    <i class=\"fas fa-edit\"");
            BeginWriteAttribute("onclick", " onclick=\"", 3940, "\"", 4023, 4);
            WriteAttributeValue("", 3950, "location.href=\'", 3950, 15, true);
#nullable restore
#line 61 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
WriteAttributeValue("", 3965, Url.Action("Edit", "Mapping"), 3965, 30, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 3995, "?vendor_Id=\'+", 3995, 13, true);
#nullable restore
#line 61 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
WriteAttributeValue("", 4008, item.Vendor_Id, 4008, 15, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral("></i>\r\n                                                </a>\r\n                                                <a href=\"javascript:void(0)\"");
            BeginWriteAttribute("id", " id=\"", 4161, "\"", 4181, 1);
#nullable restore
#line 63 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
WriteAttributeValue("", 4166, item.Vendor_Id, 4166, 15, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" class=""Vendor_Id"">
                                                    <i class=""fa fa-trash""></i>
                                                </a>
                                            </div>
                                        </td>

                                    </tr>
");
#nullable restore
#line 70 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
                                }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                            </tbody>
                        </table>
                        <div class=""card-body"" id=""div_pagination"">
                            <div class=""row"">
                                <div class=""col-sm-6"">
                                    <div class=""dataTables_info"" id=""div_paginationInfo"" role=""status"" aria-live=""polite""></div>
                                </div>
                                <div class=""col-sm-6 "">
                                    <nav aria-label=""pagination"" class=""float-right"">
                                        <ul class=""pagination pagination-flat pagination-rounded"" id=""lst_pagination""></ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
");
            WriteLiteral("            </div>\r\n        </div>\r\n");
#nullable restore
#line 92 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
         if (ViewBag.Message != null)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <script>\r\n                    $(document).ready(function () {\r\n                        var msg_Id = \"");
#nullable restore
#line 96 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
                                 Write(ViewBag.Message_Id);

#line default
#line hidden
#nullable disable
            WriteLiteral("\";\r\n                        var msg = \"");
#nullable restore
#line 97 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
                              Write(ViewBag.Message);

#line default
#line hidden
#nullable disable
            WriteLiteral(@""";
                        if (msg_Id != null && msg_Id != undefined) {
                            if (msg_Id == Message_Ids.Insert || msg_Id == Message_Ids.Update || msg_Id == Message_Ids.Delete) {
                              //  ssi_modal.notify('success', { position: 'right top', content: msg });
                                 alert(msg);
                            }
                            else if (msg_Id == Message_Ids.Error) {
                                alert(msg);
                                //ssi_modal.notify('error', { position: 'right top', content: msg });
                            }
                            else {
                               // ssi_modal.notify('info', { position: 'right top', content: msg });
                                 alert(msg);
                            }
                        }
                });

            </script>
");
#nullable restore
#line 115 "C:\Minal\EDI\EDI_App\EDI_App\Views\Mapping\List.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("    </div>\r\n</main>\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<EDI_App.Models.Mapping_M>> Html { get; private set; }
    }
}
#pragma warning restore 1591
