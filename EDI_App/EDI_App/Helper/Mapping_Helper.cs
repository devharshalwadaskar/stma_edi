﻿using EDI_App.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace EDI_App.Helper
{
    public class Mapping_Helper
    {
        HttpClient client = new HttpClient();
        string uri = APIConnect.APIConnection;

        public List<Vendor_List> GetAll_Vendors()
        {
            List<Vendor_List> data = new List<Vendor_List>();
            try
            {
                client.BaseAddress = new Uri(uri);
                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);
                HttpResponseMessage response = client.GetAsync("/api/Mapping/GetAll_Vendors").Result;
                if (response.IsSuccessStatusCode)
                {
                    string stringData = response.Content.ReadAsStringAsync().Result;
                    data = JsonConvert.DeserializeObject<List<Vendor_List>>(stringData);
                }
            }
            catch (Exception ex)
            {
                // data = JsonConvert.DeserializeObject<List<Vendor_List>>(ex.Message);
            }
            return data;
        }

        public List<Mapping_M> GetAll_VendorMappingFields()
        {
            List<Mapping_M> data = new List<Mapping_M>();
            try
            {
                client.BaseAddress = new Uri(uri);
                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);
                HttpResponseMessage response = client.GetAsync("/api/Mapping/GetAll_VendorMappingFields").Result;
                if (response.IsSuccessStatusCode)
                {
                    string stringData = response.Content.ReadAsStringAsync().Result;
                    data = JsonConvert.DeserializeObject<List<Mapping_M>>(stringData);
                }
            }
            catch (Exception ex)
            {
                //  data = JsonConvert.DeserializeObject<Mapping_M>(ex.Message);
            }
            return data;
        }
        public Mapping_M Get_VendorMappingFields(long vendor_Id)
        {
            Mapping_M data = new Mapping_M();
            try
            {
                client.BaseAddress = new Uri(uri);
                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);
                HttpResponseMessage response = client.GetAsync("/api/Mapping/Get_VendorMappingFields?vendor_Id=" + vendor_Id).Result;
                if (response.IsSuccessStatusCode)
                {
                    string stringData = response.Content.ReadAsStringAsync().Result;
                    data = JsonConvert.DeserializeObject<Mapping_M>(stringData);
                }
            }
            catch (Exception ex)
            {
                //  data = JsonConvert.DeserializeObject<Mapping_M>(ex.Message);
            }
            return data;
        }

        //Delete Project
        public Operation_Message Delete_VendorMappingFields(long vendor_Id)
        {
            Operation_Message operation_Message = new Operation_Message();
            try
            {
                {
                    client.BaseAddress = new Uri(uri);
                    MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                    client.DefaultRequestHeaders.Accept.Add(contentType);
                    HttpResponseMessage response = client.GetAsync("/api/Mapping/Delete_VendorMappingFields?vendor_Id=" + vendor_Id).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        operation_Message = JsonConvert.DeserializeObject<Operation_Message>(response.Content.ReadAsStringAsync().Result);
                        if (operation_Message == null)
                        {
                            operation_Message.Return_Message_Id = (int)Messages_Ids.Error;
                            operation_Message.Return_Message = (string.IsNullOrEmpty(operation_Message.Return_Message)) ? "Failed to delete data" : operation_Message.Return_Message;
                        }
                    }
                }
            }
            catch (Exception _ex)
            {
                operation_Message.Is_success = false;
                operation_Message.Return_Message_Id = (int)Messages_Ids.Error;
                operation_Message.Return_Message = (string.IsNullOrEmpty(operation_Message.Return_Message)) ? "Failed to delete data" : operation_Message.Return_Message;

            }
            return operation_Message;
        }


        //Insert/Update 
        public Operation_Message Mapping_InsertUpdate(Mapping_M mapping)
        {
            string error = string.Empty;
            Operation_Message operation_Message = new Operation_Message();
            try
            {

                client.BaseAddress = new Uri(uri);
                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);

                string stringData = JsonConvert.SerializeObject(mapping);
                var contentData = new StringContent(stringData, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage _response = client.PostAsync("/api/Mapping/Mapping_InsertUpdate", contentData).Result;
                operation_Message = JsonConvert.DeserializeObject<Operation_Message>(_response.Content.ReadAsStringAsync().Result);
                if (operation_Message == null)
                {
                    operation_Message.Return_Message_Id = (int)Messages_Ids.Error;
                    operation_Message.Return_Message = (string.IsNullOrEmpty(operation_Message.Return_Message)) ? "Failed to insert/update data" : operation_Message.Return_Message;
                }
            }
            catch (Exception _ex)
            {
                operation_Message.Is_success = false;
                operation_Message.Return_Message_Id = (int)Messages_Ids.Error;
                operation_Message.Return_Message = (string.IsNullOrEmpty(operation_Message.Return_Message)) ? "Failed to insert/update data" : operation_Message.Return_Message;
            }
            return operation_Message;
        }

    }
}
