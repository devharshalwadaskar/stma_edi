﻿using EDI_App.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace EDI_App.Helper
{
    public class User_Helper 
    {
        HttpClient client = new HttpClient();
        string uri = APIConnect.APIConnection;

        public List<Role> GetAll_Roles() 
        {
            List<Role> data = new List<Role>();
            try
            {
                client.BaseAddress = new Uri(uri);
                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);
                HttpResponseMessage response = client.GetAsync("/api/User/GetAll_Roles").Result;
                if (response.IsSuccessStatusCode)
                {
                    string stringData = response.Content.ReadAsStringAsync().Result;
                    data = JsonConvert.DeserializeObject<List<Role>>(stringData);
                }
            }
            catch (Exception ex)
            {
                // data = JsonConvert.DeserializeObject<List<Vendor_List>>(ex.Message);
            }
            return data;
        }

        public List<User_M> GetAll_Users()
        {
            List<User_M> data = new List<User_M>();
            try
            {
                client.BaseAddress = new Uri(uri);
                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);
                HttpResponseMessage response = client.GetAsync("/api/User/GetAll_Users").Result;
                if (response.IsSuccessStatusCode)
                {
                    string stringData = response.Content.ReadAsStringAsync().Result;
                    data = JsonConvert.DeserializeObject<List<User_M>>(stringData);
                }
            }
            catch (Exception ex)
            {
                // data = JsonConvert.DeserializeObject<List<Vendor_List>>(ex.Message);
            }
            return data;
        }

        public User_M Get_User (long user_Id)
        {
            User_M data = new User_M();
            try
            {
                client.BaseAddress = new Uri(uri);
                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);
                HttpResponseMessage response = client.GetAsync("/api/User/Get_User?user_Id=" + user_Id).Result;
                if (response.IsSuccessStatusCode)
                {
                    string stringData = response.Content.ReadAsStringAsync().Result;
                    data = JsonConvert.DeserializeObject<User_M>(stringData);
                }
            }
            catch (Exception ex)
            {
                //  data = JsonConvert.DeserializeObject<Mapping_M>(ex.Message);
            }
            return data;
        }

        //Delete User
        public Operation_Message Delete_User(long user_Id)
        {
            Operation_Message operation_Message = new Operation_Message();
            try
            {
                {
                    client.BaseAddress = new Uri(uri);
                    MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                    client.DefaultRequestHeaders.Accept.Add(contentType);
                    HttpResponseMessage response = client.GetAsync("/api/User/Delete_User?user_Id=" + user_Id).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        operation_Message = JsonConvert.DeserializeObject<Operation_Message>(response.Content.ReadAsStringAsync().Result);
                        if (operation_Message == null)
                        {
                            operation_Message.Return_Message_Id = (int)Messages_Ids.Error;
                            operation_Message.Return_Message = (string.IsNullOrEmpty(operation_Message.Return_Message)) ? "Failed to delete data" : operation_Message.Return_Message;
                        }
                    }
                }
            }
            catch (Exception _ex)
            {
                operation_Message.Is_success = false;
                operation_Message.Return_Message_Id = (int)Messages_Ids.Error;
                operation_Message.Return_Message = (string.IsNullOrEmpty(operation_Message.Return_Message)) ? "Failed to delete data" : operation_Message.Return_Message;

            }
            return operation_Message;
        }


        //Insert/Update User
        public Operation_Message InsertUpdate_User(User_M user) 
        {
            string error = string.Empty;
            Operation_Message operation_Message = new Operation_Message();
            try
            {

                client.BaseAddress = new Uri(uri);
                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);

                string stringData = JsonConvert.SerializeObject(user);
                var contentData = new StringContent(stringData, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage _response = client.PostAsync("/api/User/InsertUpdate_User", contentData).Result;
                operation_Message = JsonConvert.DeserializeObject<Operation_Message>(_response.Content.ReadAsStringAsync().Result);
                if (operation_Message == null)
                {
                    operation_Message.Return_Message_Id = (int)Messages_Ids.Error;
                    operation_Message.Return_Message = (string.IsNullOrEmpty(operation_Message.Return_Message)) ? "Failed to insert/update data" : operation_Message.Return_Message;
                }
            }
            catch (Exception _ex)
            {
                operation_Message.Is_success = false;
                operation_Message.Return_Message_Id = (int)Messages_Ids.Error;
                operation_Message.Return_Message = (string.IsNullOrEmpty(operation_Message.Return_Message)) ? "Failed to insert/update data" : operation_Message.Return_Message;
            }
            return operation_Message;
        }


        //Search Users
        public List<States> Search_User(string name) 
        {
            List<States> data = new List<States>();
            try
            {
                client.BaseAddress = new Uri(uri);
                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);
                HttpResponseMessage response = client.GetAsync("/api/User/name?user=" + name).Result; 
                if (response.IsSuccessStatusCode)
                {
                    string stringData = response.Content.ReadAsStringAsync().Result;
                    data = JsonConvert.DeserializeObject<List<States>>(stringData);
                }
            }
            catch (Exception ex)
            {
                // data = JsonConvert.DeserializeObject<List<Vendor_List>>(ex.Message);
            }
            return data;
        }
    }
}
