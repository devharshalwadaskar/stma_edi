﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EDI_App.Helper
{
    public class APIConnect
    {
        public static string APIConnection
        {
            get
            {
                return "https://localhost:44397/api";
            }
        }
    }
}
