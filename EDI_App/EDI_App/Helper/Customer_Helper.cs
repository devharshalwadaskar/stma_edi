﻿using EDI_App.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace EDI_App.Helper
{
    public class Customer_Helper
    {
        HttpClient client = new HttpClient();
        string uri = APIConnect.APIConnection;

        public List<Customer_Type> GetAll_Customer_Types()  
        {
            List<Customer_Type> data = new List<Customer_Type>();
            try
            {
                client.BaseAddress = new Uri(uri);
                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);
                HttpResponseMessage response = client.GetAsync("/api/Customer/GetAll_Customer_Types").Result;
                if (response.IsSuccessStatusCode)
                {
                    string stringData = response.Content.ReadAsStringAsync().Result;
                    data = JsonConvert.DeserializeObject<List<Customer_Type>>(stringData);
                }
            }
            catch (Exception ex)
            {
                // data = JsonConvert.DeserializeObject<List<Vendor_List>>(ex.Message);
            }
            return data;
        }

        public List<Customer_M> GetAll_Customers()
        {
            List<Customer_M> data = new List<Customer_M>();
            try
            {
                client.BaseAddress = new Uri(uri);
                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);
                HttpResponseMessage response = client.GetAsync("/api/Customer/GetAll_Customers").Result;
                if (response.IsSuccessStatusCode)
                {
                    string stringData = response.Content.ReadAsStringAsync().Result;
                    data = JsonConvert.DeserializeObject<List<Customer_M>>(stringData);
                }
            }
            catch (Exception ex)
            {
                // data = JsonConvert.DeserializeObject<List<Vendor_List>>(ex.Message);
            }
            return data;
        }

         public Customer_M Get_Customer(long customer_Id) 
        {
            Customer_M data = new Customer_M();
            try
            {
                client.BaseAddress = new Uri(uri);
                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);
                HttpResponseMessage response = client.GetAsync("/api/Customer/Get_Customer?customer_Id=" + customer_Id).Result;
                if (response.IsSuccessStatusCode)
                {
                    string stringData = response.Content.ReadAsStringAsync().Result;
                    data = JsonConvert.DeserializeObject<Customer_M>(stringData);
                }
            }
            catch (Exception ex)
            {
                //  data = JsonConvert.DeserializeObject<Mapping_M>(ex.Message);
            }
            return data;
        }

        //Delete Customer
        public Operation_Message Delete_Customer(long customer_Id) 
        {
            Operation_Message operation_Message = new Operation_Message();
            try
            {
                {
                    client.BaseAddress = new Uri(uri);
                    MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                    client.DefaultRequestHeaders.Accept.Add(contentType);
                    HttpResponseMessage response = client.GetAsync("/api/Customer/Delete_Customer?customer_Id=" + customer_Id).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        operation_Message = JsonConvert.DeserializeObject<Operation_Message>(response.Content.ReadAsStringAsync().Result);
                        if (operation_Message == null)
                        {
                            operation_Message.Return_Message_Id = (int)Messages_Ids.Error;
                            operation_Message.Return_Message = (string.IsNullOrEmpty(operation_Message.Return_Message)) ? "Failed to delete data" : operation_Message.Return_Message;
                        }
                    }
                }
            }
            catch (Exception _ex)
            {
                operation_Message.Is_success = false;
                operation_Message.Return_Message_Id = (int)Messages_Ids.Error;
                operation_Message.Return_Message = (string.IsNullOrEmpty(operation_Message.Return_Message)) ? "Failed to delete data" : operation_Message.Return_Message;

            }
            return operation_Message;
        }


        //Insert/Update Customer
        public Operation_Message InsertUpdate_Customer(Customer_M customer)
        { 
            string error = string.Empty;
            Operation_Message operation_Message = new Operation_Message();
            try
            {

                client.BaseAddress = new Uri(uri);
                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);

                string stringData = JsonConvert.SerializeObject(customer);
                var contentData = new StringContent(stringData, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage _response = client.PostAsync("/api/Customer/InsertUpdate_Customer", contentData).Result;
                operation_Message = JsonConvert.DeserializeObject<Operation_Message>(_response.Content.ReadAsStringAsync().Result);
                if (operation_Message == null)
                {
                    operation_Message.Return_Message_Id = (int)Messages_Ids.Error;
                    operation_Message.Return_Message = (string.IsNullOrEmpty(operation_Message.Return_Message)) ? "Failed to insert/update data" : operation_Message.Return_Message;
                }
            }
            catch (Exception _ex)
            {
                operation_Message.Is_success = false;
                operation_Message.Return_Message_Id = (int)Messages_Ids.Error;
                operation_Message.Return_Message = (string.IsNullOrEmpty(operation_Message.Return_Message)) ? "Failed to insert/update data" : operation_Message.Return_Message;
            }
            return operation_Message;
        }


        //State
        public List<States> GetAll_States()  
        {
            List<States> data = new List<States>();
            try
            {
                client.BaseAddress = new Uri(uri);
                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);
                HttpResponseMessage response = client.GetAsync("/api/Customer/GetAll_States").Result;
                if (response.IsSuccessStatusCode)
                {
                    string stringData = response.Content.ReadAsStringAsync().Result;
                    data = JsonConvert.DeserializeObject<List<States>>(stringData);
                }
            }
            catch (Exception ex)
            {
                // data = JsonConvert.DeserializeObject<List<Vendor_List>>(ex.Message);
            }
            return data;
        }
    }
}
