﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using EDI_App.Models;
using Newtonsoft.Json;

namespace EDI_App.Helper
{
    public class Vendor_Helper
    {

        HttpClient client = new HttpClient();
        string uri = APIConnect.APIConnection;

        public List<Vendor_M> Get_Vendors()
        {
            List<Vendor_M> data = new List<Vendor_M>();
            try
            {
                client.BaseAddress = new Uri(uri);
                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);
                HttpResponseMessage response = client.GetAsync("/api/Vendor/Get_Vendors").Result;
                if (response.IsSuccessStatusCode)
                {
                    string stringData = response.Content.ReadAsStringAsync().Result;
                    data = JsonConvert.DeserializeObject<List<Vendor_M>>(stringData);
                }
            }
            catch (Exception ex)
            {
                // data = JsonConvert.DeserializeObject<List<Vendor_List>>(ex.Message);
            }
            return data;
        }


        //Get specific vendor details


        public Vendor_M Get_VendorDetails(long vendor_Id)
        {
            Vendor_M data = new Vendor_M();
            try
            {
                client.BaseAddress = new Uri(uri);
                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);
                //HttpResponseMessage response = client.GetAsync("/api/Vendor/Get_Vendors").Result;
                HttpResponseMessage response = client.GetAsync("/api/Vendor/Get_VendorDetails?vendor_Id=" + vendor_Id).Result;

                if (response.IsSuccessStatusCode)
                {
                    string stringData = response.Content.ReadAsStringAsync().Result;
                    data = JsonConvert.DeserializeObject<Vendor_M>(stringData);
                }
            }
            catch (Exception ex)
            {
                // data = JsonConvert.DeserializeObject<List<Vendor_List>>(ex.Message);
            }
            return data;
        }


        //insert 

        public Operation_Message Vendor_Insert(Vendor_M vendor)
        {
            string error = string.Empty;
            Operation_Message operation_Message = new Operation_Message();
            try
            {

                client.BaseAddress = new Uri(uri);
                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);

                string stringData = JsonConvert.SerializeObject(vendor);
                var contentData = new StringContent(stringData, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage _response = client.PostAsync("/api/Vendor/Insert_Vendor", contentData).Result;
                operation_Message = JsonConvert.DeserializeObject<Operation_Message>(_response.Content.ReadAsStringAsync().Result);
                if (operation_Message == null)
                {
                    operation_Message.Return_Message_Id = (int)Messages_Ids.Error;
                    operation_Message.Return_Message = (string.IsNullOrEmpty(operation_Message.Return_Message)) ? "Failed to insert/update data" : operation_Message.Return_Message;
                }
            }
            catch (Exception _ex)
            {
                operation_Message.Is_success = false;
                operation_Message.Return_Message_Id = (int)Messages_Ids.Error;
                operation_Message.Return_Message = (string.IsNullOrEmpty(operation_Message.Return_Message)) ? "Failed to insert/update data" : operation_Message.Return_Message;
            }
            return operation_Message;
        }

        //Delete Vendor


        public Operation_Message Delete_Vendor(long vendor_Id)
        {
            Operation_Message operation_Message = new Operation_Message();
            try
            {
                {
                    client.BaseAddress = new Uri(uri);
                    MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                    client.DefaultRequestHeaders.Accept.Add(contentType);
                    HttpResponseMessage response = client.GetAsync("/api/Vendor/Delete_Vendor?vendor_Id=" + vendor_Id).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        operation_Message = JsonConvert.DeserializeObject<Operation_Message>(response.Content.ReadAsStringAsync().Result);
                        if (operation_Message == null)
                        {
                            operation_Message.Return_Message_Id = (int)Messages_Ids.Error;
                            operation_Message.Return_Message = (string.IsNullOrEmpty(operation_Message.Return_Message)) ? "Failed to delete data" : operation_Message.Return_Message;
                        }
                    }
                }
            }
            catch (Exception _ex)
            {
                operation_Message.Is_success = false;
                operation_Message.Return_Message_Id = (int)Messages_Ids.Error;
                operation_Message.Return_Message = (string.IsNullOrEmpty(operation_Message.Return_Message)) ? "Failed to delete data" : operation_Message.Return_Message;

            }
            return operation_Message;
        }
        //update vendor

        public Operation_Message Update_Vendor(Vendor_M vendor)
        {
            string error = string.Empty;
            Operation_Message operation_Message = new Operation_Message();
            try
            {

                client.BaseAddress = new Uri(uri);
                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);

                string stringData = JsonConvert.SerializeObject(vendor);
                var contentData = new StringContent(stringData, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage _response = client.PostAsync("/api/Vendor/Update_Vendor", contentData).Result;

                operation_Message = JsonConvert.DeserializeObject<Operation_Message>(_response.Content.ReadAsStringAsync().Result);
                if (operation_Message == null)
                {
                    operation_Message.Return_Message_Id = (int)Messages_Ids.Error;
                    operation_Message.Return_Message = (string.IsNullOrEmpty(operation_Message.Return_Message)) ? "Failed to insert/update data" : operation_Message.Return_Message;
                }
            }
            catch (Exception _ex)
            {
                operation_Message.Is_success = false;
                operation_Message.Return_Message_Id = (int)Messages_Ids.Error;
                operation_Message.Return_Message = (string.IsNullOrEmpty(operation_Message.Return_Message)) ? "Failed to insert/update data" : operation_Message.Return_Message;
            }
            return operation_Message;
        }


        //Zip
        public List<Zips> GetAll_Zip(long state_Id, string search_Text)
        {
            List<Zips> data = new List<Zips>();
            try
            {
                client.BaseAddress = new Uri(uri);
                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);


                HttpResponseMessage response = client.GetAsync("/api/Vendor/GetAll_Zip?state_Id=" + state_Id + "&search_Text=" + search_Text).Result;
                if (response.IsSuccessStatusCode)
                {
                    string stringData = response.Content.ReadAsStringAsync().Result;
                    data = JsonConvert.DeserializeObject<List<Zips>>(stringData);
                }


            }
            catch (Exception ex)
            {
                // data = JsonConvert.DeserializeObject<List<Vendor_List>>(ex.Message);
            }
            return data;
        }
    }
}
