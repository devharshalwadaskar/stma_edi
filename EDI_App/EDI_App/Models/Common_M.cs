﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EDI_App.Models
{
    public class Common_M
    {
    }

    public class Operation_Message
    {
        public Int16 Operation_Type { get; set; }
        public Int64 Returned_Id { get; set; } // Id returned by the function
        public string Return_Message { get; set; } //Error Message 
        public int Return_Message_Id { get; set; } //Error Message  
        public bool Is_success { get; set; } // Success status either 0 or 1 
        public string Other_Data { get; set; } //Keep other data if any present
        public object Data_Select { get; set; } // Data in case of select
    }

    public enum Messages_Ids
    {
        Error = 0,
        Insert = 1,
        Update = 2,
        Delete = 3,
        Other = 4,
        Exists = 5,
        UnAuthorized = 6,
        Not_Exists = 7
    }
}
