﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EDI_App.Models
{
    public class Vendor_M
    {


        public long Vendor_Id { get; set; }
        public string Vendor_Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Zip { get; set; }
        public string Phone_Number { get; set; }
        public string Email_Address { get; set; }
        public string Secondary_Email_Address { get; set; }
        public string Fax_Number { get; set; }
        public string Tax_Number { get; set; }
        public string Contact_Person { get; set; }
        public long State_Id { get; set; }
        public string State { get; set; }
        public string Website { get; set; }
        public string Vendor_Code { get; set; }
        public char Mode { get; set; }

        public long Zip_Code_Id { get; set; }
       public string Zip_Code  { get; set; }
    }

   
}
