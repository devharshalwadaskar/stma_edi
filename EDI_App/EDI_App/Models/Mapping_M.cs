﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EDI_App.Models
{
    public class Mapping_M
    {
        public string Invoice_Id_Alias { get; set; }
        public string Invoice_Date_Alias { get; set; }
        public long Vendor_Id { get; set; }
        public string Vendor_Name { get; set; }
        public string Customer_Id_Alias { get; set; }
        public string Customer_Name_Alias { get; set; }
        public string Product_Id_Alias { get; set; }
        public string Product_Name_Alias { get; set; }
        public string Product_Quantity_Alias { get; set; }
        public string Product_Unit_Alias { get; set; }
        public string Product_Price_Alias { get; set; }
        public string Product_Description_Alias { get; set; }
        public string Product_Discount_Alias { get; set; }
        public string Product_Amount_Alias { get; set; }
        public string Invoice_Amount_Alias { get; set; }
        public long Mapping_Id { get; set; }
        public char Mode { get; set; }
        //public long Fields_Id { get; set; }
        //public long Field_Alias { get; set; } 

    }

    public class Vendor_List
    {
        public long Vendor_Id { get; set; }
        public string Vendor_Name { get; set; }
    }
    //public class Vendor_Mapping_Insert
    //{
    //    public long Mapping_Id { get; set; }
    //    public long Fields_Id { get; set; }

    //}
}
