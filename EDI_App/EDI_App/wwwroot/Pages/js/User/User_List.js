﻿
$(document).ready(function () {
    $('body').on('click', "#tbl_UserList a.User_Id", function () {
        var user_Id = $(this).attr('id');
        if (user_Id > 0) {
            confirmDeleteConfirmation(user_Id);
        }
    });
});

//confirmation box to delete test case
function confirmDeleteConfirmation(user_Id) {
    if (confirm('Do you really want to delete details?')) {
        var user_Confirmation_To_Delete = false;
        Delete_UserConfirmation(user_Id, user_Confirmation_To_Delete);
    }
}



//delete
function Delete_UserConfirmation(user_Id) {
    $.ajax({
        type: "GET",
        url: '/User/Delete',
        async: false,
        data: { user_Id: user_Id }, 
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (response) {
            if (response != null) {
                if (response.return_Message != undefined && response.return_Message != null) {
                    if (response.return_Message_Id == Message_Ids.Delete) {
                        alert(response.return_Message);
                        location.href = "/User/List";
                    }
                    else if (response.return_Message_Id == Message_Ids.Error) {
                        alert(response.return_Message);
                        location.href = "/User/List";
                    }
                    else if (response.return_Message_Id == Message_Ids.Other) {
                        alert(response.return_Message);
                        location.href = "/User/List";
                    }
                    else {
                        alert(response.return_Message);
                        location.href = "/User/List";
                    }
                }
            }
            else {

                alert(response.responseText);
            }
        },
        failure: function (response) {
            alert(response.responseText);
        },
        error: function (response) {
            alert(response.responseText);
        }
    });
}

