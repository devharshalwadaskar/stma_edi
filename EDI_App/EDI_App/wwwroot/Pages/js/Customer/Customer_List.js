﻿
$(document).ready(function () {
    $('body').on('click', "#tbl_CustomerList a.Customer_Id", function () {
        var customer_Id = $(this).attr('id');
        if (customer_Id > 0) {
            confirmDeleteConfirmation(customer_Id);
        }
    });
});

//confirmation box to delete test case
function confirmDeleteConfirmation(customer_Id) {
    if (confirm('Do you really want to delete details?')) {
        var user_Confirmation_To_Delete = false;
        Delete_MappingConfirmation(customer_Id, user_Confirmation_To_Delete);
    }
}

function Delete_MappingConfirmation(customer_Id) {
    $.ajax({
        type: "GET",
        url: '/Customer/Delete',
        async: false,
        data: { customer_Id: customer_Id },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (response) {
            if (response != null) {
                if (response.return_Message != undefined && response.return_Message != null) {
                    if (response.return_Message_Id == Message_Ids.Delete) {
                        alert(response.return_Message);
                        location.href="/Customer/List/"
                    }
                    else if (response.return_Message_Id == Message_Ids.Error) {
                        alert(response.return_Message);
                    }
                    else if (response.return_Message_Id == Message_Ids.Other) {
                        alert(response.return_Message);
                    }
                    else {
                        alert(response.return_Message);
                    }
                }
            }
            else {

                ssi_modal.notify('error', { position: 'right top', content: response.message })
            }
        },
        failure: function (response) {
            alert(response.responseText);
        },
        error: function (response) {
            alert(response.responseText);
        }
    });
}

