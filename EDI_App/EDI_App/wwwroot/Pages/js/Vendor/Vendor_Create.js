﻿$(document).ready(function () {


    $("#val_Vendor_Name").hide();
    $("#val_Vendor_Id").hide();
    $("#val_Vendor_Address").hide();
    $("#val_Vendor_City").hide();
    $("#val_Vendor_Country").hide();
    $("#val_Vendor_Zip").hide();
    $("#val_Vendor_Phone_Number").hide();
    $("#val_Vendor_Email_Address").hide();
    $("#val_Vendor_Secondary_Email_Address").hide();
    $("#val_Vendor_Fax_Number").hide();
    $("#val_Vendor_Tax_Number").hide();
    $("#val_Vendor_Contact_Person").hide();
    $("#val_Vendor_State").hide();
    $("#val_Vendor_Website").hide();
    $("#val_Vendor_Code").hide();




    //validation for fields
    $('#txt_Vendor_Name').keyup(function (e) {
        if ($("#txt_Vendor_Name").val().length > 0) {
            $("#val_Vendor_Name").hide();
        }
        else {
            $("#val_Vendor_Name").show();
        }
    });

    //validation for fields
    $('#txt_Vendor_Address').keyup(function (e) {
        if ($("#txt_Vendor_Address").val().length > 0) {
            $("#val_Vendor_Address").hide();
        }
        else {
            $("#val_Vendor_Address").show();
        }
    });

    //validation for fields
    $('#txt_Vendor_City').keyup(function (e) {
        if ($("#txt_Vendor_City").val().length > 0) {
            $("#val_Vendor_City").hide();
        }
        else {
            $("#val_Vendor_City").show();
        }
    });

    //validation for fields
    $('#txt_Vendor_Country').keyup(function (e) {
        if ($("#txt_Vendor_Country").val().length > 0) {
            $("#val_Vendor_Country").hide();
        }
        else {
            $("#val_Vendor_Country").show();
        }
    });

    //validation for fields
    $('#txt_Vendor_Zip').keyup(function (e) {
        if ($("#txt_Vendor_Zip").val().length > 0) {
            $("#val_Vendor_Zip").hide();
        }
        else {
            $("#val_Vendor_Zip").show();
        }
    });

    //validation for fields
    $('#txt_Vendor_Phone_Number').keyup(function (e) {
        if ($("#txt_Vendor_Phone_Number").val().length > 0) {
            $("#val_Vendor_Phone_Number").hide();
        }
        else {
            $("#val_Vendor_Phone_Number").show();
        }
    });

    //validation for fields
    $('#txt_Vendor_Email_Address').keyup(function (e) {
        if ($("#txt_Vendor_Email_Address").val().length > 0) {
            $("#val_Vendor_Email_Address").hide();
        }
        else {
            $("#val_Vendor_Email_Address").show();
        }
    });






    //validation for fields
    $('#txt_Vendor_State').keyup(function (e) {
        if ($("#txt_Vendor_State").val().length > 0) {
            $("#val_Vendor_State").hide();
        }
        else {
            $("#val_Vendor_State").show();
        }
    });


    //validation for fields
    $('#txt_Vendor_State').keyup(function (e) {
        if ($("#txt_Vendor_State").val().length > 0) {
            $("#val_Vendor_State").hide();
        }
        else {
            $("#val_Vendor_State").show();
        }
    });

    //validation for fields
    $('#txt_Vendor_Code').keyup(function (e) {
        if ($("#txt_Vendor_Code").val().length > 0) {
            $("#val_Vendor_Code").hide();
        }
        else {
            $("#val_Vendor_Code").show();
        }
    });







    $('#btn_Submit').click(function () {
        var valError_status = false;

        if ($('#txt_Vendor_Name').val() == '' || $('#txt_Vendor_Name').val() <= 0) {
            $("#val_Vendor_Name").show();
            // $('#val_Customer_Id_Alias').css('visibility', 'visible');
            valError_status = true;
        }

        if ($('#txt_Vendor_Address').val() == '' || $('#txt_Vendor_Address').val() <= 0) {
            $("#val_Vendor_Address").show();
            // $('#val_Customer_Id_Alias').css('visibility', 'visible');
            valError_status = true;
        }
        if ($('#txt_Vendor_City').val() == '' || $('#txt_Vendor_City').val() <= 0) {
            $("#val_Vendor_City").show();
            valError_status = true;
        }
        if ($('#txt_Vendor_Country').val() == '' || $('#txt_Vendor_Country').val() <= 0) {
            $("#val_Vendor_Country").show();
            valError_status = true;
        }

        if ($('#txt_Vendor_Zip').val() == '' || $('#txt_Vendor_Zip').val() <= 0) {
            $("#val_Vendor_Zip").show();
            valError_status = true;
        }

        if ($('#txt_Vendor_Phone_Number').val() == '' || $('#txt_Vendor_Phone_Number').val() <= 0) {
            $("#val_Vendor_Phone_Number").show();
            valError_status = true;
        }

        if ($('#txt_Vendor_Email_Address').val() == '' || $('#txt_Vendor_Email_Address').val() <= 0) {
            $("#val_Vendor_Email_Address").show();
            valError_status = true;
        }




        if ($('#txt_Vendor_State').val() == '' || $('#txt_Vendor_State').val() <= 0) {
            $("#val_Vendor_State").show();
            valError_status = true;
        }



        if ($('#txt_Vendor_Code').val() == '' || $('#txt_Vendor_Code').val() <= 0) {
            $("#val_Vendor_Code").show();
            // $('#val_Customer_Id_Alias').css('visibility', 'visible');
            valError_status = true;
        }


        if (valError_status == false) {
            $.ajax({
                type: "post",
                url: "/Vendor/Create",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (response) {
                    if (response != null) {
                        alert(data);
                        location.href = '/Vendor/List/';
                    } else {

                    }
                },
                failure: function (response) {

                },
                error: function (response) {

                }
            });
        }
    })


    $('#btn_Clear').click(function () {

        $("#txt_Vendor_Code").text("");
        $("#txt_Vendor_Code").val("");
        $("#val_Vendor_Name").hide();
        $("#val_Vendor_Address").hide();
        $("#val_Vendor_City").hide();
        $("#val_Vendor_Country").hide();
        $("#val_Vendor_Zip").hide();
        $("#val_Vendor_Phone_Number").hide();
        $("#val_Vendor_Email_Address").hide();
        $("#val_Vendor_Secondary_Email_Address").hide();
        $("#val_Vendor_Fax_Number").hide();
        $("#val_Vendor_Tax_Number").hide();
        $("#val_Vendor_Contact_Person").hide();
        $("#val_Vendor_State").hide();
        $("#val_Vendor_Website").hide();
        $("#val_Vendor_Code").hide();
        $("#val_Vendor_Id").hide();
    })

})

function GetAll_States_Json() {
    $.ajax({
        type: "GET",
        url: "Customers/GetAll_States_Json/",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (data) {
            if (data.response != null) {
                if (data.response.lst.length > 0) {
                    var _html = "<option value='0'>Please Select</option>";
                    $(data.response.lst).each(function (i, data) {
                        _html += "<option value=" + data.state_Id + ">" + data.state + "</option>";
                    });
                    $("#ddl_States").html(_html);
                }
            }
            else {
                $("#ddl_States").html("<option value>Please Select</option>");
                $('#hdn_State').val(null);
            }
        },
        failure: function (data) {
            $("#ddl_States").html("<option >Please Select</option>");
            $('#hdn_State').val(null);
        },
        error: function (data) {
            $("#ddl_States").html("<option >Please Select</option>");
            $('#hdn_State').val(null);
        }
    });

}

function Get_VendorDetails(vendor_Id) {


    $.ajax(
        {
            type: "get",
            url: "Vendor/Get_VendorDetails/",
            dataType: "json",
            data: { vendor_Id: vendor_Id },
            cache: false,
            success: function (response) {
                if (response.vendor_Id != 0) {
                    $('#txt_Vendor_Id').val(response.Vendor_Id);
                    $('#txt_Vendor_Name').val(response.Vendor_Name);
                    $('#txt_Vendor_Address').val(response.Vendor_Address);
                    $('#txt_Vendor_City').val(response.Vendor_City);
                    $('#txt_Vendor_Country').val(response.Vendor_Country);
                    $('#txt_Vendor_Zip').val(response.Vendor_Zip);
                    $('#txt_Vendor_Phone_Number').val(response.Vendor_Phone_Number);
                    $('#txt_Vendor_Email_Address').val(response.Vendor_Email_Address);
                    $('#txt_Vendor_Secondary_Email_Address').val(response.Vendor_Secondary_Email_Address);
                    $('#txt_Vendor_Fax_Number').val(response.Vendor_Fax_Number);
                    $('#txt_Vendor_Tax_Number').val(response.Vendor_Tax_Number);
                    $('#txt_Vendor_Contact_Person').val(response.Vendor_Contact_Person);
                    $('#txt_Vendor_State').val(response.Vendor_State);
                    $('#txt_Vendor_Website').val(response.Vendor_Website);
                    $('#txt_Vendor_Code').val(response.Vendor_Code);
                }
            }
        })
}










//---------------------------------
//$(document).ready(function () {


//    $("#val_Vendor_Name").hide();
//    $("#val_Vendor_Address").hide();
//    $("#val_Vendor_City").hide();
//    $("#val_Vendor_Country").hide();
//    $("#val_Vendor_Zip").hide();
//    $("#val_Vendor_Phone_Number").hide();
//    $("#val_Vendor_Email_Address").hide();
//    $("#val_Vendor_Secondary_Email_Address").hide();
//    $("#val_Vendor_Fax_Number").hide();
//    $("#val_Vendor_Tax_Number").hide();
//    $("#val_Vendor_Contact_Person").hide();
//    $("#val_Vendor_State").hide();
//    $("#val_Vendor_Website").hide();
//    $("#val_Vendor_Code").hide();





//    //validation for fields
//    $('#txt_Vendor_Name').keyup(function (e) {
//        if ($("#txt_Vendor_Name").val().length > 0) {
//            $("#val_Vendor_Name").hide();
//        }
//        else {
//            $("#val_Vendor_Name").show();
//        }
//    });

//    //validation for fields
//    $('#txt_Vendor_Address').keyup(function (e) {
//        if ($("#txt_Vendor_Address").val().length > 0) {
//            $("#val_Vendor_Address").hide();
//        }
//        else {
//            $("#val_Vendor_Address").show();
//        }
//    });

//    //validation for fields
//    $('#txt_Vendor_City').keyup(function (e) {
//        if ($("#txt_Vendor_City").val().length > 0) {
//            $("#val_Vendor_City").hide();
//        }
//        else {
//            $("#val_Vendor_City").show();
//        }
//    });

//    //validation for fields
//    $('#txt_Vendor_Country').keyup(function (e) {
//        if ($("#txt_Vendor_Country").val().length > 0) {
//            $("#val_Vendor_Country").hide();
//        }
//        else {
//            $("#val_Vendor_Country").show();
//        }
//    });

//    //validation for fields
//    $('#txt_Vendor_Zip').keyup(function (e) {
//        if ($("#txt_Vendor_Zip").val().length > 0) {
//            $("#val_Vendor_Zip").hide();
//        }
//        else {
//            $("#val_Vendor_Zip").show();
//        }
//    });

//    //validation for fields
//    $('#txt_Vendor_Phone_Number').keyup(function (e) {
//        if ($("#txt_Vendor_Phone_Number").val().length > 0) {
//            $("#val_Vendor_Phone_Number").hide();
//        }
//        else {
//            $("#val_Vendor_Phone_Number").show();
//        }
//    });

//    //validation for fields
//    $('#txt_Vendor_Email_Address').keyup(function (e) {
//        if ($("#txt_Vendor_Email_Address").val().length > 0) {
//            $("#val_Vendor_Email_Address").hide();
//        }
//        else {
//            $("#val_Vendor_Email_Address").show();
//        }
//    });

//    //validation for fields
//    $('#txt_Vendor_Secondary_Email_Address').keyup(function (e) {
//        if ($("#txt_Vendor_Secondary_Email_Address").val().length > 0) {
//            $("#val_Vendor_Secondary_Email_Address").hide();
//        }
//        else {
//            $("#val_Vendor_Secondary_Email_Address").show();
//        }
//    });

//    //validation for fields
//    $('#txt_Vendor_Fax_Number').keyup(function (e) {
//        if ($("#txt_Vendor_Fax_Number").val().length > 0) {
//            $("#val_Vendor_Fax_Number").hide();
//        }
//        else {
//            $("#val_Product_Amount_Alias").show();
//        }
//    });

//    //validation for fields
//    $('#txt_Vendor_Tax_Number').keyup(function (e) {
//        if ($("#txt_Vendor_Tax_Number").val().length > 0) {
//            $("#val_Vendor_Tax_Number").hide();
//        }
//        else {
//            $("#val_Vendor_Tax_Number").show();
//        }
//    });

//    //validation for fields
//    $('#txt_Vendor_Contact_Person').keyup(function (e) {
//        if ($("#txt_Vendor_Contact_Person").val().length > 0) {
//            $("#val_Vendor_Contact_Person").hide();
//        }
//        else {
//            $("#val_Vendor_Contact_Person").show();
//        }
//    });


//    //validation for fields
//    $('#txt_Vendor_State').keyup(function (e) {
//        if ($("#txt_Vendor_State").val().length > 0) {
//            $("#val_Vendor_State").hide();
//        }
//        else {
//            $("#val_Vendor_State").show();
//        }
//    });


//    //validation for fields
//    $('#txt_Vendor_Website').keyup(function (e) {
//        if ($("#txt_Vendor_Website").val().length > 0) {
//            $("#val_Vendor_Website").hide();
//        }
//        else {
//            $("#val_Vendor_Website").show();
//        }
//    });
//    //validation for fields
//    $('#txt_Vendor_Code').keyup(function (e) {
//        if ($("#txt_Vendor_Code").val().length > 0) {
//            $("#val_Vendor_Code").hide();
//        }
//        else {
//            $("#val_Vendor_Code").show();
//        }
//    });



//    $('#btn_Submit').click(function () {
//        var valError_status = false;

//        if ($('#txt_Vendor_Name').val() == '' || $('#txt_Vendor_Name').val() <= 0) {
//            $("#val_Vendor_Name").show();
//            // $('#val_Customer_Id_Alias').css('visibility', 'visible');
//            valError_status = true;
//        }

//        if ($('#txt_Vendor_Address').val() == '' || $('#txt_Vendor_Address').val() <= 0) {
//            $("#val_Vendor_Address").show();
//            // $('#val_Customer_Id_Alias').css('visibility', 'visible');
//            valError_status = true;
//        }
//        if ($('#txt_Vendor_City').val() == '' || $('#txt_Vendor_City').val() <= 0) {
//            $("#val_Vendor_City").show();
//            valError_status = true;
//        }
//        if ($('#txt_Vendor_Country').val() == '' || $('#txt_Vendor_Country').val() <= 0) {
//            $("#val_Vendor_Country").show();
//            valError_status = true;
//        }

//        if ($('#txt_Vendor_Zip').val() == '' || $('#txt_Vendor_Zip').val() <= 0) {
//            $("#val_Vendor_Zip").show();
//            valError_status = true;
//        }

//        if ($('#txt_Vendor_Phone_Number').val() == '' || $('#txt_Vendor_Phone_Number').val() <= 0) {
//            $("#val_Vendor_Phone_Number").show();
//            valError_status = true;
//        }

//        if ($('#txt_Vendor_Email_Address').val() == '' || $('#txt_Vendor_Email_Address').val() <= 0) {
//            $("#val_Vendor_Email_Address").show();
//            valError_status = true;
//        }

//        if ($('#txt_Vendor_Secondary_Email_Address').val() == '' || $('#txt_Vendor_Secondary_Email_Address').val() <= 0) {
//            $("#val_Vendor_Secondary_Email_Address").show();
//            valError_status = true;
//        }

//        if ($('#txt_Vendor_Fax_Number').val() == '' || $('#txt_Vendor_Fax_Number').val() <= 0) {
//            $("#val_Vendor_Fax_Number").show();
//            valError_status = true;
//        }

//        if ($('#txt_Vendor_Tax_Number').val() == '' || $('#txt_Vendor_Tax_Number').val() <= 0) {
//            $("#val_Vendor_Tax_Number").show();
//            valError_status = true;
//        }

//        if ($('#txt_Vendor_Contact_Person').val() == '' || $('#txt_Vendor_Contact_Person').val() <= 0) {
//            $("#val_Vendor_Contact_Person").show();
//            valError_status = true;
//        }


//        if ($('#txt_Vendor_State').val() == '' || $('#txt_Vendor_State').val() <= 0) {
//            $("#val_Vendor_State").show();
//            valError_status = true;
//        }

//        if ($('#txt_Vendor_Website').val() == '' || $('#txt_Vendor_Website').val() <= 0) {
//            $("#val_Vendor_Website").show();
//            valError_status = true;
//        }
//        if ($('#txt_Vendor_Code').val() == '' || $('#txt_Vendor_Code').val() <= 0) {
//            $("#val_Vendor_Code").show();
//            valError_status = true;
//        }








//        if (valError_status == false) {
//            $.ajax({
//                type: "post",
//                url: "/Vendor/Create",
//                contentType: "application/json; charset=utf-8",
//                dataType: "json",
//                cache: false,
//                success: function (response) {
//                    if (response != null) {
//                        alert(data);
//                        location.href = '/Vendor/List/';
//                    } else {

//                    }
//                },
//                failure: function (response) {

//                },
//                error: function (response) {

//                }
//            });
//        }
//    })


//    $('#btn_Clear').click(function () {
//        $("#val_Vendor_Name").hide();
//        $("#val_Vendor_Address").hide();
//        $("#val_Vendor_City").hide();
//        $("#val_Vendor_Country").hide();
//        $("#val_Vendor_Zip").hide();
//        $("#val_Vendor_Phone_Number").hide();
//        $("#val_Vendor_Email_Address").hide();
//        $("#val_Vendor_Secondary_Email_Address").hide();
//        $("#val_Vendor_Fax_Number").hide();
//        $("#val_Vendor_Tax_Number").hide();
//        $("#val_Vendor_Contact_Person").hide();
//        $("#val_Vendor_State").hide();
//        $("#val_Vendor_Website").hide();
//        $("#val_Vendor_Code").hide();
//    })

//})




//function Get_Vendors(vendor_Id) {
//    debugger;

//    $.ajax(
//        {
//            type: "get",
//            url: "Vendor/Get_Vendors/",
//            dataType: "json",
//            data: { vendor_Id: vendor_Id },
//            cache: false,
//            success: function (response) {
//                if (response.vendor_Id != 0) {
//                    $('#ddl_Vendors').val(response.vendor_Id);
//                    $('#txt_Vendor_Name').val(response.Vendor_Name);
//                    $('#txt_Vendor_Address').val(response.Vendor_Address);
//                    $('#txt_Vendor_City').val(response.Vendor_City);
//                    $('#txt_Vendor_Country').val(response.Vendor_Country);
//                    $('#txt_Vendor_Zip').val(response.Vendor_Zip);
//                    $('#txt_Vendor_Phone_Number').val(response.Vendor_Phone_Number);
//                    $('#txt_Vendor_Email_Address').val(response.Vendor_Email_Address);
//                    $('#txt_Vendor_Secondary_Email_Address').val(response.Vendor_Secondary_Email_Address);
//                    $('#txt_Vendor_Fax_Number').val(response.Vendor_Fax_Number);
//                    $('#txt_Vendor_Tax_Number').val(response.Vendor_Tax_Number);
//                    $('#txt_Vendor_Contact_Person').val(response.Vendor_Contact_Person);
//                    $('#txt_Vendor_State').val(response.Vendor_State);
//                    $('#txt_Vendor_Website').val(response.Vendor_Website);
//                    $('#txt_Vendor_Code').val(response.Vendor_Code);
//                }
//            }
//        })
//}