﻿
$(document).ready(function () {
    $('body').on('click', "#tbl_VendorList a.Vendor_Id", function () {
        var vendor_Id = $(this).attr('id');
        if (vendor_Id > 0) {
            confirmDeleteConfirmation(vendor_Id);
        }
    });
});

//confirmation box to delete test case
function confirmDeleteConfirmation(vendor_Id) {
    if (confirm('Do you really want to delete details?')) {
        // alert('Thanks for confirming');
        var user_Confirmation_To_Delete = false;
        Delete_VendorConfirmation(vendor_Id, user_Confirmation_To_Delete);
    }
    //else {
    //    alert('Why did you press cancel? You should have confirmed');
    //}
    //if (confirm("Do you want to delete vendor fields mapping !"))
    //{
    //    txt = "You pressed OK!";

    //    //var user_Confirmation_To_Delete = false;
    //    //Delete_MappingConfirmation(vendor_Id, user_Confirmation_To_Delete)

    //}
    //else {
    //    txt = "You pressed Cancel!";
    //}
}


//ssi_modal.confirm({
//    content: 'Do you want to delete vendor fields mapping ?',
//    okBtn: {
//        className: 'btn btn-primary'
//    },
//    cancelBtn: {
//        className: 'btn btn-danger'
//    }
//}, function (result) {
//    if (result) {
//        var user_Confirmation_To_Delete = false;
//        Delete_MappingConfirmation(vendor_Id, user_Confirmation_To_Delete)
//     }

// });

//};

//delete
function Delete_VendorConfirmation(vendor_Id, User_Confirmation_To_Delete) {
    $.ajax({
        type: "GET",
        url: '/Vendor/Delete',
        async: false,
        data: { vendor_Id: vendor_Id },//, User_Confirmation_To_Delete: User_Confirmation_To_Delete },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (response) {
            if (response != null) {
                if (response.return_Message != undefined && response.return_Message != null) {
                    if (response.return_Message_Id == Message_Ids.Delete) {
                        alert(response.return_Message);
                        location.href = "/Vendor/List";
                    }
                    else if (response.return_Message_Id == Message_Ids.Error) {
                        //  ssi_modal.notify('error', { position: 'right top', content: response.return_Message });
                        alert(response.return_Message);
                    }
                    else if (response.return_Message_Id == Message_Ids.Other) {
                        //  confirmDelete(testCase_id, response.return_Message)
                        alert(response.return_Message);
                    }
                    else {
                        // ssi_modal.notify('info', { position: 'right top', content: response.return_Message });
                        alert(response.return_Message);
                    }
                }
            }
            else {

                ssi_modal.notify('error', { position: 'right top', content: response.message })
            }
        },
        failure: function (response) {
            alert(response.responseText);
        },
        error: function (response) {
            alert(response.responseText);
        }
    });
}

