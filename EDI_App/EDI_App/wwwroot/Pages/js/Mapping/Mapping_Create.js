﻿$(document).ready(function () {

    $("#val_Vendor_Name").hide();
    $("#val_Customer_Id_Alias").hide();
    $("#val_Invoice_Id_Alias").hide();
    $("#val_Invoice_Date_Alias").hide();
    $("#val_Product_Id_Alias").hide();
    $("#val_Product_Name_Alias").hide();
    $("#val_Product_Name_Alias").hide();
    $("#val_Product_Description_Alias").hide();
    $("#val_Product_Unit_Alias").hide();
    $("#val_Product_Discount_Alias").hide();
    $("#val_Product_Amount_Alias").hide();
    $("#val_Invoice_Amount_Alias").hide();

    //validation for fields
    $("#ddl_Vendors").change(function () {
        if ($("#ddl_Vendors").selectedIndex = "0" || $("#ddl_Vendors").selectedIndex === undefined) {
            $('#txt_Vendor_Name').val('');
        }
        var vendor_id = $(this).val();
        $('#txt_Vendor_Name').val(vendor_id);
        if (vendor_id > 0) {
            $("#txt_Vendor_Name").val(vendor_id);
        }
        else {
            $("#txt_Vendor_Name").val(null);
        }
        if ($("#txt_Vendor_Name").val().length > 0) {
            $("#val_Vendor_Name").hide();
        }
        else {
            $("#val_Vendor_Name").show();
        }
    });

    //validation for fields
    $('#txt_Customer_Id_Alias').keyup(function (e) {
        if ($("#txt_Customer_Id_Alias").val().length > 0) {
            $("#val_Customer_Id_Alias").hide();
        }
        else {
            $("#val_Customer_Id_Alias").show();
        }
    });

    //validation for fields
    $('#txt_Invoice_Id_Alias').keyup(function (e) {
        if ($("#txt_Invoice_Id_Alias").val().length > 0) {
            $("#val_Invoice_Id_Alias").hide();
        }
        else {
            $("#val_Invoice_Id_Alias").show();
        }
    });

    //validation for fields
    $('#txt_Invoice_Date_Alias').keyup(function (e) {
        if ($("#txt_Invoice_Date_Alias").val().length > 0) {
            $("#val_Invoice_Date_Alias").hide();
        }
        else {
            $("#val_Invoice_Date_Alias").show();
        }
    });

    //validation for fields
    $('#txt_Product_Id_Alias').keyup(function (e) {
        if ($("#txt_Product_Id_Alias").val().length > 0) {
            $("#val_Product_Id_Alias").hide();
        }
        else {
            $("#val_Product_Id_Alias").show();
        }
    });

    //validation for fields
    $('#txt_Product_Name_Alias').keyup(function (e) {
        if ($("#txt_Product_Name_Alias").val().length > 0) {
            $("#val_Product_Name_Alias").hide();
        }
        else {
            $("#val_Product_Name_Alias").show();
        }
    });

    //validation for fields
    $('#txt_Product_Description_Alias').keyup(function (e) {
        if ($("#txt_Product_Description_Alias").val().length > 0) {
            $("#val_Product_Description_Alias").hide();
        }
        else {
            $("#val_Product_Description_Alias").show();
        }
    });

    //validation for fields
    $('#txt_Product_Unit_Alias').keyup(function (e) {
        if ($("#txt_Product_Unit_Alias").val().length > 0) {
            $("#val_Product_Unit_Alias").hide();
        }
        else {
            $("#val_Product_Unit_Alias").show();
        }
    });

    //validation for fields
    $('#txt_Product_Discount_Alias').keyup(function (e) {
        if ($("#txt_Product_Discount_Alias").val().length > 0) {
            $("#val_Product_Discount_Alias").hide();
        }
        else {
            $("#val_Product_Discount_Alias").show();
        }
    });

    //validation for fields
    $('#txt_Product_Amount_Alias').keyup(function (e) {
        if ($("#txt_Product_Amount_Alias").val().length > 0) {
            $("#val_Product_Amount_Alias").hide();
        }
        else {
            $("#val_Product_Amount_Alias").show();
        }
    });

    //validation for fields
    $('#txt_Invoice_Amount_Alias').keyup(function (e) {
        if ($("#txt_Invoice_Amount_Alias").val().length > 0) {
            $("#val_Invoice_Amount_Alias").hide();
        }
        else {
            $("#val_Invoice_Amount_Alias").show();
        }
    });

    $('#btn_Submit').click(function () {
        var valError_status = false;

        if ($('#txt_Customer_Id_Alias').val() == '' || $('#txt_Customer_Id_Alias').val() <= 0) {
            $("#val_Customer_Id_Alias").show();
            // $('#val_Customer_Id_Alias').css('visibility', 'visible');
            valError_status = true;
        }

        if ($('#txt_Customer_Id_Alias').val() == '' || $('#txt_Customer_Id_Alias').val() <= 0) {
            $("#val_Customer_Id_Alias").show();
            // $('#val_Customer_Id_Alias').css('visibility', 'visible');
            valError_status = true;
        }
        if ($('#txt_Invoice_Id_Alias').val() == '' || $('#txt_Invoice_Id_Alias').val() <= 0) {
            $("#val_Invoice_Id_Alias").show();
            valError_status = true;
        }
        if ($('#txt_Invoice_Date_Alias').val() == '' || $('#txt_Invoice_Date_Alias').val() <= 0) {
            $("#val_Invoice_Date_Alias").show();
            valError_status = true;
        }

        if ($('#txt_Product_Id_Alias').val() == '' || $('#txt_Product_Id_Alias').val() <= 0) {
            $("#val_Product_Id_Alias").show();
            valError_status = true;
        }

        if ($('#txt_Product_Name_Alias').val() == '' || $('#txt_Product_Name_Alias').val() <= 0) {
            $("#val_Product_Name_Alias").show();
            valError_status = true;
        }

        if ($('#txt_Product_Description_Alias').val() == '' || $('#txt_Product_Description_Alias').val() <= 0) {
            $("#val_Product_Description_Alias").show();
            valError_status = true;
        }

        if ($('#txt_Product_Unit_Alias').val() == '' || $('#txt_Product_Unit_Alias').val() <= 0) {
            $("#val_Product_Unit_Alias").show();
            valError_status = true;
        }

        if ($('#txt_Product_Discount_Alias').val() == '' || $('#txt_Product_Discount_Alias').val() <= 0) {
            $("#val_Product_Discount_Alias").show();
            valError_status = true;
        }

        if ($('#txt_Product_Amount_Alias').val() == '' || $('#txt_Product_Amount_Alias').val() <= 0) {
            $("#val_Product_Amount_Alias").show();
            valError_status = true;
        }

        if ($('#txt_Invoice_Amount_Alias').val() == '' || $('#txt_Invoice_Amount_Alias').val() <= 0) {
            $("#val_Invoice_Amount_Alias").show();
            valError_status = true;
        }

        if (valError_status == false) {
            $.ajax({
                type: "post",
                url: "/Mapping/Create",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (response) {
                    if (response != null) {
                        alert(data);
                        location.href = '/Mapping/List/';
                    } else {

                    }
                },
                failure: function (response) {

                },
                error: function (response) {

                }
            });
        }
    })

    $('#btn_Clear').click(function () {
        $("#val_Vendor_Name").hide();
        $("#val_Customer_Id_Alias").hide();
        $("#val_Invoice_Id_Alias").hide();
        $("#val_Invoice_Date_Alias").hide();
        $("#val_Product_Id_Alias").hide();
        $("#val_Product_Name_Alias").hide();
        $("#val_Product_Name_Alias").hide();
        $("#val_Product_Description_Alias").hide();
        $("#val_Product_Unit_Alias").hide();
        $("#val_Product_Discount_Alias").hide();
        $("#val_Product_Amount_Alias").hide();
        $("#val_Invoice_Amount_Alias").hide();
    })

})


function Get_VendorMappingFields(vendor_Id) {
    debugger;

    $.ajax(
        {
            type: "get",
            url: "Mapping/Get_VendorMappingFields/",
            dataType: "json",
            data: { vendor_Id: vendor_Id },
            cache: false,
            success: function (response) {
                if (response.vendor_Id != 0) {
                    $('#ddl_Vendors').val(response.vendor_Id);
                    $('#txt_Vendor_Name').val(response.Vendor_Name);
                    $('#txt_Customer_Id_Alias').val(response.Customer_Id_Alias);
                    $('#txt_Invoice_Id_Alias').val(response.Invoice_Id_Alias);
                    $('#txt_Invoice_Date_Alias').val(response.Invoice_Date_Alias);
                    $('#txt_Product_Id_Alias').val(response.Product_Id_Alias);
                    $('#txt_Product_Name_Alias').val(response.Product_Name_Alias);
                    $('#txt_Product_Description_Alias').val(response.Product_Description_Alias);
                    $('#txt_Product_Unit_Alias').val(response.Product_Unit_Alias);
                    $('#txt_Product_Discount_Alias').val(response.Product_Discount_Alias);
                    $('#txt_Product_Amount_Alias').val(response.Product_Amount_Alias);
                    $('#txt_Invoice_Amount_Alias').val(response.Invoice_Amount_Alias);
                }
            }
        })
}