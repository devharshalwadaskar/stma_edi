﻿
    $(document).ready(function () {
        $('body').on('click', "#tbl_MappingList a.Vendor_Id", function () {
            var vendor_Id = $(this).attr('id');
            if (vendor_Id > 0) {
                confirmDeleteConfirmation(vendor_Id);
            }
        });
});

//confirmation box to delete test case
function confirmDeleteConfirmation(vendor_Id) {
    if (confirm('Do you really want to delete details?')) {
        var user_Confirmation_To_Delete = false;
        Delete_MappingConfirmation(vendor_Id, user_Confirmation_To_Delete);
    }
}



//delete
        function Delete_MappingConfirmation(vendor_Id) {
        $.ajax({
            type: "GET",
            url: '/Mapping/Delete',
            async: false,
            data: { vendor_Id: vendor_Id },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function (response) {
                if (response != null) {
                    if (response.return_Message != undefined && response.return_Message != null) {
                        if (response.return_Message_Id == Message_Ids.Delete) {
                            alert(response.return_Message);
                            location.href = "/Mapping/List";
                        }
                        else if (response.return_Message_Id == Message_Ids.Error) {
                            alert(response.return_Message);
                            location.href = "/Mapping/List";
                        }
                        else if (response.return_Message_Id == Message_Ids.Other) {
                            alert(response.return_Message);
                            location.href = "/Mapping/List";
                        }
                        else {
                            alert(response.return_Message);
                            location.href = "/Mapping/List";
                        }
                    }
                }
                else {

                    alert(response.responseText);
                }
            },
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });
}

