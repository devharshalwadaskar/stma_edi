﻿const Message_Ids =
{
    Error: 0,
    Insert: 1,
    Update: 2,
    Delete: 3,
    Other: 4,
    Exists: 5
};

//Global Variables for messages used in Testing Center Page
{
    //var MSG_FOLLW_TESTING_SEQUENCE = 'Please follow testing sequentially.'; //Used in Tesing Center(Execution Page)
    //var MSG_TESTSCRIPT_STATUS_CHECKING = 'Please update the status to Pass/Fail.'; //Used in Tesing Center(Execution Page)
}
