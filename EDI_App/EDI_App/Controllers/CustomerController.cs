﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EDI_App.Helper;
using EDI_App.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace EDI_App.Controllers
{
    public class CustomerController : Controller
    {
        [HttpGet]
        public IActionResult List()
        {
            List<Customer_M> lst_Customers = new List<Customer_M>();
            Customer_Helper customer_Helper = new Customer_Helper();
            lst_Customers = customer_Helper.GetAll_Customers();
            return View(lst_Customers);
        }

        [HttpGet]
        public IActionResult Create()
        {
            try
            {
                List<SelectListItem> Customer_TypeListSelectListItem = new List<SelectListItem>();
                List<SelectListItem> State_ListSelectListItem = new List<SelectListItem>();

                Customer_Helper customer_Helper = new Customer_Helper();

                var customer_Ddl = customer_Helper.GetAll_Customer_Types();
                if (customer_Ddl != null)
                {
                    Customer_TypeListSelectListItem = (from p in customer_Ddl.AsEnumerable()
                                                       orderby p.Customer_Type_Name ascending
                                                       select new SelectListItem
                                                       {
                                                           Text = p.Customer_Type_Name.ToString(),
                                                           Value = p.Customer_Type_Id.ToString()//,
                                                       }).ToList();

                }
                customer_Helper = new Customer_Helper();
                var state_Ddl = customer_Helper.GetAll_States();
                if (state_Ddl != null)
                {
                    State_ListSelectListItem = (from p in state_Ddl.AsEnumerable()
                                                       orderby p.State ascending
                                                       select new SelectListItem
                                                       {
                                                           Text = p.State.ToString(),
                                                           Value = p.State_Id.ToString()//,
                                                       }).ToList();

                } 
                ViewBag.Customers_Type = Customer_TypeListSelectListItem;
                ViewBag.States = State_ListSelectListItem;
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(include: "Customer_Name,Customer_Code,Address,City,State,State_Id,Country,Email_Address,Secondary_Email_Address,Phone_Number,Fax_Number,Tax_Number,Zip,Status,Customer_Type_Id")]Customer_M customer)
        {
            Operation_Message msg = new Operation_Message();
            Customer_Helper customer_Helper = new Customer_Helper();

            try
            {
                if (ModelState.IsValid)
                {
                    customer.Mode = 'I';
                    msg = customer_Helper.InsertUpdate_Customer(customer);
                }

                //Bind Customer Type List
                {
                    List<SelectListItem> Customer_TypeListSelectListItem = new List<SelectListItem>();
                    List<SelectListItem> State_ListSelectListItem = new List<SelectListItem>();

                    customer_Helper = new Customer_Helper();

                    var customer_Ddl = customer_Helper.GetAll_Customers();
                    if (customer_Ddl != null)
                    {
                        Customer_TypeListSelectListItem = (from p in customer_Ddl.AsEnumerable()
                                                           orderby p.Customer_Name ascending
                                                           select new SelectListItem
                                                           {
                                                               Text = p.Customer_Name.ToString(),
                                                               Value = p.Customer_Type_Id.ToString(),
                                                               Selected = false
                                                           }).ToList();

                    }
                    customer_Helper = new Customer_Helper();
                    var state_Ddl = customer_Helper.GetAll_States();
                    if (state_Ddl != null)
                    {
                        State_ListSelectListItem = (from p in state_Ddl.AsEnumerable()
                                                    orderby p.State ascending
                                                    select new SelectListItem
                                                    {
                                                        Text = p.State.ToString(),
                                                        Value = p.State_Id.ToString()//,
                                                    }).ToList();

                    }

                    ViewBag.Customers_Type = Customer_TypeListSelectListItem;
                    ViewBag.States = State_ListSelectListItem;
                }

                if (msg.Is_success)
                {
                    ViewBag.Message_Id = msg.Return_Message_Id;
                    ViewBag.Message = msg.Return_Message;

                    List<Customer_M> lst_Customers = new List<Customer_M>();
                    customer_Helper = new Customer_Helper();
                    lst_Customers = customer_Helper.GetAll_Customers();

                    return View("List", lst_Customers);
                }
                else
                {
                    ViewBag.Message_Id = msg.Return_Message_Id;
                    ViewBag.Message = msg.Return_Message;
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View();
            }
        }

        [HttpGet]
        public IActionResult Edit(long customer_Id)
        {
            List<SelectListItem> Customer_TypeListSelectListItem = new List<SelectListItem>();
            List<SelectListItem> State_ListSelectListItem = new List<SelectListItem>();

            Customer_M customer = new Customer_M();
            Customer_Type customer_Type = new Customer_Type();
            Customer_Helper customer_Helper = new Customer_Helper();
            try
            {
                customer = customer_Helper.Get_Customer(customer_Id);
                customer_Helper = new Customer_Helper();

                var customer_Type_Ddl = customer_Helper.GetAll_Customer_Types();
                if (customer_Type_Ddl != null)
                {
                    Customer_TypeListSelectListItem = (from p in customer_Type_Ddl.AsEnumerable()
                                                       orderby p.Customer_Type_Name ascending
                                                       select new SelectListItem
                                                       {
                                                           Text = p.Customer_Type_Name.ToString(),
                                                           Value = p.Customer_Type_Id.ToString(),
                                                           Selected = (long)p.Customer_Type_Id == customer.Customer_Type_Id ? true : false
                                                       }).ToList();

                }
                customer_Helper = new Customer_Helper();
                var state_Ddl = customer_Helper.GetAll_States();
                if (state_Ddl != null)
                {
                    State_ListSelectListItem = (from p in state_Ddl.AsEnumerable()
                                                orderby p.State ascending
                                                select new SelectListItem
                                                {
                                                    Text = p.State.ToString(),
                                                    Value = p.State_Id.ToString(),
                                                    Selected = (long)p.State_Id == customer.State_Id ? true : false
                                                }).ToList();

                }

                ViewBag.Customers_Type = Customer_TypeListSelectListItem;
                ViewBag.States = State_ListSelectListItem;
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            return View(customer);
        }

        [HttpPost]
        public ActionResult Edit([Bind(include: "Customer_Id,Customer_Name,Customer_Code,Address,City,State,,State_Id,Country,Email_Address,Secondary_Email_Address,Phone_Number,Fax_Number,Tax_Number,Zip,Status,Customer_Type_Id")]Customer_M customer)
        {
            Operation_Message msg = new Operation_Message();
            Customer_Helper customer_Helper = new Customer_Helper();

            try
            {
                if (ModelState.IsValid)
                {
                    customer.Mode = 'U';
                    msg = customer_Helper.InsertUpdate_Customer(customer);
                }

                //Bind Dropdown Lists
                {
                    List<SelectListItem> Customer_TypeListSelectListItem = new List<SelectListItem>();
                    List<SelectListItem> State_ListSelectListItem = new List<SelectListItem>();


                    customer_Helper = new Customer_Helper();
                    var customer_Ddl = customer_Helper.GetAll_Customers();
                    if (customer_Ddl != null)
                    {
                        Customer_TypeListSelectListItem = (from p in customer_Ddl.AsEnumerable()
                                                           orderby p.Customer_Name ascending
                                                           select new SelectListItem
                                                           {
                                                               Text = p.Customer_Name.ToString(),
                                                               Value = p.Customer_Type_Id.ToString(),
                                                               Selected = false
                                                           }).ToList();

                    }
                    customer_Helper = new Customer_Helper();
                    var state_Ddl = customer_Helper.GetAll_States();
                    if (state_Ddl != null)
                    {
                        State_ListSelectListItem = (from p in state_Ddl.AsEnumerable()
                                                    orderby p.State ascending
                                                    select new SelectListItem
                                                    {
                                                        Text = p.State.ToString(),
                                                        Value = p.State_Id.ToString()//,
                                                    }).ToList();

                    }

                    ViewBag.Customers_Type = Customer_TypeListSelectListItem;
                    ViewBag.States = State_ListSelectListItem;
                }

                if (msg.Is_success)
                {
                    List<Customer_M> lst_Customer = new List<Customer_M>();
                    customer_Helper = new Customer_Helper();
                    lst_Customer = customer_Helper.GetAll_Customers();

                    ViewBag.Message_Id = msg.Return_Message_Id;
                    ViewBag.Message = msg.Return_Message;


                    return View("List", lst_Customer);
                }
                else
                {
                    ViewBag.Message_Id = msg.Return_Message_Id;
                    ViewBag.Message = msg.Return_Message;
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View();
            }
        }

        [HttpGet]
        public JsonResult Delete(long customer_Id)
        {
            Operation_Message operation_Message = new Operation_Message();
            try
            {
                Customer_Helper customer_Helper = new Customer_Helper();
                if (customer_Id > 0)
                {
                    operation_Message = customer_Helper.Delete_Customer(customer_Id);
                    if (operation_Message.Is_success)
                    {
                        if (operation_Message.Return_Message_Id == (int)Messages_Ids.Delete)
                        {
                            ViewBag.Message_Id = (int)Messages_Ids.Delete;
                            operation_Message.Return_Message_Id = (int)Messages_Ids.Delete;

                            ViewBag.Message = operation_Message.Return_Message;
                            operation_Message.Return_Message = operation_Message.Return_Message;
                        }
                        else if (operation_Message.Return_Message_Id == (int)Messages_Ids.Other)
                        {
                            ViewBag.Message_Id = (int)Messages_Ids.Other;
                            operation_Message.Return_Message_Id = (int)Messages_Ids.Other;

                            ViewBag.Message = operation_Message.Return_Message;
                            operation_Message.Return_Message = operation_Message.Return_Message;
                        }
                        else
                        {
                            ViewBag.Message_Id = (int)Messages_Ids.Error;
                            operation_Message.Return_Message_Id = (int)Messages_Ids.Error;

                            ViewBag.Message = operation_Message.Return_Message;
                            operation_Message.Return_Message = operation_Message.Return_Message;
                        }
                    }
                    else
                    {
                        ViewBag.Message_Id = (int)Messages_Ids.Error;
                        ViewBag.Message = "Error";
                    }
                }
                else
                {
                    ViewBag.Message_Id = (int)Messages_Ids.Error;
                    ViewBag.Message = "Error";
                }
            }
            catch (Exception _ex)
            {
                ViewBag.Message_Id = (int)Messages_Ids.Error;
                ViewBag.Message = "Error";
            }
            return Json(operation_Message);
        }

        [HttpGet]
        public JsonResult GetAll_Customer_Type_Json()
        {
            List<SelectListItem> customer_ListSelectListItem = new List<SelectListItem>();
            try
            {
                Customer_Helper customer_Helper = new Customer_Helper();
                var customer_Ddl = customer_Helper.GetAll_Customer_Types();

                if (customer_Ddl != null)
                {
                    customer_ListSelectListItem = (from p in customer_Ddl.AsEnumerable()
                                                select new SelectListItem
                                                {
                                                    Text = p.Customer_Type_Name.ToString(),
                                                    Value = p.Customer_Type_Id.ToString()//,
                                                }).ToList();
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            var obj = new
            {
                response = new
                {
                    lst = customer_ListSelectListItem,
                    message = ViewBag.Message,
                    message_Id = ViewBag.Message_Id
                }
            };
            return Json(obj);
        }
        [HttpGet]
        public JsonResult GetAll_States_Json()
        {
            List<States> state_Ddl = new List<States>();
            try
            {
                Customer_Helper customer_Helper = new Customer_Helper();
                state_Ddl = customer_Helper.GetAll_States();
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            var obj = new
            {
                response = new
                {
                    lst = state_Ddl,
                    message = ViewBag.Message,
                    message_Id = ViewBag.Message_Id
                }
            };
            return Json(obj);
        }
        [HttpGet]
        public JsonResult Get_Customer_Json(long customer_Id) 
        {
            Customer_M customer = new Customer_M();
            try
            {
                Customer_Helper customer_Helper = new Customer_Helper();
                customer = customer_Helper.Get_Customer(customer_Id);
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            //var obj = new
            //{
            //    response = new
            //    {
            //        lst = customer,
            //        message = ViewBag.Message,
            //        message_Id = ViewBag.Message_Id
            //    }
            //};
            return Json(customer);
        }

    }
}