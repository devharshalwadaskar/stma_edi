﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EDI_App.Helper;
using EDI_App.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace EDI_App.Controllers
{
    public class MappingController : Controller
    {
        [HttpGet]
        public IActionResult List()
        {
            List<Mapping_M> lst_Mapping = new List<Mapping_M>();
            Mapping_Helper mapping_Helper = new Mapping_Helper();
            lst_Mapping = mapping_Helper.GetAll_VendorMappingFields();
            return View(lst_Mapping);
        }

        [HttpGet]
        public IActionResult Create()
        {
            try
            {
                List<SelectListItem> vendorListSelectListItem = new List<SelectListItem>();

                Mapping_Helper mapping_Helper = new Mapping_Helper();
                var vendor_Ddl = mapping_Helper.GetAll_Vendors();
                if (vendor_Ddl != null)
                {
                    vendorListSelectListItem = (from p in vendor_Ddl.AsEnumerable()
                                                orderby p.Vendor_Name ascending
                                                select new SelectListItem
                                                {
                                                    Text = p.Vendor_Name.ToString(),
                                                    Value = p.Vendor_Id.ToString()//,
                                                }).ToList();

                }

                ViewBag.Vendors = vendorListSelectListItem;
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(include: "Vendor_Id,Invoice_Id_Alias,Invoice_Date_Alias,Invoice_Amount_Alias,Customer_Id_Alias,Customer_Name_Alias,Product_Id_Alias,Product_Name_Alias,Product_Quantity_Alias,Product_Unit_Alias,Product_Price_Alias,Product_Description_Alias,Product_Discount_Alias,Product_Amount_Alias,Invoice_Amount_Alias")]Mapping_M mapping)
        {
            Operation_Message msg = new Operation_Message();
            Mapping_Helper mapping_Helper = new Mapping_Helper();

            try
            {
                if (ModelState.IsValid)
                {
                    mapping.Mode = 'I';
                    msg = mapping_Helper.Mapping_InsertUpdate(mapping);
                }

                //Bind Vendor List
                {
                    List<SelectListItem> vendorListSelectListItem = new List<SelectListItem>();
                    mapping_Helper = new Mapping_Helper();

                    var vendor_Ddl = mapping_Helper.GetAll_Vendors();
                    if (vendor_Ddl != null)
                    {
                        vendorListSelectListItem = (from p in vendor_Ddl.AsEnumerable()
                                                    orderby p.Vendor_Name ascending
                                                    select new SelectListItem
                                                    {
                                                        Text = p.Vendor_Name.ToString(),
                                                        Value = p.Vendor_Id.ToString(),
                                                        Selected = false
                                                    }).ToList();

                    }
                    ViewBag.Vendors = vendorListSelectListItem;
                }

                if (msg.Is_success)
                {
                    ViewBag.Message_Id = msg.Return_Message_Id;
                    ViewBag.Message = msg.Return_Message;

                    List<Mapping_M> lst_mapping = new List<Mapping_M>();
                    mapping_Helper = new Mapping_Helper();
                    lst_mapping = mapping_Helper.GetAll_VendorMappingFields();
                    return View("List", lst_mapping);
                    //if (msg.Return_Message_Id == (int)Messages_Ids.Insert)
                    //{
                    //    ViewBag.Message_Id = msg.Return_Message_Id;
                    //    ViewBag.Message = msg.Return_Message;
                    //    return View("List", lst_mapping);
                    //}

                    //else
                    //{
                    //    ViewBag.Message_Id = msg.Return_Message_Id;
                    //    ViewBag.Message = msg.Return_Message;
                    //    return View("Create", mapping);
                    //}
                }
                else
                {
                    ViewBag.Message_Id = msg.Return_Message_Id;
                    ViewBag.Message = msg.Return_Message;
                    return View("Create", mapping);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View("Create", mapping);
            }
        }

        [HttpGet]
        public IActionResult Edit(long vendor_Id)
        {
            List<SelectListItem> vendorListSelectListItem = new List<SelectListItem>();
            Mapping_M mapping = new Mapping_M();
            Mapping_Helper mapping_Helper = new Mapping_Helper();
            try
            {
                var vendor_Ddl = mapping_Helper.GetAll_Vendors();
                if (vendor_Ddl != null)
                {
                    vendorListSelectListItem = (from p in vendor_Ddl.AsEnumerable()
                                                orderby p.Vendor_Name ascending
                                                select new SelectListItem
                                                {
                                                    Text = p.Vendor_Name.ToString(),
                                                    Value = p.Vendor_Id.ToString(),
                                                    Selected = (long)p.Vendor_Id == vendor_Id ? true : false
                                                }).ToList();

                }
                mapping_Helper = new Mapping_Helper();
                mapping = mapping_Helper.Get_VendorMappingFields(vendor_Id);
                ViewBag.Vendors = vendorListSelectListItem;
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            return View(mapping);
        }

        [HttpPost]
        public ActionResult Edit([Bind(include: "Vendor_Id,Invoice_Id_Alias,Invoice_Date_Alias,Invoice_Amount_Alias,Customer_Id_Alias,Mapping_Id,Customer_Name_Alias,Product_Id_Alias,Product_Name_Alias,Product_Quantity_Alias,Product_Unit_Alias,Product_Price_Alias,Product_Description_Alias,Product_Discount_Alias,Product_Amount_Alias,Invoice_Amount_Alias")]Mapping_M mapping)
        {
            Operation_Message msg = new Operation_Message();
            Mapping_Helper mapping_Helper = new Mapping_Helper();

            try
            {
                if (ModelState.IsValid)
                {
                    mapping.Mode = 'U';
                    msg = mapping_Helper.Mapping_InsertUpdate(mapping);
                }

                //Bind Vendor List
                {
                    List<SelectListItem> vendorListSelectListItem = new List<SelectListItem>();

                    mapping_Helper = new Mapping_Helper();
                    var vendor_Ddl = mapping_Helper.GetAll_Vendors();
                    if (vendor_Ddl != null)
                    {
                        vendorListSelectListItem = (from p in vendor_Ddl.AsEnumerable()
                                                    orderby p.Vendor_Name ascending
                                                    select new SelectListItem
                                                    {
                                                        Text = p.Vendor_Name.ToString(),
                                                        Value = p.Vendor_Id.ToString(),
                                                        Selected = false
                                                    }).ToList();

                    }
                    ViewBag.Vendors = vendorListSelectListItem;
                }

                if (msg.Is_success)
                {
                    List<Mapping_M> lst_mapping = new List<Mapping_M>();
                    mapping_Helper = new Mapping_Helper();
                    lst_mapping = mapping_Helper.GetAll_VendorMappingFields();

                    ViewBag.Message_Id = msg.Return_Message_Id;
                    ViewBag.Message = msg.Return_Message;
                    return View("List", lst_mapping);
                    //if (msg.Return_Message_Id == (int)Messages_Ids.Update)
                    //{
                    //    return View("List", lst_mapping);
                    //}

                    //else
                    //{
                    //    return View("Create", mapping);
                    //}
                }
                else
                {
                    ViewBag.Message_Id = msg.Return_Message_Id;
                    ViewBag.Message = msg.Return_Message;
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View();
            }
        }


        [HttpGet]
        public JsonResult Get_VendorMappingFields(long vendor_Id)
        {
            Mapping_M mapping = new Mapping_M();
            Mapping_Helper mapping_Helper = new Mapping_Helper();

            try
            {
                if (vendor_Id != 0)
                {
                    mapping = mapping_Helper.Get_VendorMappingFields(vendor_Id);
                }

            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            return (Json(mapping));
        }

        [HttpGet]
        public JsonResult Delete(long vendor_Id)//, bool user_Confirmation_To_Delete_Test_Script)
        {
            Operation_Message operation_Message = new Operation_Message();
            try
            {
                Mapping_Helper mapping_Helper = new Mapping_Helper();
                if (vendor_Id > 0)
                {
                    operation_Message = mapping_Helper.Delete_VendorMappingFields(vendor_Id);
                    if (operation_Message.Is_success)
                    {
                        if (operation_Message.Return_Message_Id == (int)Messages_Ids.Delete)
                        {
                            ViewBag.Message_Id = (int)Messages_Ids.Delete;
                            operation_Message.Return_Message_Id = (int)Messages_Ids.Delete;

                            ViewBag.Message = operation_Message.Return_Message;
                            operation_Message.Return_Message = operation_Message.Return_Message;
                        }
                        else if (operation_Message.Return_Message_Id == (int)Messages_Ids.Other)
                        {
                            ViewBag.Message_Id = (int)Messages_Ids.Other;
                            operation_Message.Return_Message_Id = (int)Messages_Ids.Other;

                            ViewBag.Message = operation_Message.Return_Message;
                            operation_Message.Return_Message = operation_Message.Return_Message;
                        }
                        else
                        {
                            ViewBag.Message_Id = (int)Messages_Ids.Error;
                            operation_Message.Return_Message_Id = (int)Messages_Ids.Error;

                            ViewBag.Message = operation_Message.Return_Message;
                            operation_Message.Return_Message = operation_Message.Return_Message;
                        }
                    }
                    else
                    {
                        ViewBag.Message_Id = (int)Messages_Ids.Error;
                        ViewBag.Message = "Error";
                    }
                }
                else
                {
                    ViewBag.Message_Id = (int)Messages_Ids.Error;
                    ViewBag.Message = "Error";
                }
            }
            catch (Exception _ex)
            {
                ViewBag.Message_Id = (int)Messages_Ids.Error;
                ViewBag.Message = "Error";
            }
            return Json(operation_Message);
        }

        [HttpGet]
        public JsonResult GetAll_Vendors_Json()
        {

            List<SelectListItem> vendorListSelectListItem = new List<SelectListItem>();
            try
            {
                Mapping_Helper mapping_Helper = new Mapping_Helper();

                var vendor_Ddl = mapping_Helper.GetAll_Vendors();

                if (vendor_Ddl != null)
                {
                    vendorListSelectListItem = (from p in vendor_Ddl.AsEnumerable()
                                                select new SelectListItem
                                                {
                                                    Text = p.Vendor_Name.ToString(),
                                                    Value = p.Vendor_Id.ToString()//,
                                                }).ToList();
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            var obj = new
            {
                response = new
                {
                    lst = vendorListSelectListItem,
                    message = ViewBag.Message,
                    message_Id = ViewBag.Message_Id
                }
            };
            return Json(obj);
        }
    }
}