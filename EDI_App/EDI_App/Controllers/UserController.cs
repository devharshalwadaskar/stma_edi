﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using EDI_App.Helper;
using EDI_App.Models;
using Microsoft.AspNetCore.Mvc;

namespace EDI_App.Controllers
{
    public class UserController : Controller
    {
        public IActionResult List()
        {
            List<User_M> lst_users = new List<User_M>();
            User_Helper user_Helper = new User_Helper();
            lst_users = user_Helper.GetAll_Users();
            return View(lst_users);
        }

        [HttpGet]
        public IActionResult Create()
        {
            try
            {
                List<Role> lst_roles = new List<Role>();
                User_Helper user_Helper = new User_Helper();
                lst_roles = user_Helper.GetAll_Roles();
                User_M user = new User_M();
                var model = new User_M
                {
                    Role_List = lst_roles
                };

                return View(model);
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult Create([Bind(include: "First_Name,Last_Name,Email_Address,Secondary_Email_Address,Phone_Number,Address,Other_Info,Password,Role_Id")]User_M user)
        {
            User_Helper user_Helper = new User_Helper();
            try
            {
                Operation_Message msg = new Operation_Message();
                user.Mode = 'i';
                string password = Encrypt(user.Password);
                user.Password = password;
                msg = user_Helper.InsertUpdate_User(user);
                List<User_M> lst_users = new List<User_M>();
                user_Helper = new User_Helper();
                lst_users = user_Helper.GetAll_Users();

                if (msg.Is_success)
                {
                    ViewBag.Message_Id = msg.Return_Message_Id;
                    ViewBag.Message = msg.Return_Message;

                    return View("List", lst_users);
                }
                else
                {
                    ViewBag.Message_Id = msg.Return_Message_Id;
                    ViewBag.Message = msg.Return_Message;

                    return View(lst_users);
                }
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        [HttpGet]
        public IActionResult Edit(long user_Id)
        {
            List<Role> lst_roles = new List<Role>();
            User_Helper user_Helper = new User_Helper();
            lst_roles = user_Helper.GetAll_Roles();

            User_M user = new User_M();
            User_M model = new User_M();
            user_Helper = new User_Helper();

            user = user_Helper.Get_User(user_Id);
            model.Role_List = lst_roles;

            model.First_Name = user.First_Name;
            model.Last_Name = user.Last_Name;
            model.Phone_Number = user.Phone_Number;
            model.Address = user.Address;
            model.Email_Address = user.Email_Address;
            model.Secondary_Email_Address = user.Secondary_Email_Address;
            model.Password = user.Password;
            model.ConfirmPassword = user.Password;
            model.Other_Info = user.Other_Info;
            model.Role_Id = user.Role_Id;
            model.Role_Name = user.Role_Name;
           

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit([Bind(include: "User_Id,First_Name,Last_Name,Email_Address,Secondary_Email_Address,Phone_Number,Address,Other_Info,Password,Role_Id")]User_M user)
        {
            User_Helper user_Helper = new User_Helper();
            try
            {
                Operation_Message msg = new Operation_Message();
                user.Mode = 'u';
                string password = Encrypt(user.Password);
                user.Password = password;

                msg = user_Helper.InsertUpdate_User(user);

                List<User_M> lst_users = new List<User_M>();
                user_Helper = new User_Helper();
                lst_users = user_Helper.GetAll_Users();

                if (msg.Is_success)
                {
                    ViewBag.Message_Id = msg.Return_Message_Id;
                    ViewBag.Message = msg.Return_Message;

                    return View("List", lst_users);
                }
                else
                {
                    ViewBag.Message_Id = msg.Return_Message_Id;
                    ViewBag.Message = msg.Return_Message;

                    return View(lst_users);
                }
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        private string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        private string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        [HttpGet]
        public JsonResult Delete(long user_Id)
        {
            Operation_Message operation_Message = new Operation_Message();
            try
            {
                User_Helper user_Helper = new User_Helper();
                if (user_Id > 0)
                {
                    operation_Message = user_Helper.Delete_User(user_Id);
                    if (operation_Message.Is_success)
                    {
                        if (operation_Message.Return_Message_Id == (int)Messages_Ids.Delete)
                        {
                            ViewBag.Message_Id = (int)Messages_Ids.Delete;
                            operation_Message.Return_Message_Id = (int)Messages_Ids.Delete;

                            ViewBag.Message = operation_Message.Return_Message;
                            operation_Message.Return_Message = operation_Message.Return_Message;
                        }
                        else if (operation_Message.Return_Message_Id == (int)Messages_Ids.Other)
                        {
                            ViewBag.Message_Id = (int)Messages_Ids.Other;
                            operation_Message.Return_Message_Id = (int)Messages_Ids.Other;

                            ViewBag.Message = operation_Message.Return_Message;
                            operation_Message.Return_Message = operation_Message.Return_Message;
                        }
                        else
                        {
                            ViewBag.Message_Id = (int)Messages_Ids.Error;
                            operation_Message.Return_Message_Id = (int)Messages_Ids.Error;

                            ViewBag.Message = operation_Message.Return_Message;
                            operation_Message.Return_Message = operation_Message.Return_Message;
                        }
                    }
                    else
                    {
                        ViewBag.Message_Id = (int)Messages_Ids.Error;
                        ViewBag.Message = "Error";
                    }
                }
                else
                {
                    ViewBag.Message_Id = (int)Messages_Ids.Error;
                    ViewBag.Message = "Error";
                }
            }
            catch (Exception _ex)
            {
                ViewBag.Message_Id = (int)Messages_Ids.Error;
                ViewBag.Message = "Error";
            }
            return Json(operation_Message);
        }
    }
}