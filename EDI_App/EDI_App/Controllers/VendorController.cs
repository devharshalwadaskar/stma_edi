﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EDI_App.Helper;
using EDI_App.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace EDI_App.Controllers
{
    public class VendorController : Controller
    {
               
        [HttpGet]
        public IActionResult List()
        {
            List<Vendor_M> lst_mapping = new List<Vendor_M>();
            Vendor_Helper mapping_Helper = new Vendor_Helper();
            lst_mapping = mapping_Helper.Get_Vendors();
            return View(lst_mapping);
        }


        [HttpGet]
        public IActionResult Create()
        {
            try
            {
                List<SelectListItem> State_ListSelectListItem = new List<SelectListItem>();

                Customer_M customer = new Customer_M();
                Customer_Helper customer_Helper = new Customer_Helper();
                var state_Ddl = customer_Helper.GetAll_States();
                if (state_Ddl != null)
                {
                    State_ListSelectListItem = (from p in state_Ddl.AsEnumerable()
                                                orderby p.State ascending
                                                select new SelectListItem
                                                {
                                                    Text = p.State.ToString(),
                                                    Value = p.State_Id.ToString(),
                                                    Selected = p.State.ToUpper().Trim() == "TEXAS" ? true : false
                                                }).ToList();

                }
                ViewBag.States = State_ListSelectListItem;



                List<SelectListItem> Zip_ListSelectListItem = new List<SelectListItem>();
                Vendor_M vendor = new Vendor_M();
                Vendor_Helper vendor_Helper = new Vendor_Helper();
                var zips_Ddl = vendor_Helper.GetAll_Zip(vendor.State_Id, "");
                if (zips_Ddl != null)
                {
                    Zip_ListSelectListItem = (from p in zips_Ddl.AsEnumerable()
                                              orderby p.Zip_Code ascending
                                              select new SelectListItem
                                              {
                                                  Text = p.Zip_Code.ToString(),
                                                  Value = p.Zip_Code_Id.ToString()//,
                                              }).ToList();

                }
                ViewBag.Zip_Code = Zip_ListSelectListItem;


            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            return View();
        }


        // Insert_Vender
        [HttpPost]
        public ActionResult Create([Bind(include: "Vendor_Name,Address,City,Country,Zip_Code,Phone_Number,Email_Address,Secondary_Email_Address ,Fax_Number ,Tax_Number, Contact_Person ,State,State_Id ,Website, Vendor_Code")]Vendor_M vendor1)
        {
            Operation_Message msg = new Operation_Message();
            Vendor_Helper vendor_Helper = new Vendor_Helper();

            try
            {
                if (ModelState.IsValid)
                {
                    vendor1.Mode = 'I';
                    msg = vendor_Helper.Vendor_Insert(vendor1);
                }


                List<SelectListItem> State_ListSelectListItem = new List<SelectListItem>();

                Customer_M customer = new Customer_M();
                Customer_Helper customer_Helper = new Customer_Helper();
                var state_Ddl = customer_Helper.GetAll_States();
                if (state_Ddl != null)
                {
                    State_ListSelectListItem = (from p in state_Ddl.AsEnumerable()
                                                orderby p.State ascending
                                                select new SelectListItem
                                                {
                                                    Text = p.State.ToString(),
                                                    Value = p.State_Id.ToString(),
                                                    Selected = (long)p.State_Id == customer.State_Id ? true : false
                                                }).ToList();

                }
                ViewBag.States = State_ListSelectListItem;



                if (msg.Is_success)
                {
                    ViewBag.Message_Id = msg.Return_Message_Id;
                    ViewBag.Message = msg.Return_Message;

                    List<Vendor_M> lst_mapping = new List<Vendor_M>();
                    vendor_Helper = new Vendor_Helper();
                    lst_mapping = vendor_Helper.Get_Vendors();

                    return View("List", lst_mapping);
                }
                else
                {
                    ViewBag.Message_Id = msg.Return_Message_Id;
                    ViewBag.Message = msg.Return_Message;
                    List<Vendor_M> lst_mapping = new List<Vendor_M>();
                    vendor_Helper = new Vendor_Helper();
                    lst_mapping = vendor_Helper.Get_Vendors();

                    return View("List", lst_mapping);
                    // return View();



                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View();
            }
        }

        //delete vendor

        [HttpGet]
        public JsonResult Delete(long vendor_Id)//, bool user_Confirmation_To_Delete_Test_Script)
        {
            Operation_Message operation_Message = new Operation_Message();
            try
            {
                Vendor_Helper _testCase_Helper = new Vendor_Helper();
                if (vendor_Id > 0)
                {
                    operation_Message = _testCase_Helper.Delete_Vendor(vendor_Id);
                    if (operation_Message.Is_success)
                    {
                        if (operation_Message.Return_Message_Id == (int)Messages_Ids.Delete)
                        {
                            ViewBag.Message_Id = (int)Messages_Ids.Delete;
                            operation_Message.Return_Message_Id = (int)Messages_Ids.Delete;

                            ViewBag.Message = operation_Message.Return_Message;
                            operation_Message.Return_Message = operation_Message.Return_Message;
                        }
                        else if (operation_Message.Return_Message_Id == (int)Messages_Ids.Other)
                        {
                            ViewBag.Message_Id = (int)Messages_Ids.Other;
                            operation_Message.Return_Message_Id = (int)Messages_Ids.Other;

                            ViewBag.Message = operation_Message.Return_Message;
                            operation_Message.Return_Message = operation_Message.Return_Message;
                        }
                        else
                        {
                            ViewBag.Message_Id = (int)Messages_Ids.Error;
                            operation_Message.Return_Message_Id = (int)Messages_Ids.Error;

                            ViewBag.Message = operation_Message.Return_Message;
                            operation_Message.Return_Message = operation_Message.Return_Message;
                        }
                    }
                    else
                    {
                        ViewBag.Message_Id = (int)Messages_Ids.Error;
                        ViewBag.Message = "Error";
                    }
                }
                else
                {
                    ViewBag.Message_Id = (int)Messages_Ids.Error;
                    ViewBag.Message = "Error";
                }
            }
            catch (Exception _ex)
            {
                ViewBag.Message_Id = (int)Messages_Ids.Error;
                ViewBag.Message = "Error";
            }
            return Json(operation_Message);
        }

        //update Vendor

        [HttpGet]
        public IActionResult Edit(long vendor_Id)
        {

            List<Vendor_M> lst_vendor = new List<Vendor_M>();
            Vendor_M vendor = new Vendor_M();

            try
            {

                Vendor_Helper vendor_Helper = new Vendor_Helper();
                vendor = vendor_Helper.Get_VendorDetails(vendor_Id);
                List<SelectListItem> State_ListSelectListItem = new List<SelectListItem>();

                Customer_M customer = new Customer_M();
                Customer_Helper customer_Helper = new Customer_Helper();
                var state_Ddl = customer_Helper.GetAll_States();
                if (state_Ddl != null)
                {
                    State_ListSelectListItem = (from p in state_Ddl.AsEnumerable()
                                                orderby p.State ascending
                                                select new SelectListItem
                                                {
                                                    Text = p.State.ToString(),
                                                    Value = p.State_Id.ToString(),
                                                    Selected = (long)p.State_Id == customer.State_Id ? true : false
                                                }).ToList();

                }
                ViewBag.States = State_ListSelectListItem;
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }


            return View(vendor);
        }

        [HttpPost]
        public ActionResult Edit([Bind(include: "Vendor_Id,Vendor_Name,Address,City,Country, Zip_Code,Phone_Number,Email_Address,Secondary_Email_Address ,Fax_Number , Tax_Number, Contact_Person ,State,State_Id ,Website, Vendor_Code")]Vendor_M vendor)
        {
            Operation_Message msg = new Operation_Message();
            Vendor_Helper vendor_Helper = new Vendor_Helper();

            try
            {
                List<SelectListItem> State_ListSelectListItem = new List<SelectListItem>();

                Customer_M customer = new Customer_M();
                Customer_Helper customer_Helper = new Customer_Helper();
                var state_Ddl = customer_Helper.GetAll_States();
                if (state_Ddl != null)
                {
                    State_ListSelectListItem = (from p in state_Ddl.AsEnumerable()
                                                orderby p.State ascending
                                                select new SelectListItem
                                                {
                                                    Text = p.State.ToString(),
                                                    Value = p.State_Id.ToString(),
                                                    Selected = (long)p.State_Id == customer.State_Id ? true : false
                                                }).ToList();

                }
                ViewBag.States = State_ListSelectListItem;


                if (ModelState.IsValid)
                {
                    vendor.Mode = 'U';
                    msg = vendor_Helper.Update_Vendor(vendor);
                }

                if (msg.Is_success)
                {
                    List<Vendor_M> lst_vendor = new List<Vendor_M>();
                    vendor_Helper = new Vendor_Helper();
                    lst_vendor = vendor_Helper.Get_Vendors();

                    ViewBag.Message_Id = msg.Return_Message_Id;
                    ViewBag.Message = msg.Return_Message;


                    return View("List", lst_vendor);
                }
                else
                {
                    ViewBag.Message_Id = msg.Return_Message_Id;
                    ViewBag.Message = msg.Return_Message;
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View();
            }
        }


        [HttpGet]
        public JsonResult Get_Vendor_Json(long vendor_Id)
        {
            Vendor_M vendor = new Vendor_M();
            try
            {
                Vendor_Helper vendor_Helper = new Vendor_Helper();
                vendor = vendor_Helper.Get_VendorDetails(vendor_Id);
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            return Json(vendor);
        }



        [HttpGet]
        public JsonResult GetAll_Zip(long state_Id, string search_Text)
        {
            List<Zips> zip_Ddl = new List<Zips>();

            List<SelectListItem> _userDdlList = new List<SelectListItem>();
            try
            {
                Vendor_Helper vendor_Helper = new Vendor_Helper();
                zip_Ddl = vendor_Helper.GetAll_Zip(state_Id, search_Text);

                _userDdlList = (from p in zip_Ddl.AsEnumerable()
                                select new SelectListItem
                                {
                                    Text = p.Zip_Code,
                                    Value = p.Zip_Code_Id.ToString()
                                }).ToList();
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            //var obj = new
            //{
            //    response = new
            //    {
            //        //lst = zip_Ddl,
            //        //message = ViewBag.Message,
            //        //message_Id = ViewBag.Message_Id
            //    }
            //};
            return Json(_userDdlList);
        }


    }
}